package com.mySampleApplication.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.mySampleApplication.client.MySampleApplicationService;

public class MySampleApplicationServiceImpl extends RemoteServiceServlet implements MySampleApplicationService {
    // Implementation of sample interface method
    public String getMessage(String msg) {
        return "Error: " + msg;
    }

    public String calc(int a, int b, String oper) {
        double result = 0;
        switch (oper){
            case "+": result = a+b;
            break;
            case "-": result = a-b;
            break;
            case "/": result = (double)(a)/b;
            break;
            case "*": result = a*b;
            break;
            default: return Double.toString(result);
        }
        return Double.toString(result);
//        return "Error";
    }

}