package com.mySampleApplication.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.DOM;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class MySampleApplication implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        final TextBox param1 = new TextBox();
        final TextBox param2 = new TextBox();
        final ListBox operations = new ListBox();
        final Button button = new Button("Calc");
        final Label label = new Label();

        operations.addItem("+");
        operations.addItem("-");
        operations.addItem("/");
        operations.addItem("*");

        button.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                try {
                    int paramInt1 = Integer.parseInt(param1.getText());
                    int paramInt2 = Integer.parseInt(param2.getText());
                    String oper = operations.getSelectedItemText();
                    MySampleApplicationService.App.getInstance().calc(paramInt1, paramInt2, oper, new MyAsyncCallback(label));
                }
                catch (NumberFormatException e) {
                    MySampleApplicationService.App.getInstance().getMessage("Please, input only number without character", new MyAsyncCallback(label));
                }
            }
        });

        RootPanel.get("slot1").add(label);
        RootPanel.get("slot2").add(param1);
        RootPanel.get("slot3").add(param2);
        RootPanel.get("slot4").add(operations);
        RootPanel.get("slot5").add(button);
    }


    private static class MyAsyncCallback implements AsyncCallback<String> {
        private Label label;

        public MyAsyncCallback(Label label) {
            this.label = label;
        }

        public void onSuccess(String result) {
            label.getElement().setInnerHTML(result);
        }

        public void onFailure(Throwable throwable) {
            label.setText("Failed to receive answer from server!");
        }
    }
}
