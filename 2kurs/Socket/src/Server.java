import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    final int PORT = 3456;
    private List<Connection> connections;

    public Server() throws IOException {
        connections = new ArrayList<>();
        go();
    }

    public void go() throws IOException {
        ServerSocket serverSocket = new ServerSocket(PORT);
        while (true) {
            Socket client = serverSocket.accept();
            System.out.println("i get");
            connections.add(new Connection(this, client));
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }
}
