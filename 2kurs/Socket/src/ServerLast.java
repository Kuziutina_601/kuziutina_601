import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class ServerLast {

    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        String name;
        ServerSocket s = new ServerSocket(PORT);
        Socket client = s.accept();
        PrintWriter os = new PrintWriter(client.getOutputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
        Scanner sc = new Scanner(System.in);
        System.out.println("What is your name? please, enter it");
        name = sc.nextLine();
        String buf, lk = "";
        while (!lk.equals("end")) {
            System.out.print("You: ");
            buf = sc.nextLine();
            if (buf.equals("end")){
                os.println(buf);
                os.flush();
                break;
            }
            os.println(name + ": " + buf);
            os.flush();

            lk = br.readLine();
            while (lk.equals("")) {
                lk = br.readLine();
            }
            System.out.println(lk);
        }
        os.close();
    }
}
