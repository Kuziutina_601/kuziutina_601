import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientLast {
    public static void main(String[] args) throws IOException {
        String name;

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);
        BufferedReader is = new BufferedReader(new InputStreamReader(s.getInputStream()));

        PrintWriter pw = new PrintWriter(s.getOutputStream());
        Scanner sc = new Scanner(System.in);
        System.out.println("What is your name? please, enter it");
        name = sc.nextLine();
        String buf, x = "";
        while (!x.equals("end")) {
            x = is.readLine();
            while (x.equals("")) {
                x = is.readLine();
            }
            System.out.println(x);

            System.out.print("You: ");
            buf = sc.nextLine();
            if (buf.equals("end")) {
                pw.println(buf);
                pw.flush();
                break;
            }
            pw.println(name + ": " + buf);
            pw.flush();
        }
        is.close();

    }
}
