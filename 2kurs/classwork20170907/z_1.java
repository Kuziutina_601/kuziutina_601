import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by пользователь on 06.09.2017.
 */
public class z_1 {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File("z1_result"));

        pw.println("<html>\n <head>\n<title>Таблица</title>\n</head>\n<body>\n<table border=\"1\">");

        for (int i = 1; i <= 10; i++) {
            pw.println("<tr>");
            for (int j = 1; j <= 10; j++) {
                pw.println("<td>" + i*j + "</td>");
            }
            pw.println("</tr>");
        }

        pw.println("</table>\n" +
                "</body>\n" +
                "</html>");
        pw.close();
    }
}
