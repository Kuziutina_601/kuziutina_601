package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ChatView extends Stage{

    private BufferedReader reader;
    private PrintWriter writer;
    private TextArea ta;


    public ChatView() {
        super();
        this.reader = null;
        this.writer = null;
        initialView();
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
        if (writer != null) {
            ChatListener cl = new ChatListener(ta, reader);
            cl.setDaemon(true);
            cl.start();
        }
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
        if (reader != null) {
            ChatListener cl = new ChatListener(ta, reader);
            cl.setDaemon(true);
            cl.start();
        }
    }

    public void initialView() {
        this.setTitle("Super puper chat");
        VBox vBox = new VBox();
        Group root = new Group();
        Button send = new Button();
        send.setText("Send");


        ta = new TextArea();

        ta.setEditable(false);
        TextField tf = new TextField();
        tf.requestFocus();

        send.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                String message = tf.getText();

                tf.clear();
                ta.setText(ta.getText() + "You: " + message + "\n");

                writer.println(message);
            }
        });
        vBox.getChildren().add(ta);
        vBox.getChildren().add(tf);
        vBox.getChildren().add(send);
        root.getChildren().add(vBox);
        Scene scene = new Scene(root, 400, 350);
        this.setScene(scene);

    }
}
