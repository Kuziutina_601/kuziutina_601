package sample;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;

public class ChatListener extends Thread {
    private TextArea textArea;
    private BufferedReader reader;

    public ChatListener(TextArea textArea, BufferedReader reader) {
        this.textArea = textArea;
        this.reader = reader;
    }

    public void run() {
        while (true) {
            try {
                String line = reader.readLine();

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        textArea.setText(textArea.getText() + line + "\n");
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
