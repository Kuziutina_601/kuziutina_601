package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Connection implements Runnable {
    private Socket socket;
    private Server server;
    private Thread thread;
    private PrintWriter pw;

    public Connection(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;

        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {

        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            pw = new PrintWriter(socket.getOutputStream(), true);

            int i = 0;
            String x = "";
            while (true) {

                x = is.readLine();


                for (Connection connection: server.getConnections()){
                    if (connection != this) {
                        connection.getPw().println(x);
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PrintWriter getPw() {
        return pw;
    }

    public void setPw(PrintWriter pw) {
        this.pw = pw;
    }

}