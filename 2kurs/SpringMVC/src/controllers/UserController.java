package controllers;

import DAO.UserDAO;
import Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {
    private UserDAO userDAO;

    @RequestMapping(method = RequestMethod.GET)
    public String users(ModelMap map) {
        List<User> users = userDAO.getAll();
        map.addAttribute("users", users);
        return "/users";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public String getInfo(@PathVariable("id") int id, ModelMap map) {
        User user = userDAO.get(id);
        map.addAttribute("user", user);
        return "/userInfo";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/delete")
    public String deleteUser(@PathVariable("id") int id) {
        userDAO.delete(userDAO.get(id));
        return "redirect:/users";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}")
    public String updateUser(@PathVariable("id") int id, @RequestParam("name") String name, ModelMap map) {
        User upUser = new User(id, name);
        userDAO.update(upUser);
        map.addAttribute("user", upUser);
        return "/userInfo";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addUser(@RequestParam("name") String name, ModelMap map) {
        User newUser = new User(name);
        userDAO.add(newUser);
        map.addAttribute("users", userDAO.getAll());
        return "/users";
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
