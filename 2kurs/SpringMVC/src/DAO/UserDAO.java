package DAO;

import Helper.DBConnection;
import Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserDAO implements DAO<User> {
    public String test;
    private Connection conn;

    public UserDAO() {

    }



    @Override
    public boolean add(User model) {
        conn = DBConnection.getConnection();
        try {
            PreparedStatement ps = conn.prepareStatement("insert into users(name) values (?)");
            ps.setString(1, model.getName());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(User model) {
        conn = DBConnection.getConnection();
        try {
            PreparedStatement ps = conn.prepareStatement("delete from users where id = ?");
            ps.setInt(1, model.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(User model) {
        conn = DBConnection.getConnection();
        try {
            PreparedStatement ps = conn.prepareStatement("update users set name = ? where id = ?");
            ps.setString(1, model.getName());
            ps.setInt(2, model.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public User get(int id) {
        conn = DBConnection.getConnection();
        User user = null;
        try {
            PreparedStatement ps = conn.prepareStatement("select * from users where id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            user = new User(rs.getInt("id"), rs.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<User> getAll() {
        conn = DBConnection.getConnection();
        List<User> users = new ArrayList<>();

        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from users");
            while (rs.next()) {
                users.add(new User(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }


}
