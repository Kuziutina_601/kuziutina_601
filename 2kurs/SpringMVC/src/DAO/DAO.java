package DAO;

import Model.Model;

public interface DAO<T extends Model> {
    public boolean add(T model);
    public boolean delete(T model);
    public boolean update(T model);
    public Model get(int id);
}
