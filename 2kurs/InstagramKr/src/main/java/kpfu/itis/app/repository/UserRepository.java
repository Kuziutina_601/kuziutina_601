package kpfu.itis.app.repository;

import kpfu.itis.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByLogin(String username);
}
