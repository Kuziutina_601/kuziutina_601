package kpfu.itis.app.repository;

import kpfu.itis.app.model.Like;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Like, Long> {
}
