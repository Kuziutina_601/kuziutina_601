package kpfu.itis.app.service;

import kpfu.itis.app.model.Post;
import kpfu.itis.app.repository.PostRepository;
import kpfu.itis.app.service.inter.PostServiceInter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService implements PostServiceInter {
    @Autowired
    private PostRepository postRepository;


    @Override
    public void addPost(Post news) {
        postRepository.save(news);
    }

    @Override
    public List<Post> getAllPost() {
        return postRepository.findAll();
    }

    @Override
    public Post find(long id) {
        return postRepository.getOne(id);
    }

    @Override
    public void delete(long id) {
        postRepository.deleteById(id);

    }
}
