package kpfu.itis.app.controller;

import kpfu.itis.app.repository.UserRepository;
import kpfu.itis.app.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/user")
public class ProfileController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getAllCars(@PathVariable("id") int id, ModelMap map) {
        map.addAttribute("user", userRepository.getOne(Long.valueOf(id)));
        return "profile";
    }

}
