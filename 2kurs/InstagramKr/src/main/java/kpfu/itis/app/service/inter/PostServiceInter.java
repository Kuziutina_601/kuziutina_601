package kpfu.itis.app.service.inter;

import kpfu.itis.app.model.Post;

import java.util.List;

public interface PostServiceInter {
    void addPost(Post news);
    List<Post> getAllPost();
    Post find(long id);
    void delete(long id);
}