package kpfu.itis.app.controller;

import kpfu.itis.app.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/feed")
public class FeedController {

    @Autowired
    private PostService postService;

    @RequestMapping(method = RequestMethod.GET)
    public String getAllCars(ModelMap map) {
        map.addAttribute("posts", postService.getAllPost());
        return "feed";
    }

}
