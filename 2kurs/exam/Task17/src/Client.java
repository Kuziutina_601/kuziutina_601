import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        String name;

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        BufferedReader is = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter pw = new PrintWriter(s.getOutputStream(), true);

        Scanner scanner = new Scanner(System.in);
        Field field = new Field();

        String x = "";
        int row, col, win;
        while (true) {
            System.out.println("please enter row and col start with 0");
            row = scanner.nextInt();
            col = scanner.nextInt();
            field.onlyStep(row, col, 1);
            pw.println(row + " " + col);
            field.print();

            System.out.println("wait opponent");
            x = is.readLine();
            String[] data = x.split(" ");
            row = Integer.parseInt(data[0]);
            col = Integer.parseInt(data[1]);
            win = Integer.parseInt(data[2]);
            if (win == 1) {
                System.out.println("WIN CLIENT");
                break;
            }
            else if (win == 2) {
                System.out.println("FUUU CLIENT");
                break;
            }
            else if (win == 3) {
                System.out.println("TOGETHER");
                break;
            }
            field.onlyStep(row, col, 2);
            field.print();

        }
//        is.close();



//        System.out.println("i connected");
    }
}
