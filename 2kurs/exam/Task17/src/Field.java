public class Field {
    private int[][] field = new int[3][3];

    public Field() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = 0;
            }
        }
    }

    public int[][] getField() {
        return field;
    }

    public void setField(int[][] field) {
        this.field = field;
    }

    public boolean step(int row, int col, int player) {
        field[row][col] = player;
        return checkWin(row, col, player);
    }

    public boolean checkWin(int row, int col, int player) {
        if (player == field[row][(col + 1) %3] && player == field[row][(col + 2) %3]) return true;
        if (player == field[(row+1)%3][col] && player == field[(row + 2)%3][col]) return true;
        if (row == col)  {
            if (player == field[(row + 1) %3][(col + 1) %3] && player == field[(row + 2)%3][(col + 2) %3]) return true;
        }
        if (row + col == 2) {
            if (player == field[0][2] && player == field[1][1]  && player == field[2][0]) return true;
        }
        return false;
    }

    public void onlyStep(int row, int col, int player) {
        field[row][col] = player;
    }

    public void print() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean hasZero() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == 0) return true;
            }
        }
        return false;
    }
}
