import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Connection implements Runnable{
    private Socket socket;
    private Server server;
    private Thread thread;
    private PrintWriter pw;
    private boolean game = true;
    private boolean winner;
    private Field field;
    private Scanner sc;

    public Connection(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;
        field = new Field();
        sc = new Scanner(System.in);

        thread = new Thread(this);
        thread.start();

    }

    public void startPlay() {

        System.out.println("opponent went");
        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            pw = new PrintWriter(socket.getOutputStream(), true);

            String x = "";
            int col, row;
            while (true) {

                System.out.println("i wait opponent");
                x = is.readLine();
                String[] data = x.split(" ");
                row = Integer.parseInt(data[0]);
                col = Integer.parseInt(data[1]);

                boolean win = field.step(row, col, 1);
                field.print();
                if (win){
                    pw.println("0 0 1");
                    System.out.println("FUUUUU");
                    break;
                }
                else if (!field.hasZero()) {
                    System.out.println("TOGETHER");
                    pw.println("0 0 3");
                    break;
                }
                else {
                    System.out.println("please enter row and col start with 0");
                    row = sc.nextInt();
                    col = sc.nextInt();
                    win = field.step(row, col, 2);
                    field.print();
                    if (win) {
                        pw.println(row + " " + col + " 2");
                        field.print();
                        System.out.println("WIN!!!");
                        break;
                    }
                    else {
                        pw.println(row + " " + col + " 0");
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PrintWriter getPw() {
        return pw;
    }

    public void setPw(PrintWriter pw) {
        this.pw = pw;
    }

    @Override
    public void run() {
        startPlay();
    }
}