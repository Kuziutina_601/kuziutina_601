package Task9;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(new File("in.txt"));
            PrintWriter pw = new PrintWriter(new File("out.txt"));

            List<Integer> list = new ArrayList<>();

            while (scanner.hasNext()) {
                list.add(scanner.nextInt());
            }

            Collections.sort(list, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    int count1 = countFive(o1);
                    int count2 = countFive(o2);

                    if (count1 > count2) return 1;
                    if (count1 < count2) return -1;
                    return 0;
                }

                public int countFive(Integer number) {
                    int count = 0;
                    int buf;
                    while (number > 0) {
                        buf = number%10;
                        if (buf == 5) {
                            count++;
                        }
                        number = number/10;
                    }
                    return count;
                }
            });

            for (Integer i: list) {
                pw.println(i);
            }
            pw.flush();

            pw.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
