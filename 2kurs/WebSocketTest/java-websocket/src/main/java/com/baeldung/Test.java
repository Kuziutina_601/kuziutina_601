package com.baeldung;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        double p;
        //Scanner sc = new Scanner(System.in);
        //p = sc.nextDouble();
        double result = 1.0;
        for (int j = 5; j < 156; j+=2) {
            p = j /100.0;
            //System.out.println(p);
            result = 1.0;

            for (int i = 99; i > 75; i--) {
                result = result * i * (1 - p) * p / (i - 75);
            }
            for (int i = 0; i < 51; i++) {
                result *= p;

            }
            if (result >0.8) {
                System.out.printf("%.2f", result);
                System.out.println("    " + p);
            }
        }
    }
}
