package Classwork2602.TaskZero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Developer {

    public String getName() {
        return name;
    }

    private String name;

    public Developer(String name) {
        this.name = name;
    }

    public void work() {
        System.out.println("I am working");
    }

    public void learnLanguage(String language) {
        System.out.println("I am learning language " + language);
    }
}
