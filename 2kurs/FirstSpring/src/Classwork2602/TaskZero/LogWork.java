package Classwork2602.TaskZero;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LogWork {
    // Developer здесь из произвольного пакета
    // Параметр JoinPoint – вся информация о вызванном методе

    @Before("execution(* *..Developer.work())")
    public void loggingWork(JoinPoint jp){
        System.out.println(((Developer) jp.getThis()).getName()      + " is ready to work");
    }

}
