package Classwork2102;

import org.springframework.stereotype.Component;



@Component
public class GoodClass {
    private final int testArg = 5;

    public int getTestArg() {
        return testArg;
    }
}
