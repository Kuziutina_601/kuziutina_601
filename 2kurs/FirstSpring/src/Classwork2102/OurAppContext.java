package Classwork2102;

import org.reflections.Reflections;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OurAppContext {
    private Map<String, Object> beans;

    public OurAppContext(String packageName) {
        Map<String, BeanDefinition> beanDefinitionMap = createBeanDefinitions(packageName);
        createBeans(beanDefinitionMap);
    }

    private void createBeans(Map<String, BeanDefinition> beanDefinitionMap) {
        beans = new HashMap<>();
        for (String beanNames: beanDefinitionMap.keySet()) {
            if (beans.get(beanNames) == null) {
                Object o = createBean(beanDefinitionMap.get(beanNames).getClassType());
                beans.put(beanNames, o);
                initialMethodWithAutowired(o);
//                Class cl = beanDefinitionMap.get(beanNames).getClassType();
//                Object o = null;
//                try {
//                    o = cl.newInstance();
//                    beans.put(beanNames, o);
//                    for (Method m: cl.getMethods()) {
//                        if (m.getName().startsWith("set") && m.isAnnotationPresent(Autowired.class)) {
//                            Class[] getClass = m.getParameterTypes();
//                            for (Class name: getClass) {
//                                if (beanDefinitionMap.containsKey(name.getName())) {
//                                    m.invoke(beanDefinitionMap.get(name.getName()).getClassType());
//                                }
//                                else {
//                                    new NoSuchBeanDefinitionException("no class, no component");
//                                }
//                            }
//                            //тута ищем зависимость
//                            //ищем бин
//                            //внедряем бин, вызываем сеттер с помощью invoke
//                        }
//                    }
//                } catch (InstantiationException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                }
            }
        }
    }

    private Object createBean(Class cl) {
        Object o = null;
        try {
            o = cl.newInstance();
            return o;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initialMethodWithAutowired(Object o) throws NoSuchBeanDefinitionException{
        for (Method m: o.getClass().getMethods()) {
            if (m.getName().startsWith("set") && m.isAnnotationPresent(Autowired.class)) {
                for (Class name : m.getParameterTypes()) {
                    if (beans.containsKey(name.getName())) {
                        try {
                            m.invoke(o, beans.get(name.getName()));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    } else {
                        throw new NoSuchBeanDefinitionException("no class, no component");
                    }
                }
                //тута ищем зависимость
                //ищем бин
                //внедряем бин, вызываем сеттер с помощью invoke
            }
        }
    }

    private Map<String,BeanDefinition> createBeanDefinitions(String packageName) {
        Map<String, BeanDefinition> beanDef = new HashMap<>();

        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Component.class);

        for (Class<?> cl: annotated) {
            String name = cl.getName();
            beanDef.put(name, new BeanDefinition(cl));
        }
        return beanDef;
    }




    public <T> T getBean(Class<T> classType) {
        String beanName = classType.getName();

        return (T) beans.get(beanName);
    }
}
