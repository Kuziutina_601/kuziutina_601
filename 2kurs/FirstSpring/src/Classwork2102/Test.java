package Classwork2102;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Test {
    private GoodClass gc;

    @Autowired
    public void setGoodClass(GoodClass gc) {
        this.gc = gc;
        System.out.println("i set");
    }

    public GoodClass getGoodClass() {
        return gc;
    }
}
