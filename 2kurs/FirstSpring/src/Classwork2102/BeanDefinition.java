package Classwork2102;

public class BeanDefinition {
    private Class classType;

    public BeanDefinition(Class cl) {
        classType = cl;
    }

    public Class getClassType() {
        return classType;
    }
}
