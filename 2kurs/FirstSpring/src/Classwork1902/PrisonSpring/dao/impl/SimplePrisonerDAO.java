package Classwork1902.PrisonSpring.dao.impl;

import Classwork1902.PrisonSpring.dao.PrisonerDAO;
import org.springframework.stereotype.Component;

@Component
public class SimplePrisonerDAO implements PrisonerDAO{

    @Override
    public String getRandomPrisonerName() {
        if (Math.random() > 0.5) {
            return "Vlad";
        }
        return "Eldar";
    }
}
