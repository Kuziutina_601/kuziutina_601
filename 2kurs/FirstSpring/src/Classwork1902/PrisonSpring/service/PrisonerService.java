package Classwork1902.PrisonSpring.service;

import Classwork1902.PrisonSpring.dao.PrisonerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class PrisonerService {
    private PrisonerDAO prisonerDAO;


    @Autowired
    public PrisonerService(PrisonerDAO prisonerDAO) {
        this.prisonerDAO = prisonerDAO;
        System.out.println("i create");
    }

    public void print() {
        System.out.println("test " + prisonerDAO.getRandomPrisonerName());
    }
//
//    @Autowired
//    public void setPrisonerDAO(PrisonerDAO prisonerDAO) {
//        this.prisonerDAO = prisonerDAO;
//    }
//
//    public PrisonerDAO getPrisonerDAO() {
//        return prisonerDAO;
//    }
}
