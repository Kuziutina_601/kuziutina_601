import java.util.Scanner;

public class Keks {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int prev, buf, a = 0, b = 1000000;
        int min = Integer.MAX_VALUE;
        prev = 1;

        for(int i = 0; i < n; i++) {
            buf = sc.nextInt();
            if (buf <= 500000 && a < buf) {
                a = buf;
            }
            else if(buf > 500000 && b > buf) {
                b = buf;
            }
        }

        buf = Math.max(a-1, 1000000-b);

        System.out.println(buf);



    }
}
