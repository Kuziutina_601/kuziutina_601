package Classwork1402;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("/spring-config.xml");

//        Classwork1402.Team team1 = (Classwork1402.Team) ac.getBean("team1");
//
        Team team2 = (Team) ac.getBean("team2");
//
//        System.out.println(team1);
//        System.out.println(team2);
//
//        Classwork1402.Player player1 = (Classwork1402.Player) ac.getBean("player1");
//        Classwork1402.Player player2 = (Classwork1402.Player) ac.getBean("player2");
//
//        System.out.println(player1);
//        System.out.println(player2);

        System.out.println(team2.getPlayers());

    }
}
