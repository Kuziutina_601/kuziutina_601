package Classwork1402;

public class FootballPlayer extends Player {

    public FootballPlayer(String name, Team team) {
        this.name = name;
        this.team = team;
    }



    @Override
    public String toString() {
        return "FP " + name + "  " + team;
    }
}
