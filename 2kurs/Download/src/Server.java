import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Server {

    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        ServerSocket s = new ServerSocket(PORT);
        Socket client = s.accept();
        PrintWriter os = new PrintWriter(client.getOutputStream());
//        BufferedReader os = new BufferedReader(new InputStreamReader(client.getOutputStream()));
//        OutputStream os = client.getOutputStream();
        byte b = (byte) new Random().nextInt(127);
        os.write(b);
    }
}
