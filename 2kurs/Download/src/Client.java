import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);
        BufferedReader is = new BufferedReader(new InputStreamReader(s.getInputStream()));
        int x = is.read();
        System.out.println(x);
    }
}
