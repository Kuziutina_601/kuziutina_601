/**
 * Created by пользователь on 21.02.2017.
 */
public class Game {
    public void gameIsGoing(){
        Player player = new Player();
        player.setCurrentArmory(new Sword());
        OrcCreater oc = new OrcCreater();
        BadGuy orc1 = oc.getBadGuy();

        ZhnetsCreater zc = new ZhnetsCreater();
        BadGuy zhnets1 = zc.getBadGuy();

        WolfCreater wc = new WolfCreater();
        BadGuy wolf1 = wc.getBadGuy();

        zhnets1.attack(player);
        player.attack(orc1);
        player.attack(zhnets1);

        Book b = new Book();
        player.setCurrentArmory(new BookToArmoryAdapter(b));
        player.attack(orc1);
    }

    public static void main(String[] args) {
        (new Game()).gameIsGoing();
    }
}
