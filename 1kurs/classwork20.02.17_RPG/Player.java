/**
 * Created by пользователь on 20.02.2017.
 */
public class Player extends GameCharacter{
    private String name;
    private int hp;
    private ArmoryThatCanAttack currentArmory;

    public Player() {
        this.name = "Player";
        this.hp = 100;
        this.currentArmory = new Sword();
    }

    public void attack(GameCharacter badGuy) {
        currentArmory.attack(badGuy);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void setCurrentArmory(ArmoryThatCanAttack armory) {
        this.currentArmory = armory;
    }

    public ArmoryThatCanAttack getCurrentArmory() {
        return currentArmory;
    }

    public Player getPlayer() {
        if (this == null) new Player();
        return this;
    }

}
