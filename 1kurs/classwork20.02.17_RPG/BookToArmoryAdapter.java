/**
 * Created by пользователь on 21.02.2017.
 */
public class BookToArmoryAdapter implements ArmoryThatCanAttack {
    private Book adptee;

    public BookToArmoryAdapter(Book b) {adptee = b;}

    @Override
    public void attack(GameCharacter badGuy) {
        adptee.throwIt(badGuy);
    }
}
