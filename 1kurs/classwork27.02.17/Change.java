import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by пользователь on 27.02.2017.
 */
public class Change {

    public static String s;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        s = sc.nextLine();


        System.out.println(smallPart(0, s.length() - 1));


    }

    public static String smallPart (int l, int r) {
        //System.out.println("i have new part " + l + "  " + r);
        String result = "";
        boolean has = false;
        StackInteger st = new StackInteger();
        int start = l;
        ArrayList<Integer> qq = new ArrayList<>();
        for (int i = l; i < r; i++) {
            //System.out.println(s);
            //System.out.println(i);
            //System.out.println(i + "   " + s.charAt(i));
            if (s.charAt(i) == '(')
                st.push(1);
            else if (s.charAt(i) == ')')
                st.pop();
            else if ((s.charAt(i) == '+' || s.charAt(i) == '-') && st.isEmpty()){
                has = true;
                result += s.charAt(i) + smallPart(start, i-1);
                start = i + 1;
            }
            else if ((s.charAt(i) == '*' || s.charAt(i) == '/') && st.isEmpty()) {
                qq.add(i);
            }
        }
        if (has)
            result += smallPart(start, r);
        else {
            if (qq.isEmpty()) {
                if (s.charAt(l) == '(')
                    result += smallPart(l+1, r-1);
                else {
                    for (int i = l; i <= r; i++) {
                        result += s.charAt(i);
                    }
                }
            }
            else {
                start = l;
                for (int i = 0; i < qq.size(); i++) {
                    if (s.charAt(start) == '(')
                        result += s.charAt(qq.get(i)) +  smallPart(start + 1, qq.get(i) - 2);
                    else
                        result += s.charAt(qq.get(i)) + smallPart(start, qq.get(i) - 1);
                    start = qq.get(i) + 1;
                }
                if (s.charAt(start) == '(')
                    result += smallPart(start + 1, r - 1);
                else
                    result += smallPart(start, r);
            }

        }
        return result;
    }
}
