package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.*;

/**
 * Created by пользователь on 16.05.2017.
 */
public class MyStage extends Stage {

    private BufferedReader reader;
    private PrintWriter writer;
    public MyStage(PipedInputStream pis, PipedOutputStream pos) {
        super();
        this.reader = new BufferedReader(new InputStreamReader(pis));
        this.writer = new PrintWriter(pos, true);
        initialView();
    }

    public void initialView() {
        this.setTitle("jjjj");
        VBox vBox = new VBox();
        Group root = new Group();
        Button send = new Button();
        send.setText("Send");


        TextArea ta = new TextArea();

        ta.setEditable(false);
        TextField tf = new TextField();
        tf.requestFocus();
        ChatListener cl = new ChatListener(ta, reader);
        cl.setDaemon(true);
        cl.start();
        send.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = tf.getText();
                tf.clear();
                //display
                System.out.println("i here");
                ta.setText(ta.getText() + "You: " + message + "\n");
                ///send
                writer.println(message);
            }
        });
        vBox.getChildren().add(ta);
        vBox.getChildren().add(tf);
        vBox.getChildren().add(send);
        root.getChildren().add(vBox);
        Scene scene = new Scene(root, 300, 300);
        this.setScene(scene);

    }
}
