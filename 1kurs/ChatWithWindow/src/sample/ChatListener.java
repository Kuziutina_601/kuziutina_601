package sample;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;

/**
 * Created by пользователь on 16.05.2017.
 */
public class ChatListener extends Thread {
    private TextArea textArea;
    private BufferedReader reader;

    public ChatListener(TextArea textArea, BufferedReader reader) {
        this.textArea = textArea;
        this.reader = reader;
    }

    public void run() {
        while (true) {
            try {
                System.out.println("ll");
                String line = reader.readLine();
                System.out.println("i read");
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        textArea.setText(textArea.getText() + line + "\n");
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
