package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.text.View;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        PipedOutputStream pos1 = new PipedOutputStream();
        PipedInputStream pis1 = new PipedInputStream(pos1);
        PipedOutputStream pos2 = new PipedOutputStream();
        PipedInputStream pis2 = new PipedInputStream(pos2);

        Stage my1 = new MyStage(pis1, pos2);
        Stage my2 = new MyStage(pis2, pos1);

        my1.show();
        my2.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
