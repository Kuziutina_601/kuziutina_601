/**
 * Created by пользователь on 06.03.2017.
 */
public class Elem {
    private Konj value;
    private Elem next;

    public Elem() {
        this.value = new Konj();
        this.next = null;
    }

    public Elem(Konj value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Konj getValue() {
        return value;
    }

    public Elem getNext() {
        return next;
    }

    public void setValue(Konj value) {
        this.value = value;
    }

    public void setNext(Elem next) {
        this.next = next;
    }
}
