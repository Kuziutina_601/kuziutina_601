import java.util.ArrayList;

/**
 * Created by пользователь on 24.02.2017.
 */
public class Konj extends ArrayList<Integer> {

    public String toString() {
        String s = "";
        if (size() == 0) throw new NullPointerException("Hasn't konj");
        if (get(size() - 1) == 0)
            return "1";
        for (int i = 0; i < size(); i++) {
            s += "X" + get(i);
            if (i < size() - 1)
                s += '&';
        }

        return s;
    }

    public boolean valueKonj(boolean[] v) {
        for (int i = 0; i < size(); i++) {
            if (get(i) == 0)
                return true;
            if (!v[get(i) - 1])
                return false;

        }
        return true;
    }

    public boolean search(int m) {
        for (int i = 0; i < size(); i++) {
            if (get(i) == m)
                return true;
        }
        return false;
    }
}
