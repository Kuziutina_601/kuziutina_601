import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by пользователь on 24.02.2017.
 */
public class ZhegalkinPolinom implements Cloneable{
    private Elem head = null;
    private Elem tail = null;
    private final static int MAX_SIZE = 4;

    public ZhegalkinPolinom() {}

    public ZhegalkinPolinom(String s) {
        int m;
        Elem p;
        String[] kons;
        kons = s.split("\\+");
        for (int i = 0; i < kons.length; i++) {
            p = new Elem();
            m = 0;
            if (kons[i].length() == 1)
                p.getValue().add(0);
            else {
                while (m < kons[i].length()) {
                    if (kons[i].charAt(m) == 'X' || kons[i].charAt(m) == 'x')
                        m++;
                    p.getValue().add(Integer.parseInt(kons[i].charAt(m) + ""));
                    m += 2;
                }
            }
            if (head == null) {
                head = p;
                tail = head;
            } else {
                tail.setNext(p);
                tail = p;
            }
        }
    }

    public String toString() {
        String s = "";
        Elem p = head;
        if (p == null)
            return "";
        while (p.getNext() != null) {
            s += p.getValue() + "+";
            p = p.getNext();
        }
        s += p.getValue();
        return s;
    }

    public void insert(Konj k) {
        Konj b = (Konj)k.clone();
        Elem g = new Elem(b, null);
        Collections.sort(g.getValue());
        boolean has = false;
        Elem p = head;
        while (p != null && !has) {
            if (p.getValue().equals(g.getValue()))
                has = true;
            p = p.getNext();
        }
        if (!has) {
            if (head == null) {
                head = g;
                tail = head;
            }
            else {
                tail.setNext(g);
                tail = g;
            }
        }
    }

    public ZhegalkinPolinom summ(ZhegalkinPolinom pl) {
                                                         //i need to analyze in detail a reference type
        boolean[] deleted = new boolean[pl.size()];
        for (int i = 0; i < deleted.length; i++)
            deleted[i] = true;
        Elem p, k;
        k = new Elem();
        boolean search;
        ZhegalkinPolinom result = new ZhegalkinPolinom();
        p = head;
        int z;
        while (p != null) {
            search = false;
            k = pl.getHead();
            z = 0;
            while (k != null && !search) {
                if (deleted[z] && k.getValue().equals(p.getValue())){
                    search = true;
                    deleted[z] = false;
                }
                k = k.getNext();
                z++;
            }
            if (!search)
                result.insert(p.getValue());
            p = p.getNext();
        }
        k = pl.getHead();
        z = 0;
        while (k != null) {
            if (deleted[z])
                result.insert(k.getValue());
            k = k.getNext();
            z++;
        }

        return result;
    }

    public boolean value(boolean[] v) {
        boolean result = false;
        Elem p = head;
        while (p != null) {
            if (p.getValue().valueKonj(v))
                result = !result;
            p = p.getNext();
        }
        return result;
    }

    public void sortByLength() {
        ZhegalkinPolinom res = new ZhegalkinPolinom();
        Elem k = new Elem();
        Elem p;
        for (int i = MAX_SIZE; i > 0; i--) {
            p = head;
            while (p != null) {
                if (p.getValue().size() == i) {
                    if (p.getValue().get(0) == 0)
                        k = p;
                    else {
                        res.insert(p.getValue());
                        remove(p.getValue());
                    }
                }
                p = p.getNext();
            }
        }
        if (k != null)
            res.insert(k.getValue());
        head = res.getHead();
    }

    public ZhegalkinPolinom polinomWith(int i) {
        ZhegalkinPolinom result = new ZhegalkinPolinom();
        Elem p = head;
        while (p != null) {
            if (p.getValue().search(i))
                result.insert(p.getValue());
            p = p.getNext();
        }
        return result;
    }

    private void remove(Konj k) {
        Collections.sort(k);
        Elem p = head;
        Elem before;
        boolean search = false;
        if (head == null) throw new NullPointerException("Hasn't polinom");
        if (k.equals(head.getValue())){
            head = head.getNext();
        }
        else {
            while (p != null && !search) {
                before = p;
                p = p.getNext();
                if (p.getValue().equals(k)) {
                    search = true;
                    before.setNext(p.getNext());
                }
            }
        }
    }

    public Elem getHead() {
        return head;
    }

    public int size() {
        int n = 0;
        Elem p = head;
        while (p != null) {
            n++;
            p = p.getNext();
        }
        return n;
    }
}
