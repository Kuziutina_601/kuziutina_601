import java.util.ArrayList;
import java.util.List;

/**
 * Created by пользователь on 28.03.2017.
 */
public class NodeDif<T> {
    private T value;
    private NodeDif<T> next;
    private NodeDif<T> son;


    public NodeDif(T value) {
        this.value = value;
    }

    public NodeDif() {
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public NodeDif<T> getNext() {
        return next;
    }

    public void setNext(NodeDif<T> next) {
        this.next = next;
    }

    public NodeDif<T> getSon() {
        return son;
    }

    public void setSon(NodeDif<T> son) {
        this.son = son;
    }
}
