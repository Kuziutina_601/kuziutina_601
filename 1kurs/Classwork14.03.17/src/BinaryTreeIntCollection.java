import java.util.Iterator;

/**
 * Created by пользователь on 16.03.2017.
 */
public class BinaryTreeIntCollection<T extends Comparable<T>> implements MyCollection<T>, Iterable<T> {
    private BinaryTree bt;
    private int size = 0;

    public BinaryTreeIntCollection() {
        this.bt = new BinaryTree();
    }

    @Override
    public boolean add(T value) {
        size++;
        if (bt.getRoot() == null) bt.setRoot(new Node<T>(value));
        else {
            /*          My solution
            Node<Integer> p = bt.getRoot();
            Node<Integer> before = p;
            while (p != null) {
                before = p;
                if (p.getValue() > value) p = p.getLeft();
                else p = p.getRight();
            }
            if (before.getValue() > value) before.setLeft(new Node<>(value));
            else before.setRight(new Node<>(value));*/

            //AbMM
            Node<T> p = bt.getRoot();
            boolean go = true;
            boolean left = true;
            while (go) {
                if (value.compareTo(p.getValue()) < 0) {
                    left = true;
                    if (p.getLeft() != null) p = p.getLeft();
                    else go = false;
                }
                else {
                    left = false;
                    if (p.getRight() != null) p = p.getRight();
                    else go = false;
                }
            }
            if (left) p.setLeft(new Node<>(value));
            else p.setRight(new Node<>(value));

        }
        return true;
    }

    @Override
    public void remove(T value) {

    }

    @Override
    public boolean contains(T value) {
        if (bt.getRoot() == null) return false;
        else {
            Node<T> p = bt.getRoot();
            while (p != null) {
                if (value.compareTo(p.getValue()) < 0) p = p.getLeft();
                else if (p.getValue() == value) return true;
                else p = p.getRight();
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public void clear() {
        size = 0;
        bt.setRoot(null);
    }

    @Override
    public int size() {
        return size;
    }

    public void printTree() {
         bt.printTree();
    }
}
