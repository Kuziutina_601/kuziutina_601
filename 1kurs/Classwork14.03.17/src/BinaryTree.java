/**
 * Created by пользователь on 09.03.2017.
 */
public class BinaryTree<T> {
    private Node<T> root;

    /*private static <T> void makeTreeOnNode (Node<Integer> node, int n)  {
        int nLeft = n/2;
        int nRight = n - 1 - nLeft;
        node.setValue(n);
        if (nLeft > 0) {
            node.setLeft(new Node<>());
            makeTreeOnNode(node.getLeft(), nLeft);
        }
        if (nRight > 0) {
            node.setRight(new Node<>());
            makeTreeOnNode(node.getRight(), nRight);
        }
    }

    public BinaryTree (int n) {
        root = new Node<T>();
        makeTreeOnNode(root, n);
    }*/

    public BinaryTree() {
        root = null;
    }

    public void printTree() {
        printNode(root, 0);
    }

    private void printNode(Node<T> root, int h) {
        if (root != null) {
            printNode(root.getLeft(), h + 1);
            for (int i = 1; i <= h; i++)
                System.out.print("  ");
            System.out.println(root.getValue());
            printNode(root.getRight(), h + 1);
        }
    }

    public Node<T> getRoot() {
        return root;
    }

    public void setRoot(Node<T> root) {
        this.root = root;
    }
}
