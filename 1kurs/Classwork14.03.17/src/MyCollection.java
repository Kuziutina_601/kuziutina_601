import java.util.Iterator;

/**
 * Created by пользователь on 16.03.2017.
 */
public interface MyCollection<T> {

    boolean add(T value);
    void remove(T value);
    boolean contains(T value);
    Iterator<T> iterator();
    void clear();
    int size();
}
