package repositories;

import db.DataBaseConnection;
import entities.Continent;
import entities.Country;

import java.util.List;

/**
 * Created by пользователь on 23.03.2017.
 */
public class CountryRepository {
    private static DataBaseConnection dbConnection;
    public CountryRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }
    public List<Country> getAllCountry() {
        return dbConnection.getDataBaseConnection().getAllCountries();
    }


    public Country getByName (String name) {
        for (Country country: dbConnection.getDataBaseConnection().getAllCountries()) {
            if (name.equals(country.getCountryName())) return country;
        }
        return null;
    }

    //public  getById(int id) {
   //     return dbConnection.getDataBaseConnection().getById(id);
    //}

}
