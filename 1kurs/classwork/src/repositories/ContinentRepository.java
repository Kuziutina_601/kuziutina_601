package repositories;

import db.DataBase;
import db.DataBaseConnection;
import entities.Continent;

import java.util.List;

/**
 * Created by пользователь on 23.03.2017.
 */
public class ContinentRepository {
    private static DataBaseConnection dbConnection;
    public ContinentRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }
    public List<Continent> getAllContinents() {
        return dbConnection.getDataBaseConnection().getAllContinents();
    }

    public Continent getById(int id) {
        return dbConnection.getDataBaseConnection().getContinentById(id);
    }

    public Continent getByName (String name) {
        for (Continent continent: dbConnection.getDataBaseConnection().getAllContinents()) {
            if (name.equals(continent.getContinent())) return continent;
        }
        return null;
    }
}
