package db;

import entities.Continent;
import entities.Country;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by пользователь on 23.03.2017.
 */
public class DataBase {
    private List<Country> countries;
    private List<Continent> continents;

    public void init() {
        prepareContinents("DataSets/continents.csv");
        prepareCountries("DataSets/countries.csv");
    }

    private void prepareContinents(String fileName) {
        continents = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                Continent c = new Continent();
                c.setContinentId(Integer.parseInt(strData[0]));
                c.setContinent(strData[1].substring(1, strData[1].length()- 1));
                continents.add(c);
            }
            sc.close();
            System.out.println("continent db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    private void prepareCountries(String fileName) {

        countries = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                Country c = new Country();
                c.setCountryId(Integer.parseInt(strData[0]));
                c.setCountryName(strData[1].substring(1, strData[1].length()- 1));
                c.setContinentId(Integer.parseInt(strData[2]));
                c.setContinent(getContinentById(c.getContinentId()));
                countries.add(c);
            }
            sc.close();
            System.out.println("countries db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    public List<Continent> getAllContinents() {
        return continents;
    }

    public List<Country> getAllCountries() {
        return countries;
    }

    public Continent getContinentById(int id) {
        for (Continent continent: getAllContinents()) {
            if (continent.getContinentId() == id) {
                return continent;
            }
        }
        return null;
    }

}
