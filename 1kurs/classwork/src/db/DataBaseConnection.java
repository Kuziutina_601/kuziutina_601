package db;

/**
 * Created by пользователь on 23.03.2017.
 */
public class DataBaseConnection {
    private static DataBase db;

    public static DataBase getDataBaseConnection() {
        if (db == null) {
            db = new DataBase();
            db.init();
        }
        return db;
    }
}
