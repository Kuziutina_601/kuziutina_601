import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by пользователь on 21.03.2017.
 */
public class Task02_210317 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("en_v1.dic"));
        HashMap<Integer, HashSet<String>> dictionary = new HashMap<>();
        String buf;

        while (sc.hasNextLine()) {
            buf = sc.nextLine();
            String[] s = buf.split(" ");
            if (dictionary.get(Integer.parseInt(s[1])) == null) {
                HashSet<String> bufer = new HashSet<>();
                bufer.add(s[0]);
                dictionary.put(Integer.parseInt(s[1]), bufer);
            }
            else {
                dictionary.get(Integer.parseInt(s[1])).add(s[0]);
            }
        }
        System.out.println(dictionary.toString());
    }
}
