package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//Id,Model,Make
public class Car_names {
    private int id;
    private String model;
    private String make;

    public Car_names(int id, String model, String make) {
        this.id = id;
        this.model = model;
        this.make = make;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }
}
