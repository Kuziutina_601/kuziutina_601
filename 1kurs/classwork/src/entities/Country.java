package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//Country,CountryName,entities.Continent
public class Country {
    private int countryId;
    private String countryName;
    private int continentId;
    private Continent continent;

    public Country() {

    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getContinentId() {
        return continentId;
    }

    public void setContinentId(int continentId) {
        this.continentId = continentId;
    }

    public String toString() {
        return "Contry{ " +
                "contryId " + countryId +
                ", country name " + countryName;
    }
}
