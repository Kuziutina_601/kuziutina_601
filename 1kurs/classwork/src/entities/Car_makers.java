package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//Id,Maker,FullName,entities.Country
public class Car_makers {
    private long id;
    private String maker;
    private String fullName;
    private long country;

    public Car_makers(long id, String maker, String fullName, long country) {
        this.id = id;
        this.maker = maker;
        this.fullName = fullName;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getCountry() {
        return country;
    }

    public void setCountry(long country) {
        this.country = country;
    }
}
