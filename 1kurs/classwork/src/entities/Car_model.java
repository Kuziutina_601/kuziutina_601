package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//ModelId,Maker,Model
public class Car_model {
    private int modelId;
    private int makerId;
    private String model;

    public Car_model(int modelId, int makerId, String model) {
        this.modelId = modelId;
        this.makerId = makerId;
        this.model = model;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getMakerId() {
        return makerId;
    }

    public void setMakerId(int makerId) {
        this.makerId = makerId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
