import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by пользователь on 21.03.2017.
 */
public class Task01_210317 {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("en_v1.dic"));
        HashMap<String, Integer> dictionary = new HashMap<>();
        String s;
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            String[] buf = s.split(" ");
            if (buf.length == 2) {
                dictionary.put(buf[0], Integer.parseInt(buf[1]));
            }
        }

        System.out.println(dictionary.toString());
    }

}
