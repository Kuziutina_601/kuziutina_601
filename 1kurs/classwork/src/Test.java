import db.DataBaseConnection;
import repositories.ContinentRepository;
import repositories.CountryRepository;

/**
 * Created by пользователь on 23.03.2017.
 */
public class Test {
    private static ContinentRepository continentRepository;
    private static CountryRepository countryRepository;
    public static void init() {
        continentRepository = new ContinentRepository(new DataBaseConnection());
        countryRepository = new CountryRepository(new DataBaseConnection());
    }
    public static ContinentRepository createContinentsRepo(){
        return new ContinentRepository(
                new DataBaseConnection()
        );
    }
    public static void main(String[] args) {
        ContinentRepository cr = createContinentsRepo();
        System.out.println(cr.getAllContinents());
        System.out.println(cr.getByName("america"));
    }
}
