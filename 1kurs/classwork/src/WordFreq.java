import java.util.Collections;

/**
 * Created by пользователь on 21.03.2017.
 */
public class WordFreq implements Comparable<WordFreq>{
    private String word;
    private int num;

    @Override
    public int compareTo(WordFreq o) {
        if (word.compareTo(o.getWord()) > 0) return 1;
        if (word.compareTo(o.getWord()) == 0) {
            if (num > o.getNum()) return 1;
            else if (num == o.getNum()) return 0;
            else return -1;
        }
        else return -1;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
