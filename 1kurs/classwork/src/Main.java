import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by пользователь on 23.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        List<WordFreq> ll = new ArrayList<>();
        Collections.sort(ll, new Comparator<WordFreq>() {
            @Override
            public int compare(WordFreq o1, WordFreq o2) {
                if (o1.getNum() > o2.getNum()) return 1;
                if (o1.getNum() == o2.getNum()) {
                    if (o1.getWord().compareTo(o2.getWord()) > 0) return 1;
                    else if (o1.getWord().compareTo(o2.getWord()) == 0) return 0;
                    else return -1;
                }
                else return -1;

            }
        });
    }
}
