import java.util.Arrays;
import java.util.Random;

/**
 * Created by пользователь on 18.03.2017.
 */
public class Main2 {
    public static void main(String[] args) {
        int[] array = new int[1000000];

        Random r = new Random();
        for (int i = 0; i < array.length; i++) array[i] =array.length - i ;

        System.out.println(Arrays.toString(array));
        System.out.println();
        long start = System.currentTimeMillis();
        MyIntrosort.sort(array);
        long end = System.currentTimeMillis();
        System.out.println();
        System.out.println(Arrays.toString(array));
        System.out.println(end - start);
    }
}
