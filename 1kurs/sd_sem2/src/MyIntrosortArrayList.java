import java.util.ArrayList;

/**
 * Created by пользователь on 19.03.2017.
 */
public class MyIntrosortArrayList {
    private static final int MIN_SIZE = 16;
    private static int count;
    private static int count_heap;
    private static int count3;

    public static void sort(ArrayList<Integer> array) {
        count = 0;
        count_heap = 0;
        count3 = 0;
        int depth = (int) ( 2 * Math.floor(Math.log(array.size())/Math.log(2)));
        introsort(array, 0, array.size()-1, depth);
        insertion_sort(array, 0, array.size());
        //System.out.println(Math.max(count, Math.max(count_heap, count3)));
    }

    private static void insertion_sort(ArrayList<Integer> array, int begin, int end) {
        //System.out.println("im in inser");
        for (int i = begin; i < end; i++){
            int k = i;
            int value = array.get(i);
            while (k != begin && array.get(k - 1) > value) {
                array.set(k, array.get(k - 1));
                k--;
                count3++;
                //System.out.println("insert");
            }
            array.set(k, value);
        }
    }

    public static void introsort (ArrayList<Integer> array, int begin, int end, int depth) {
        while (end - begin > MIN_SIZE) {
            //System.out.println(depth);
            if (depth == 0) {
                //heapsort copypast and adapt(((
                int n = end - begin;
                for (int i = n / 2; i >= 1; i--) { //build true array
                    int d = array.get(begin + i - 1);
                    int j = i;
                    while (j <= n / 2) {
                        int child = 2 * j;
                        if (child < n && array.get(begin + child - 1) < array.get(begin + child)) {
                            child++;
                        }
                        if (d > array.get(begin + child - 1)) {
                            break;
                        }
                        array.set(begin + j - 1, array.get(begin + child - 1));
                        j = child;
                    }
                    array.set(begin + j - 1, d);
                }
                for (int i = n; i > 1; i--) {
                    int t = array.get(begin);
                    array.set(begin, array.get(begin + i - 1));
                    array.set(begin + i - 1, t);
                    int d = array.get(begin + i - 1);
                    int j = 1;
                    int m = i - 1;
                    while (j <= m / 2) {
                        count_heap++;
                        int child = 2 * j;
                        if (child < m && array.get(begin + child - 1) < array.get(begin + child)) {
                            child++;
                        }
                        if (d >= array.get(begin + child - 1)) {
                            break;
                        }
                        array.set(begin + j - 1, array.get(begin + child - 1));
                        j = child;
                    }
                    array.set(begin + j - 1, d);
                }
                return;
            }
            depth--;
            int p = part(array, begin, end, medianOfThree(array, begin, end, (begin + (end - begin)/2)));
            introsort(array, p, end, depth);
            end = p;

        }
    }

    public static int medianOfThree(ArrayList<Integer> array, int begin, int end, int middle) {
        int start = array.get(begin);
        int tail = array.get(end);
        int mid = array.get(middle);
        if (start > mid) {
            if (start < tail) return start;
            else{
                if (tail < mid) return tail;
                else return mid;
            }

        }
        else {
            if (mid < tail) return mid;
            else {
                if (tail > start) return tail;
                else return start;
            }
        }
    }

    public static int part(ArrayList<Integer> array, int begin, int end, int basis) {
        while (begin < end) {
            while (array.get(begin) < basis) {
                count++;
                begin++;
            }
            while (array.get(end) > basis) end--;
            if (begin < end) {
                int t = array.get(begin);
                array.set(begin, array.get(end));
                array.set(end, t);

                end--;
                begin++;
            }
        }
        return begin;
    }
}
