import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by пользователь on 14.03.2017.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Random r = new Random();
        Scanner sc = new Scanner(new File("arrays.txt"));
        String buf = "";

        //int[] ar = new int[10000];
        for (int i = 0; i < 80; i++) {
            buf = sc.nextLine();
            String[] arr = buf.split(" ");
            int[] ar = new int[arr.length];
            //ArrayList<Integer> ar = new ArrayList<>();
            for (int j = 0; j < arr.length; j++) {
                //ar.add(Integer.parseInt(arr[j]));
                ar[j] = Integer.parseInt(arr[j]);
            }
            MyIntrosort.sort(ar);
            System.out.println(Arrays.toString(ar));
            boolean wrong = false;
            for (int j= 0; j < arr.length - 1 && !wrong; j++) {
                if (ar[j] > ar[j+1]) wrong = true;
            }
            if (wrong) {
                System.out.println("WRONG");
                break;
            }

        }
    }
}
