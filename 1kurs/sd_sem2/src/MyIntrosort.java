import java.util.regex.Matcher;

/**
 * Created by пользователь on 19.03.2017.
 */
public class MyIntrosort {
    private static final int MIN_SIZE = 16;
    private static int count;
    private static int count_heap;
    private static int count3;

    public static void sort(int[] array) {
        count = 0;
        count_heap = 0;
        count3 = 0;
        int depth = (int) ( 2 * Math.floor(Math.log(array.length)/Math.log(2)));
        introsort(array, 0, array.length - 1, depth);
        insertion_sort(array, 0, array.length);
        //System.out.println(Math.max(count, Math.max(count_heap, count3)));

    }

    private static void insertion_sort(int[] array, int begin, int end) {
        //System.out.println("im in inser");
        for (int i = begin; i < end; i++){
            int k = i;
            int value = array[i];
            while (k != begin && array[k - 1] > value) {
                array[k] = array[k - 1];
                k--;
                count3++;
                //System.out.println("insert");
            }
            array[k] = value;
        }
    }

    public static void introsort (int[] array, int begin, int end, int depth) {
        while (end - begin > MIN_SIZE) {
            //System.out.println(depth);
            if (depth == 0) {
                //heapsort copypast and adapt(((
                int n = end - begin;
                for (int i = n / 2; i >= 1; i--) { //build true array
                    int d = array[begin + i - 1];
                    int j = i;
                    while (j <= n / 2) {
                        int child = 2 * j;
                        if (child < n && array[begin + child - 1] < array[begin + child]) {
                            child++;
                        }
                        if (d > array[begin + child - 1]) {
                            break;
                        }
                        array[begin + j - 1] = array[begin + child - 1];
                        j = child;
                    }
                    array[begin + j - 1] = d;
                }
                for (int i = n; i > 1; i--) {
                    int t = array[begin];
                    array[begin] = array[begin + i - 1];
                    array[begin + i - 1] = t;
                    int d = array[begin + i - 1];
                    int j = 1;
                    int m = i - 1;
                    while (j <= m / 2) {
                        count_heap++;
                        int child = 2 * j;
                        if (child < m && array[begin + child - 1] < array[begin + child]) {
                            child++;
                        }
                        if (d >= array[begin + child - 1]) {
                            break;
                        }
                        array[begin + j - 1] = array[begin + child - 1];
                        j = child;
                    }
                    array[begin + j - 1] = d;
                }
                return;
            }
            depth--;
            int p = part(array, begin, end, medianOfThree(array, begin, end, (begin + (end - begin)/2)));
            introsort(array, p, end, depth);
            end = p;

        }
    }

    public static int medianOfThree(int[] array, int begin, int end, int middle) {
        if (array[begin] > array[middle]) {
            if (array[begin] < array[end]) return array[begin];
            else{
                if (array[end] < array[middle]) return array[end];
                else return array[middle];
            }

        }
        else {
            if (array[middle] < array[end]) return array[middle];
            else {
                if (array[end] > array[begin]) return array[end];
                else return array[begin];
            }
        }
    }

    public static int part(int[] array, int begin, int end, int basis) {
        while (begin < end) {
            while (array[begin] < basis) {
                count++;
                begin++;
            }
            while (array[end] > basis) end--;
            if (begin < end) {
                int t = array[begin];
                array[begin] = array[end];
                array[end] = t;

                end--;
                begin++;
            }
        }
        return begin;
    }
}
