package Tasks;

import repositories.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by пользователь on 20.04.2017.
 */
public class Task01 {
    private static ContinentRepository continentRepository;
    private static CountryRepository countryRepository;
    private static CarMakerRepository carMakerRepository;
    private static CarModelRepository carModelRepository;
    private static CarNameRepository carNameRepository;
    private static CarDataRepository carDataRepository;

    public static void main(String[] args) {
        init();
        List<String> kk = new ArrayList<>();
        kk.add("africa");
        kk.add("asia");
        System.out.println(countryRepository.getByContinentNames(kk));
    }


    public static void init() {
        continentRepository = new ContinentRepository();
        countryRepository = new CountryRepository();
        carMakerRepository = new CarMakerRepository();
        carModelRepository = new CarModelRepository();
        carNameRepository = new CarNameRepository();
        carDataRepository = new CarDataRepository();
    }

}
