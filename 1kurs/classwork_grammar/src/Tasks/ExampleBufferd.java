package Tasks;

import entities.CarData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by пользователь on 25.04.2017.
 */
public class ExampleBufferd {
    public static void main(String[] args) throws IOException {
        List<CarData> carDatas = new ArrayList<>();
        BufferedReader br = new BufferedReader(
                new FileReader("DataSets/cars-data.csv")
        );

        br.readLine();
        String s = br.readLine();
        while (s != null) {
            String[] strData = s.split(",");
            CarData carData = new CarData();
            carData.setId(Integer.parseInt(strData[0]));
            carData.setMpg(Double.parseDouble(strData[1]));
            carData.setCylinders(Integer.parseInt(strData[2]));
            carData.setEdispl(Double.parseDouble(strData[3]));
            carData.setHorsepower(Integer.parseInt(strData[4]));
            carData.setWeight(Integer.parseInt(strData[5]));
            carData.setAccelerate(Double.parseDouble(strData[6]));
            carData.setYear(Integer.parseInt(strData[7]));
            carDatas.add(carData);
            s = br.readLine();
        }

    }
}
