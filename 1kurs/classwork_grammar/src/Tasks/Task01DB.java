package Tasks;

import entities.CarMaker;
import entities.Continent;
import entities.Country;
import repositories.*;

import java.util.List;

/**
 * Created by пользователь on 18.04.2017.
 */
public class Task01DB {

    private static ContinentRepository continentRepository;
    private static CountryRepository countryRepository;
    private static CarMakerRepository carMakerRepository;
    private static CarModelRepository carModelRepository;
    private static CarNameRepository carNameRepository;
    private static CarDataRepository carDataRepository;

    public static void main(String[] args) {
        init();
       // System.out.println(carMakerRepository);
        //List<CarMaker> carMakers = carMakerRepository.getByContinent("asia");
//        List<CarMaker> names = carMakerRepository.getByContinent("asia");

//        for (CarMaker carMaker: names) {
//            System.out.println(carMaker.getFullName());
//        }
    }

    public static void init() {
        continentRepository = new ContinentRepository();
        countryRepository = new CountryRepository();
        carMakerRepository = new CarMakerRepository();
        carModelRepository = new CarModelRepository();
        carNameRepository = new CarNameRepository();
        carDataRepository = new CarDataRepository();
    }

}
