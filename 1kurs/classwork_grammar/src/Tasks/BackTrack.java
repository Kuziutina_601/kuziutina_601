package Tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by пользователь on 04.05.2017.
 */
public class BackTrack {
    public static List<Integer> varity;
    public static void main(String[] args) {
        varity = new ArrayList<>();
        varity.add(0);
        varity.add(1);
        varity.add(2);

        backTracking(new StringBuilder(), 5);
    }

    public static void backTracking(StringBuilder solution, int n) {
        if (solution.length() == n) {
            System.out.println(solution.toString());
            return;
        }
        int m = 0;
        for (int i = 0; i < solution.length(); i++) {
            if (solution.charAt(i) == '1') m++;
        }
        for (Integer k: varity) {
            if (k == 1 && m >= 2) {
                continue;
            }
            solution.append(k);
            backTracking(solution, n);
            solution.deleteCharAt(solution.length()-1);
        }
    }
}


