package Tasks;

import entities.CarData;
import repositories.*;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by пользователь on 25.04.2017.
 */
public class Task012504 {
    private static ContinentRepository continentRepository;
    private static CountryRepository countryRepository;
    private static CarMakerRepository carMakerRepository;
    private static CarModelRepository carModelRepository;
    private static CarNameRepository carNameRepository;
    private static CarDataRepository carDataRepository;

    public static void main(String[] args) throws FileNotFoundException, NoSuchFieldException {
        init();
        List<CarData> carDatas = carDataRepository.getAllCarData();
        PrintWriter pw = new PrintWriter(
                "cars-data-sorted.txt"
        );

        Collections.sort(carDatas, new Comparator<CarData>() {
            @Override
            public int compare(CarData o1, CarData o2) {
                if (o1.getMpg() > o2.getMpg()) return 1;
                else if (o1.getMpg() < o2.getMpg()) return -1;
                else return 0;
            }
        });

        Iterator<CarData> i = carDatas.iterator();
        while (i.hasNext()) {
            pw.println(i.next());
        }
        pw.close();

        Collections.sort(carDatas);

        CoolComparator c1 = new CoolComparator("mpg");
        CoolComparator c2 = new CoolComparator("Id");
        CoolComparator c3 = new CoolComparator("kfjskd");



    }

    public static void init() {
        continentRepository = new ContinentRepository();
        countryRepository = new CountryRepository();
        carMakerRepository = new CarMakerRepository();
        carModelRepository = new CarModelRepository();
        carNameRepository = new CarNameRepository();
        carDataRepository = new CarDataRepository();
    }

}
