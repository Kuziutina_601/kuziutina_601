package Tasks;

import java.util.List;

/**
 * Created by пользователь on 04.05.2017.
 */
public class Node {
    public int lable;
    public String info;
    public Node next;


    public List<Node> jointNodes;
    public int color;

    public Node(int lable) {
        this.lable = lable;
        color = -1;
    }

    public int getLable() {
        return lable;
    }

    public void setLable(int lable) {
        this.lable = lable;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public List<Node> getJointNodes() {
        return jointNodes;
    }

    public void setJointNodes(List<Node> jointNodes) {
        this.jointNodes = jointNodes;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
