package Tasks;

import java.util.*;

/**
 * Created by пользователь on 04.05.2017.
 */
public class Task010405 {
    public static void main(String[] args) {
        Node o1 = new Node(1);
        Node o2 = new Node(2);
        Node o3 = new Node(3);
        Node o4 = new Node(4);
        Node o5 = new Node(5);
        Node o6 = new Node(6);
        List<Node> j1 = new ArrayList<>();
        j1.add(o2);
        o1.setJointNodes(j1);
        List<Node> j2 = new ArrayList<>();
        j2.add(o1);
        j2.add(o4);
        j2.add(o3);
        o2.setJointNodes(j2);
        List<Node> j3 = new ArrayList<>();
        j3.add(o4);
        j3.add(o2);
        j3.add(o5);
        o3.setJointNodes(j3);
        List<Node> j4 = new ArrayList<>();
        j4.add(o2);
        j4.add(o3);
        j4.add(o5);
        j4.add(o6);
        o4.setJointNodes(j4);
        List<Node> j5 = new ArrayList<>();
        j5.add(o3);
        j5.add(o4);
        j5.add(o6);
        o5.setJointNodes(j5);
        List<Node> j6 = new ArrayList<>();
        j6.add(o4);
        j6.add(o5);
        o6.setJointNodes(j6);
        List<Node> nodes = new ArrayList<>();
        nodes.add(o1);
        nodes.add(o2);
        nodes.add(o3);
        nodes.add(o4);
        nodes.add(o5);
        nodes.add(o6);

        backTrack(nodes);

        for (int i = 0; i < nodes.size(); i++) {
            System.out.println(nodes.get(i).getColor());
        }

    }

    public static void backTrack(List<Node> k) {
        if (back(k, 0)) {
            System.out.println("good");
        }
        else {
            System.out.println("it's not real");
        }
    }

    public static boolean back(List<Node> k, int number) {
        for (int i = 0; i < 5; i++) {
            if (checkHasColor(k.get(number), i)) {
                k.get(number).setColor(i);
                if (number == k.size() - 1)
                    return true;
                if (back(k, number + 1)) {
                    return true;
                }
                k.get(number).setColor(-1);
            }
        }
        return false;
    }

//    public boolean back(Node k) {
//
//        for (int i = 0; i < 5; i++) {
//            if (checkHasColor(k, i)) {
//                k.setColor(i);
//                for (Node nn: k.getJointNodes()) {
//                    if (nn.getColor() == -1) {
//                        if (!back(nn)) {
//                            k.setColor(-1);
//                            break;
//                        }
//                    }
//                }
//                return true;
//            }
//        }
//        return false;
//    }

    public static boolean checkHasColor(Node m, int color) {
        for (int i = 0; i < m.getJointNodes().size(); i++) {
            Node neibo = m.getJointNodes().get(i);
            if (neibo.getColor() == color) {
                return false;
            }
        }
        return true;
    }
}
