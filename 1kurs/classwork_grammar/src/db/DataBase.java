package db;

import entities.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by пользователь on 23.03.2017.
 */
public class DataBase {
    private List<Country> countries;
    private List<Continent> continents;
    private List<CarMaker> carMakers;
    private List<CarModel> carModels;
    private List<CarName> carNames;
    private List<CarData> carDatas;

    public void init() {
        prepareContinents("DataSets/continents.csv");
        prepareCountries("DataSets/countries.csv");
        prepareCarMakers("DataSets/car-makers.csv");
        prepareCarModels("DataSets/model-list.csv");
        prepareCarNames("DataSets/car-names.csv");
        prepareCarData("DataSets/cars-data.csv");
    }

    private void prepareContinents(String fileName) {
        continents = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                Continent c = new Continent();
                c.setContinentId(Integer.parseInt(strData[0]));
                c.setContinent(strData[1].substring(1, strData[1].length()- 1));
                continents.add(c);
            }
            sc.close();
            System.out.println("continent db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    private void prepareCountries(String fileName) {

        countries = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                Country c = new Country();
                c.setCountryId(Integer.parseInt(strData[0]));
                c.setCountryName(strData[1].substring(1, strData[1].length()- 1));
                c.setContinentId(Integer.parseInt(strData[2]));
                c.setContinent(getContinentById(c.getContinentId()));
                countries.add(c);
            }
            sc.close();
            System.out.println("countries db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    private void prepareCarMakers(String fileName) {

        carMakers = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                CarMaker cm = new CarMaker();
                cm.setId(Integer.parseInt(strData[0]));
                cm.setMaker(strData[1].substring(1, strData[1].length()- 1));
                cm.setFullName(strData[2].substring(1, strData[2].length()- 1));
                cm.setCountryId(Integer.parseInt(strData[3]));
                cm.setCountry(getCountryById((int) cm.getCountryId()));
                carMakers.add(cm);
            }
            sc.close();
            System.out.println("carMakers db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    private void prepareCarModels(String fileName) {
        carModels = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                CarModel cm = new CarModel();
                cm.setId(Integer.parseInt(strData[0]));
                cm.setMakerId(Integer.parseInt(strData[1]));
                cm.setCarMaker(getCarMakerById(cm.getMakerId()));
                cm.setModelName(strData[2].substring(1, strData[2].length() - 1));
                carModels.add(cm);
            }
            sc.close();
            System.out.println("carModel db success");
        } catch (IOException e) {
            System.out.println("file not found, list is epmty");
        }
    }

    public void prepareCarNames(String fileName) {
        carNames = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                CarName cn = new CarName();
                cn.setId(Integer.parseInt(strData[0]));
                cn.setModel(strData[1].substring(1, strData[1].length()- 1));
                cn.setCarModel(getCarModelByName(cn.getModel()));
                cn.setMake(strData[2].substring(1, strData[2].length() - 1));
                carNames.add(cn);
            }
            sc.close();
            System.out.println("carName db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    public void prepareCarData(String fileName) {
        carDatas = new LinkedList<>();
        try {
            Scanner sc = new Scanner(new File(fileName));
            sc.nextLine();
            while (sc.hasNextLine()) {
                String[] strData = sc.nextLine().split(",");
                CarData carData = new CarData();
                carData.setId(Integer.parseInt(strData[0]));
                carData.setCarName(getCarNameById(carData.getId()));
                carData.setMpg(Double.parseDouble(strData[1]));
                carData.setCylinders(Integer.parseInt(strData[2]));
                carData.setEdispl(Double.parseDouble(strData[3]));
                carData.setHorsepower(Integer.parseInt(strData[4]));
                carData.setWeight(Integer.parseInt(strData[5]));
                carData.setAccelerate(Double.parseDouble(strData[6]));
                carData.setYear(Integer.parseInt(strData[7]));
                carDatas.add(carData);
            }
            sc.close();
            System.out.println("car data db success");
        } catch (IOException e) {
            System.out.println("file not found, list is empty");
        }
    }

    public List<CarName> getAllCarNames() {return carNames;}

    public List<Continent> getAllContinents() {
        return continents;
    }

    public List<Country> getAllCountries() {
        return countries;
    }

    public List<CarMaker> getAllCarMakers() {return carMakers;}

    public List<CarModel> getAllCarModels() {return carModels;}

    public List<CarData> getAllCarData() {return carDatas;}

    public Continent getContinentById(int id) {
        for (Continent continent: getAllContinents()) {
            if (continent.getContinentId() == id) {
                return continent;
            }
        }
        return null;
    }

    public Country getCountryById(int id) {
        for (Country country: getAllCountries()) {
            if (country.getCountryId() == id) {
                return country;
            }
        }
        return null;
    }

    public CarMaker getCarMakerById(int id) {
        for (CarMaker carMaker: getAllCarMakers()) {
            if (carMaker.getId() == id) {
                return carMaker;
            }
        }
        return null;
    }

    public CarModel getCarModelById(int id) {
        for (CarModel carModel: getAllCarModels()) {
            if (carModel.getId() == id) {
                return carModel;
            }
        }
        return null;
    }

    public CarModel getCarModelByName(String name) {
        for (CarModel carModel: getAllCarModels()) {
            if (carModel.getModelName().equals(name)) {
                return carModel;
            }
        }
        return null;
    }

    public CarName getCarNameById(int id) {
        for (CarName carName: getAllCarNames()) {
            if (carName.getId() == id) {
                return carName;
            }
        }
        return null;
    }

}
