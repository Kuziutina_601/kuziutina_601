import entities.Continent;
import entities.Country;
import repositories.CarMakerRepository;
import repositories.ContinentRepository;
import repositories.CountryRepository;

/**
 * Created by пользователь on 17.04.2017.
 */
public class Main {
    private static ContinentRepository continentRepository;
    private static CountryRepository countryRepository;
    private static CarMakerRepository carMakerRepository;

    public static void main(String[] args) {
        init();
        System.out.println(continentRepository.getAllContinents());
        System.out.println(countryRepository.getAllCountry());
        System.out.println(carMakerRepository.getAllCarMakers());
        Continent continent  =continentRepository.getByName("africa");
        Country country = countryRepository.getByName("usa");
        country.setContinent(continent);
    }

    public static void init() {
        continentRepository = new ContinentRepository();
        countryRepository = new CountryRepository();
        carMakerRepository = new CarMakerRepository();
    }

}
