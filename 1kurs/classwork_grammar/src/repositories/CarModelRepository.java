package repositories;

import db.DataBaseConnection;
import entities.CarModel;

import java.util.List;

/**
 * Created by пользователь on 19.04.2017.
 */
public class CarModelRepository {

    public List<CarModel> getAllCarModels() {return DataBaseConnection.getDataBaseConnection().getAllCarModels();}

    public CarModel getByModelName (String name) {
        return getAllCarModels().stream()
                .filter((x) -> x.getModelName().equals(name))
                .findAny().orElse(null);
//        for (CarModel carModel: DataBaseConnection.getDataBaseConnection().getAllCarModels()) {
//            if (name.equals(carModel.getModelName())) return carModel;
//        }
//        return null;
    }
}
