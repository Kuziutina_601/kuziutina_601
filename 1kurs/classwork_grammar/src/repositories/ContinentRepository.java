package repositories;

import db.DataBaseConnection;
import entities.Continent;

import java.util.List;

/**
 * Created by пользователь on 23.03.2017.
 */
public class ContinentRepository {

    public List<Continent> getAllContinents() {
        return DataBaseConnection.getDataBaseConnection().getAllContinents();
    }

    public Continent getById(int id) {
        return DataBaseConnection.getDataBaseConnection().getContinentById(id);
    }

    public Continent getByName (String name) {
        return getAllContinents().stream()
                .filter((x) -> x.getContinent().equals(name))
                .findAny().orElse(null);
//        for (Continent continent: DataBaseConnection.getDataBaseConnection().getAllContinents()) {
//            if (name.equals(continent.getContinent())) return continent;
//        }
//        return null;
    }
}
