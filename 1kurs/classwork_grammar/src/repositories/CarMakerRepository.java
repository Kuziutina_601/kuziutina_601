package repositories;

import db.DataBaseConnection;
import entities.CarMaker;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by пользователь on 18.04.2017.
 */
public class CarMakerRepository {

    public List<CarMaker> getAllCarMakers() {
        return DataBaseConnection.getDataBaseConnection().getAllCarMakers();
    }


    public CarMaker getByFullName (String name) {
        return getAllCarMakers().stream().filter((x) -> x.getFullName().equals(name))
                .findAny().orElse(null);
//        for (CarMaker carMaker: DataBaseConnection.getDataBaseConnection().getAllCarMakers()) {
//            if (name.equals(carMaker.getFullName())) return carMaker;
//        }
//        return null;
    }

    public List<CarMaker> getByCountryName(List<String> countryName) {
        return getAllCarMakers().stream()
                .filter((x) -> countryName.contains(x.getCountry().getCountryName()))
                .collect(Collectors.toList());
//        List<CarMaker> result = new ArrayList<>();
//        for (CarMaker carMaker: DataBaseConnection.getDataBaseConnection().getAllCarMakers()){
//            if (countryName.contains(carMaker.getCountry().getCountryName())) {
//                result.add(carMaker);
//            }
//        }
//        return result;
    }

    public List<String> getByContinent(String continentName) {
//        List<CarMaker> result = new ArrayList<>();
//        for (CarMaker carMaker: DataBaseConnection.getDataBaseConnection().getAllCarMakers()) {
//            if (continentName.equals(carMaker.getCountry().getContinent().getContinent())) {
//                result.add(carMaker);
//            }
//        }
//        return result;

        return getAllCarMakers().stream()
                .filter(
                        x -> x.getCountry().getContinent().getContinent().equals(continentName)
                ).map(CarMaker::getFullName)
                .collect(Collectors.toList());


    }

}
