package repositories;

import db.DataBaseConnection;
import entities.CarName;
import entities.Country;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by пользователь on 19.04.2017.
 */
public class CarNameRepository {
    public List<CarName> getAllCarNames() {
        return DataBaseConnection.getDataBaseConnection().getAllCarNames();
    }


}

