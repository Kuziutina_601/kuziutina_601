package repositories;

import db.DataBaseConnection;
import entities.CarName;
import entities.Country;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by пользователь on 23.03.2017.
 */
public class CountryRepository {

    public List<Country> getAllCountry() {
        return DataBaseConnection.getDataBaseConnection().getAllCountries();
    }


    public Country getByName (String name) {
        return getAllCountry().stream()
                .filter((x) -> x.getCountryName().equals(name))
                .findAny().orElse(null);
//        for (Country country: DataBaseConnection.getDataBaseConnection().getAllCountries()) {
//            if (name.equals(country.getCountryName())) return country;
//        }
//        return null;
    }

    public List<Country> getByContinentNames(List<String> continentsName) {
        return getAllCountry().stream()
                .filter((x) -> continentsName.contains(x.getContinent().getContinent()))
                .collect(Collectors.toList());
//        List<Country> result = new ArrayList<>();
//        for (Country country: DataBaseConnection.getDataBaseConnection().getAllCountries()){
//            if (continentsName.contains(country.getContinent().getContinent())) {
//                result.add(country);
//            }
//        }
//        return result;
    }

    public Country getBest() {
        List<Country> country = new ArrayList<>();
        List<Integer> count = new ArrayList<>();
        for (CarName carName: DataBaseConnection.getDataBaseConnection().getAllCarNames()) {
            Country coun = carName.getCarModel().getCarMaker().getCountry();
            if (country.contains(coun)) {
                int id = country.indexOf(coun);
                count.set(id , count.get(id) + 1);
            }
            else {
                country.add(coun);
                count.add(1);
            }
        }

        int max = -1;
        Country country1 = null;
        for (int i = 0; i < count.size(); i++) {
            if (max < count.get(i)) {
                max = count.get(i);
                country1 = country.get(i);
            }
        }
        return country1;
    }


    //public  getById(int id) {
   //     return dbConnection.getDataBaseConnection().getById(id);
    //}

}
