package Threds;

/**
 * Created by пользователь on 11.05.2017.
 */
public class ThreadExample extends Thread{

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(getName() + "  " + getPriority() + "  " + i);
        }
    }

    public static void main(String[] args) {
        (new ThreadExample()).start();
    }
}
