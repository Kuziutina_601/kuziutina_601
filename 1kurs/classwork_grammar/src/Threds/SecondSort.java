package Threds;

import java.util.Arrays;

/**
 * Created by пользователь on 11.05.2017.
 */
public class SecondSort extends ThreadSort {

    public SecondSort(int[] array, int l, int r) {
        super(array, l, r, "2");
    }


    private void sorting(int[] ar, int l, int r) {
        Arrays.sort(ar, l, r);
    }

    @Override
    public void run() {
        sorting(getArray(), getL(), getR());
    }
}
