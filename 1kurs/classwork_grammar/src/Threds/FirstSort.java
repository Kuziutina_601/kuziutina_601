package Threds;

import java.util.Arrays;

/**
 * Created by пользователь on 11.05.2017.
 */
public class FirstSort extends ThreadSort {
    public FirstSort(int[] array, int l, int r) {
        super(array, l, r, "1");
    }


    private void sorting(int[] ar, int l, int r) {
        Arrays.sort(ar, l, r);
    }

    public void run() {
        System.out.println("1");
        sorting(getArray(),getL(),getR());
    }
}
