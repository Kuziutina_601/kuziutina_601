package Threds;

/**
 * Created by пользователь on 11.05.2017.
 */
public class ThreadSort extends Thread{
    private int[] array;
    private int l;
    private int r;
    private String v;

    public ThreadSort(int[] array, int l, int r, String v) {
        this.array = array;
        this.l = l;
        this.r = r;
        this.v = v;
    }
    public void run() {
        if (v.equals("1")) {
            FirstSort f = new FirstSort(array, l, r);
            f.start();
            try {
                f.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        else if (v.equals("2")) {
            SecondSort s = new SecondSort(array, l, r);
            s.start();
            try {
                s.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public int[] getArray() {
        return array;
    }

    public int getL() {
        return l;
    }

    public int getR() {
        return r;
    }
}
