package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//Id,MPG,Cylinders,Edispl,Horsepower,Weight,Accelerate,Year
public class CarData implements Comparable{
    private int Id;
    private double mpg;
    private int cylinders;
    private double edispl;
    private int horsepower;
    private int weight;
    private double accelerate;
    private int year;
    private CarName carName;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getMpg() {
        return mpg;
    }

    public void setMpg(double mpg) {
        this.mpg = mpg;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public double getEdispl() {
        return edispl;
    }

    public void setEdispl(double edispl) {
        this.edispl = edispl;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(double accelerate) {
        this.accelerate = accelerate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public CarName getCarName() {
        return carName;
    }

    public void setCarName(CarName carName) {
        this.carName = carName;
    }

    @Override
    public String toString() {
        return "CarData{" +
                "Id=" + Id +
                ", mpg=" + mpg +
                ", cylinders=" + cylinders +
                ", edispl=" + edispl +
                ", horsepower=" + horsepower +
                ", weight=" + weight +
                ", accelerate=" + accelerate +
                ", year=" + year +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof CarData)) throw new NullPointerException("it's not car data");
        CarData cd = (CarData) o;
        if (this.getId() > cd.getId()) return 1;
        else if (this.getId() < cd.getId()) return -1;
        else return 0;
    }
}
