package entities;

import db.DataBaseConnection;

/**
 * Created by пользователь on 23.03.2017.
 */

//Id,Model,Make
public class CarName {
    private int id;
    private String model;
    private String make;
    private CarModel carModel;

    public CarName() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }


    @Override
    public String toString() {
        return "CarName{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", make='" + make + '\'' +
                '}';
    }
}
