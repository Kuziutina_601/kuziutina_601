package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//ContId,entities.Continent
public class Continent {
    private int continentId;
    private String continent;

    public Continent() {}

    public String toString() {
        return "Continent: " +
                "continentId " + continentId +
                ", continent " + continent;
    }

    public int getContinentId() {
        return continentId;
    }

    public void setContinentId(int contId) {
        this.continentId = contId;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }
}
