package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//Id,Maker,FullName,entities.Country
public class CarMaker {
    private long id;
    private String maker;
    private String fullName;
    private long countryId;
    private Country country;

    public CarMaker() {
    }

    public CarMaker(long id, String maker, String fullName, long country) {
        this.id = id;
        this.maker = maker;
        this.fullName = fullName;
        this.countryId = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    @Override
    public String toString() {
        return "CarMaker{" +
                "id=" + id +
                ", maker='" + maker + '\'' +
                ", fullName='" + fullName + '\'' +
                ", countryId=" + countryId +
                '}';
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
