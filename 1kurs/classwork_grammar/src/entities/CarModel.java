package entities;

/**
 * Created by пользователь on 23.03.2017.
 */

//ModelId,Maker,Model
public class CarModel {
    private int id;
    private int makerId;
    private String modelName;
    private CarMaker carMaker;

    public CarModel() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMakerId() {
        return makerId;
    }

    public void setMakerId(int makerId) {
        this.makerId = makerId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public CarMaker getCarMaker() {
        return carMaker;
    }

    public void setCarMaker(CarMaker carMaker) {
        this.carMaker = carMaker;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "id=" + id +
                ", makerId=" + makerId +
                ", modelName='" + modelName + '\'' +
                '}';
    }
}
