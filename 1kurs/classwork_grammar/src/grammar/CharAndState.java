package grammar;

/**
 * Created by пользователь on 13.04.2017.
 */
public class CharAndState implements Comparable<CharAndState>{
    private Character c;
    private State state;

    public CharAndState(Character c, State state) {
        this.c = c;
        this.state = state;
    }

    public CharAndState(Character c) {
        this.c = c;
    }

    public CharAndState(State state) {
        this.state = state;
    }

    public CharAndState() {
    }

    public Character getChar() {
        return c;
    }

    public void setChar(Character c) {
        this.c = c;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CharAndState) {
            CharAndState cas = (CharAndState) o;
            return (cas.getChar() == c && cas.getState().equals(state));
        }
        else return false;
    }

    @Override
    public int compareTo(CharAndState o) {
        if (c != o.getChar()) {
            return c.compareTo(o.getChar());
        }
        else {
            return state.getState().compareTo(o.getState().getState());
        }
    }

    @Override
    public String toString() {
        return "grammar.CharAndState{" +
                "c=" + c +
                ", state=" + state +
                '}';
    }
}
