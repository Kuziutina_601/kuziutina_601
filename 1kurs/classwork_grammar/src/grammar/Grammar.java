package grammar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Grammar {
    private String filename = "grammar.txt";
    private Map<String, List<RightSideElem>> productions;
    private String startNonterminal;
    private String terminals;
    private List<String> nonterminals;

    public Grammar() {
        readGrammar(filename);
    }

    private void readGrammar(String filename) {
        try {
            Scanner sc = new Scanner(new File(filename));
            terminals = sc.nextLine();
            nonterminals = Arrays.asList(sc.nextLine().split(" "));
            startNonterminal = sc.nextLine();
            productions = new HashMap<>();
            while (sc.hasNextLine()) {
                String[] args = sc.nextLine().split("->");
                String[] rightSides = args[1].split("\\|");

                List<RightSideElem> list = Arrays.stream(rightSides)
                        .map((x) -> new RightSideElem(
                                    x.charAt(0),
                                    x.substring(1)
                        ))
                        .collect(Collectors.toList());
                productions.put(args[0], list);
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "grammar.Grammar{" +
                "productions=" + productions +
                ", startNonterminal='" + startNonterminal + '\'' +
                ", terminals='" + terminals + '\'' +
                ", nonterminals=" + nonterminals +
                '}';
    }

    public String getStartNonterminal() {
        return startNonterminal;
    }

    public Map<String, List<RightSideElem>> getProductions() {
        return productions;
    }
}
