package grammar;

public class InputAndState {
    private String state;
    private Character goTerminal;

    public InputAndState(String state, Character goTerminal) {
        state = state;
        this.goTerminal = goTerminal;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        state = state;
    }

    public Character getGoTerminal() {
        return goTerminal;
    }

    public void setGoTerminal(Character goTerminal) {
        this.goTerminal = goTerminal;
    }

    @Override
    public int hashCode() {
        return state.hashCode() + (int) goTerminal;
    }

    @Override
    public boolean equals(Object o) {
        InputAndState k = (InputAndState) o;
        return k.getGoTerminal().equals(goTerminal) && k.getState().equals(state);
    }
}
