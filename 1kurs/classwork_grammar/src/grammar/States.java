package grammar;

import java.util.*;

/**
 * Created by пользователь on 13.04.2017.
 */
public class States extends TreeSet<State> implements Comparable<States>{


    @Override
    public int compareTo(States o) {
        if (Arrays.equals(this.toArray(), o.toArray())) return 0;
        else
            return 1;
    }
}
