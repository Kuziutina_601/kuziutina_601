package grammar;

import java.util.*;

/**
 * Created by пользователь on 13.04.2017.
 */
public class DFA {
    private Map<CharAndStates, States> transition = new TreeMap<>();
    private String alphabet;
    private States startState;
    private States endState;

    public DFA(NFA nfa) {
        startState = new States();
        startState.add(nfa.getStartState());
        Queue<States> q = new LinkedList<>();
        q.offer(startState);
        Map<CharAndState, List<State>> nfaTr = nfa.getTransitions();
        while (!q.isEmpty()) {
            States from = q.poll();
            States to = new States();
            for (int i = 0; i < alphabet.length(); i++) {
                Character c = alphabet.charAt(i);
                for (State state: from) {
                    CharAndState cas = new CharAndState();
                    List<State> lst = nfaTr.get(cas);
                    if (lst == null)
                        continue;
                    for (State s : lst) {
                        to.add(s);
                    }
                }
                CharAndStates cass = new CharAndStates(c, from);
                transition.put(cass, to);
                boolean yes = false;
                for (CharAndStates cass2 : transition.keySet()) {
                    if (cass2.getStates().compareTo(to) == 0) {
                        yes = true;
                        break;
                    }
                }
                if (!yes) {
                    q.offer(to);
                }
            }

        }
    }
}
