package grammar;

import java.util.*;

public class NFA {

    private Map<CharAndState, List<State>> transitions;
    private State finalState = new State("$FRF@#$".hashCode() + "");
    private State startState;
    private String alphabet;

    public NFA(Grammar g) {
        //alphabet = g.get;
        transitions = new TreeMap<>();
        startState = new State(g.getStartNonterminal());

        Map<String, List<RightSideElem>> prods = g.getProductions();
        for (String leftSide : prods.keySet()) {
            State from = new State(leftSide);
            for (RightSideElem rse : prods.get(leftSide)) {
                Character c = rse.getTerminal();
                State to;
                CharAndState cas = new CharAndState(c, from);
                if (rse.getNonterminal().equals("")) {
                    to = finalState;
                }
                else {
                    to = new State(rse.getNonterminal());
                }
                if (!transitions.containsKey(cas)) {
                    transitions.put(cas, new ArrayList<>());
                }
                transitions.get(cas).add(to);
            }
        }
    }

//    private Map<grammar.InputAndState, List<grammar.State>> transistions;
//    private grammar.State finalState = new grammar.State("$%@KJS#1".hashCode() + "");
//    private grammar.State startState;
//
//    public grammar.NFA(grammar.Grammar g) {
//
//        transistions = new HashMap<>();
//        startState = new grammar.State(g.getStartNonterminal());
//        grammar.InputAndState buf;
//        Map<String, List<grammar.RightSideElem>> prods = g.getProductions();
//        for (String leftSide : prods.keySet()) {
//            for (grammar.RightSideElem rse : prods.get(leftSide)) {
//                String non = rse.getNonterminal();
//                buf = new grammar.InputAndState(leftSide, rse.getTerminal());
//                if (non.equals("")) {
//                    non = finalState;
//                }
//
//                if (transistions.containsKey(buf)) {
//                    transistions.get(buf).add(non);
//                }
//                else {
//                    List<String> buf2 = new LinkedList<>();
//                    buf2.add(non);
//                    transistions.put(buf, buf2);
//                }
//            }
//        }
//    }


    public Map<CharAndState, List<State>> getTransitions() {
        return transitions;
    }

    public void setTransitions(Map<CharAndState, List<State>> transitions) {
        this.transitions = transitions;
    }

    public State getFinalState() {
        return finalState;
    }

    public void setFinalState(State finalState) {
        this.finalState = finalState;
    }

    public State getStartState() {
        return startState;
    }

    public void setStartState(State startState) {
        this.startState = startState;
    }

    @Override
    public String toString() {
        return "grammar.NFA{" +
                "transitions=" + transitions +
                ", finalState=" + finalState +
                ", startState=" + startState +
                '}';
    }
}
