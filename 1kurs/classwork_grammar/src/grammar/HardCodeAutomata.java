package grammar;

import java.util.Arrays;

/**
 * Created by пользователь on 10.04.2017.
 */
public class HardCodeAutomata {
    public boolean checkByAutomata (int[][] transitions, int[] finalState, String inpute) {
        int buf;
        int state = 1;
    	for (int i = 0; i < inpute.length(); i++) {
    	    buf = inpute.charAt(i) - '0';
            state = transitions[buf][state];
        }
        boolean has = false;
        for (int i = 0; i < finalState.length && !has; i++) {
            if (finalState[i] == state) has = true;
        }
        return has;
    }
}
