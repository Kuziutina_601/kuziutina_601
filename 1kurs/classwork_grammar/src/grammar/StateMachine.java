package grammar;

/**
 * Created by пользователь on 10.04.2017.
 */
public class StateMachine {
    public boolean checkByStatesTask1(String input) {
        boolean result = false;
        int state = 1;
        int buf;

        for (int i = 0; i < input.length() && state != 4; i++) {
            buf = input.charAt(i) - '0';
            if (state == 1) state = 2;
            else if (state == 2 ) {
                if (buf == 0) state = 3;
                else state = 4;
            }
            else if (state == 3) {
                if (buf == 1) state = 4;
            }
        }

        return state == 3;
    }

    public boolean checkByStatesTask2(String input) {
        boolean result = false;
        int state = 1;
        int buf;

        for (int i = 0; i < input.length() && state != 5; i++) {
            buf = input.charAt(i) - '0';
            if (state == 1) {
                if (buf == 1) state = 2;
                else state = 5;
            }
            else if (state == 2) {
                if (buf == 1) state = 3;
                else state = 5;
            }
            else if (state == 3) {
                if (buf == 0) state = 4;
            }
            else if (state == 4) {
                if (buf == 1) state = 5;
            }
        }
        return state == 3 || state == 4;
    }
}