package ChatWithFile;

import java.io.*;
import java.util.Scanner;

/**
 * Created by пользователь on 15.05.2017.
 */
public class Member extends Thread {
    private String filename = "buffer";
    private String comm_file1 = "comm_file1";
    private String comm_file2 = "comm_file2";
    private int x;
    private void updateState(int n) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            fos.write(n);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void waiting() {
        try {
            Thread.sleep(1000);
            System.out.println("i wait second member");
            x = readState();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private int readState() {
        BufferedInputStream bis = null;
        int result = 0;
        try {
            bis = new BufferedInputStream(
                    new FileInputStream(filename)
            );
            result = bis.read();
            bis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void run() {
        x = 0;
        try {
            x = readState();

            if (x == 0) {
                updateState(1);
                x = readState();

                while (x == 1) {
                    waiting();
                }
                BufferedReader br1 = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(comm_file2)
                        )
                );

                while (true) {
                    /////////////////////////////////////////////////////

                    Scanner sc = new Scanner(System.in);
                    String message = sc.nextLine();
                    if (message.equals("exit")) {
                        break;
                    }
                    PrintWriter pw = new PrintWriter(
                            new FileOutputStream(comm_file1, true)
                    );
                    pw.print(message);
                    pw.close();
                    String input = br1.readLine();
                    while (input == null) {
                        if (readState() == 0) {
                            break;
                        }
                        br1.close();
                        Thread.sleep(1000);
                        br1 = new BufferedReader(
                                new InputStreamReader(
                                        new FileInputStream(comm_file2)
                                )
                        );
                        input = br1.readLine();
                    }
                    if (readState() == 0 || input.equals("exit")) {
                        break;
                    }
                    System.out.println("Guest: " + input);
                    PrintWriter pw2 = new PrintWriter(comm_file2);
                    pw2.close();
                }

            } else if (x == 1) {
                updateState(2);
                System.out.println("good, i give my partner");
                BufferedReader br1 = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(comm_file1)
                        )
                );
                while (true) {

                    String input = br1.readLine();
                    while (input == null) {
                        if (readState() == 0) {
                            break;
                        }
                        br1.close();
                        Thread.sleep(1000);
                        br1 = new BufferedReader(
                                new InputStreamReader(
                                        new FileInputStream(comm_file1)
                                )
                        );
                        input = br1.readLine();
                    }
                    if (readState() == 0 || input.equals("exit")) {
                        break;
                    }
                    System.out.println("Guest2: " + input);
                    PrintWriter pw = new PrintWriter(comm_file1);
                    pw.close();
                    Scanner sc = new Scanner(System.in);
                    String message = sc.nextLine();
                    if (message.equals("exit")) {
                        break;
                    }
                    PrintWriter pw2 = new PrintWriter(
                            new FileOutputStream(comm_file2, true)
                    );
                    pw2.print(message);
                    pw2.close();
                }
            } else if (x == 2) {
                System.out.println("occupied");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("end chat");
            updateState(0);
        }

    }

    public static void main(String[] args) {
        new Member().start();
    }

}
