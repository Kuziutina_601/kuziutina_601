import java.util.List;

/**
 * Created by пользователь on 22.05.2017.
 */
public class State {
    private int number;
    private List<State> statList;

    public State(int number, List<State> statList) {
        this.number = number;
        this.statList = statList;
    }

    public State(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<State> getStatList() {
        return statList;
    }

    public void setStatList(List<State> statList) {
        this.statList = statList;
    }
}
