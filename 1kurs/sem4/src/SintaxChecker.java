import java.util.List;

/**
 * Created by пользователь on 22.05.2017.
 */
public class SintaxChecker {

    public static void main(String[] args) {
        String s = "x := (+00000  ,-66666, +76889); 56 := fsdf/(+47564,-34765,+65375);.";
        int now = 0;
        int i = 0;
        while (now != -2 && now != -1) {
            System.out.println(i + "   " + now );
            if (now == 0) {
                i = skipZero(s, i);
                if (s.charAt(i) == '.') {
                    now = -1;
                }
                else if ((s.charAt(i) >= 'a' && s.charAt(i) <= 'z') || (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')) {
                    now = 1;
                    i++;
                }
                else {
                    now = -2;
                    break;
                }
            }
            else if (now == 1) {
                i = checkIden(s, i);
                i = skipZero(s, i);
                if (s.charAt(i) == ';') {
                    now = 0;
                }
                else if (s.charAt(i) == ':') {
                    i++;
                    if (s.charAt(i) == '=') {
                        now = 3;
                        i++;
                    }
                    else {
                        now = -2;
                        break;
                    }
                }
                else {
                    now = -2;
                    break;
                }
            }
            else if (now == 3) {
                i = skipZero(s, i);
                if ((s.charAt(i) >= 'a' && s.charAt(i) <= 'z') || (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')) {
                    i++;
                    i = checkIden(s, i);
                    i = skipZero(s, i);

                }
                else if (s.charAt(i) == '(') {
                    i = checkArray(s, i);
                    if (i == -1) {
                        now = -2;
                        break;
                    }
                }
                i = skipZero(s, i);
                if (s.charAt(i) == ';') {
                    now = 0;
                    i++;
                }
                else if (s.charAt(i) == '+' || s.charAt(i) == '-'
                        || s.charAt(i) == '/' || s.charAt(i) == '*') {
                    now = 7;
                    i++;
                }
                else {
                    now = -2;
                    break;
                }
            }
            else if (now == 7) {
                i = skipZero(s, i);
                if ((s.charAt(i) >= 'a' && s.charAt(i) <= 'z') || (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')) {
                    i++;
                    i = checkIden(s, i);
                    i = skipZero(s, i);

                }
                else if (s.charAt(i) == '(') {
                    i = checkArray(s, i);
                    if (i == -1) {
                        now = -2;
                        break;
                    }
                }
                i = skipZero(s, i);
                if (s.charAt(i) == ';') {
                    now = 0;
                    i++;
                }
                else {
                    now = -2;
                    break;
                }
            }
        }
        if (now == -2) {
            System.out.println("Exception ...");
        }
        else {
            System.out.println("Sintax - good");
        }

        System.out.println(now);
    }

    public static int checkIden(String s, int k) {
        while ( ((s.charAt(k) >= 'a' && s.charAt(k) <= 'z') || (s.charAt(k) >= 'A' && s.charAt(k) <= 'Z'))
                || (s.charAt(k) >= '0' && s.charAt(k) <= '9') ) {
            k++;
        }
        return k;
    }

    public static int skipZero(String s, int k) {
        while (s.charAt(k) == ' ') {
            k++;
        }
        return k;
    }

    public static int checkArray(String s, int k) {
        System.out.println("i go in check array    " + k);
        k++;
        for (int i = 0; i < 3 && k!= -1; i++) {
            k = skipZero(s, k);
            if (s.charAt(k) == '+' || s.charAt(k) == '-') {
                k++;
                for (int j = 0; j < 5 && k != -1; j++) {
                    if (s.charAt(k) < '0' || s.charAt(k) > '9') {
                        k = -2;
                    }
                    k++;
                }
                if (k == -1) break;
                k = skipZero(s, k);
                if (i != 2) {
                    if (s.charAt(k) != ',') k = -1;
                    else k++;
                }
                else {
                    if (s.charAt(k) != ')') k = -1;
                    else k++;
                }
            }
            else k = -1;
        }
        return k;
    }

}
