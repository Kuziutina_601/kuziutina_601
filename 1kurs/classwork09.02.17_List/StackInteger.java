/**
 * Created by пользователь on 16.02.2017.
 */
public class StackInteger implements IStack {

    private Elem head;
    private Elem tail;

    public StackInteger() {
        this.head = null;
        this.tail = null;
    }

    @Override
    public void push(int x) {
        head = new Elem(x, head);
    }

    @Override
    public int pop() {
        int c;
        if (head == null) throw new NullPointerException();
        else {
            c = head.getValue();
            head = head.getNext();
        }
        return c;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
