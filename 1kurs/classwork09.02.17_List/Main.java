import java.util.Scanner;

/**
 * Created by пользователь on 09.02.2017.
 */
public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String s = sc.next();
        String[] part = s.split(",");
        int buf, a, b;;

        //buf = Integer.parseInt(s);

        StackInteger st = new StackInteger();
        for (int i = 0; i < part.length; i++) {
            try {
                buf = Integer.parseInt(part[i]);
                st.push(buf);
            }
            catch (NumberFormatException e) {
                a = st.pop();
                b = st.pop();
                if (part[i].equals("+")){
                    st.push(a + b);
                }
                else if (part[i].equals("-")) {
                    st.push(a - b);
                }
                else if (part[i].equals("*")) {
                    st.push(a * b);
                }
                else if (part[i].equals("/")) {
                    st.push(a/b);
                }
            }
        }
        System.out.println(st.pop());
    }
}
