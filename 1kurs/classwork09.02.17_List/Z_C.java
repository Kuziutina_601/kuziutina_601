import java.util.Enumeration;
import java.util.Scanner;

/**
 * Created by пользователь on 13.02.2017.
 */
public class Z_C {

    public static Scanner sc;

    public static void main(String[] args) {

        Elem p;
        boolean has = false;
        sc = new Scanner(System.in);

        p = createList(4);

        while(p != null && !has) {
            if (p.getValue() < 0) {
                System.out.println("YES");
                has = true;
            }
            p = p.getNext();
        }

        if (!has)
            System.out.println("NO");
    }

    public static Elem createList (int n) {
        Elem p;
        Elem head = null;
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            p.setNext(head);
            head = p;
        }

        return head;
    }
}
