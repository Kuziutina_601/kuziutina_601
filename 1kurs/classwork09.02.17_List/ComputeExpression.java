import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by пользователь on 28.02.2017.
 */
public class ComputeExpression {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String s = sc.nextLine();


    }

    public static int computeExpression(String s) {
        String line = represent(s);
        System.out.println(s);
        //return compute(line);
        return 0;
    }

    public static String represent(String s) {
        String result = "";
        boolean has = false;
        int count = 0;
        int start = 0;
        ArrayList<Integer> qq = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                count++;
            else if (s.charAt(i) == ')')
                count--;
            else if ((s.charAt(i) == '+' || s.charAt(i) == '-') && count == 0){
                has = true;
                result += s.charAt(i) + represent(s.substring(start, i-1));
                start = i + 1;
            }
            else if ((s.charAt(i) == '*' || s.charAt(i) == '/') && count == 0) {
                qq.add(i);
            }
        }
        if (has)
            result += represent(s.substring(start, s.length() - 1));
        else {
            if (qq.isEmpty()) {
                if (s.charAt(0) == '(')
                    result += represent(s.substring(1, s.length() - 2));
                else {
                    for (int i = 0; i < s.length(); i++) {
                        result += s.charAt(i);
                    }
                }
            }
            else {
                start = 0;
                for (int i = 0; i < qq.size(); i++) {
                    if (s.charAt(start) == '(')
                        result += s.charAt(qq.get(i)) +  represent(s.substring(start + 1, qq.get(i) - 2));
                    else
                        result += s.charAt(qq.get(i)) + represent(s.substring(start, qq.get(i) - 1));
                    start = qq.get(i) + 1;
                }
                if (s.charAt(start) == '(')
                    result += represent(s.substring(start + 1, s.length() - 2));
                else
                    result += represent(s.substring(start, s.length() - 1));
            }

        }
        return result;
    }
}
