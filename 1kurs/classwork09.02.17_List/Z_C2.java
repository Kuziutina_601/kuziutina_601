import java.util.Scanner;

/**
 * Created by пользователь on 13.02.2017.
 */
public class Z_C2 {

    public static Scanner sc;
    public static void main(String[] args) {

        Elem head = null;
        Elem head2 = null;
        Elem p;

        sc = new Scanner(System.in);

        head = createList(4);
        head2 = createList(4);

        head = slip(head, head2);

        p = head;
        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }

    public static Elem slip(Elem head, Elem head2) {
        Elem p;
        p = head;
        while (p.getNext() != null){
            p = p.getNext();
        }

        p.setNext(head2);

        return head;
    }

    public static Elem createList (int n) {
        Elem p;
        Elem head = null;
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            p.setNext(head);
            head = p;
        }

        return head;
    }
}
