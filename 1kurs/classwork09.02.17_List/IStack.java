/**
 * Created by пользователь on 16.02.2017.
 */
public interface IStack {

    public void push(int x);
    public int pop() throws NullPointerException;
    public boolean isEmpty();
}
