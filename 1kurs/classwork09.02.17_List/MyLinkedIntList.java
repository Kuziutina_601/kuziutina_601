import java.util.Scanner;

/**
 * Created by пользователь on 13.02.2017.
 */
public class MyLinkedIntList {

    public Elem head;
    public Elem tail;

    public MyLinkedIntList() {
        head = null;
        tail = null;
    }

    public void input (int n) {
        Scanner sc = new Scanner(System.in);
        Elem p;

        head = new Elem();
        head.setValue(sc.nextInt());
        tail = head;

        for (int i = 1; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            tail.setNext(p);
            tail = p;
        }
    }

    public String toString() {
        String s = "";
        Elem p = head;

        while (p != null) {
            s += p.getValue() + " ";
            p = p.getNext();
        }

        return s;
    }

    public void addAll (MyLinkedIntList ml) {
        tail.setNext(ml.getHead());
        tail = ml.getTail();
    }

    public void addInTail(int x) {
        Elem p;
        p = new Elem();
        p.setValue(x);
        if (head == null) {
            head = p;
            tail = head;
        }
        else {
            tail.setNext(p);
            tail = p;
        }
    }

    public void addInHead(int x) {
        Elem p = new Elem();
        p.setNext(head);
        p.setValue(x);
        head = p;
    }

    public void deleteHead() {
        if (head == null) new NullPointerException();
        else {
            head = head.getNext();
        }
    }

    public void deleteTail() {
        if (head.getNext() == null) {
            this.deleteHead();
        }
        else {
            Elem p, l = null;
            p = head;
            while (p.getNext() != null) {
                l = p;
                p = p.getNext();
            }
            l.setNext(null);
        }
    }

    public Elem getHead() {
        return head;
    }

    public Elem getTail() {
        return tail;
    }
}
