import java.util.Scanner;

/**
 * Created by пользователь on 13.02.2017.
 */
public class MyLinkedCharList {

    public ElemChar head;
    public ElemChar tail;

    public MyLinkedCharList() {
        head = null;
        tail = null;
    }

    /*public void input (int n) {
        Scanner sc = new Scanner(System.in);
        Elem p;

        head = new Elem();
        head.setValue(sc.next().charA);
        tail = head;

        for (int i = 1; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            tail.setNext(p);
            tail = p;
        }
    }*/

    public String toString() {
        String s = "";
        ElemChar p = head;

        while (p != null) {
            s += p.getValue() + " ";
            p = p.getNext();
        }

        return s;
    }

    public void addAll (MyLinkedCharList ml) {
        tail.setNext(ml.getHead());
        tail = ml.getTail();
    }

    public void addInTail(char x) {
        ElemChar p;
        p = new ElemChar();
        p.setValue(x);
        if (head == null) {
            head = p;
            tail = head;
        }
        else {
            tail.setNext(p);
            tail = p;
        }
    }

    public void addInHead(char x) {
        ElemChar p = new ElemChar();
        p.setNext(head);
        p.setValue(x);
        head = p;
    }

    public void deleteHead() {
        if (head == null) new NullPointerException();
        else {
            head = head.getNext();
        }
    }

    public void deleteTail() {
        if (head.getNext() == null) {
            this.deleteHead();
        }
        else {
            ElemChar p, l = null;
            p = head;
            while (p.getNext() != null) {
                l = p;
                p = p.getNext();
            }
            l.setNext(null);
        }
    }

    public boolean isEmpty() {
        if (head == null)
            return true;
        else
            return false;
    }

    public char getLast() {
        return tail.getValue();
    }

    public ElemChar getHead() {
        return head;
    }

    public ElemChar getTail() {
        return tail;
    }
}
