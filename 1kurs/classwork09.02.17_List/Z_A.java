import java.util.Scanner;

/**
 * Created by пользователь on 13.02.2017.
 */
public class Z_A {
    public static void main(String[] args) {

        Elem head = null;
        Elem p;
        //Elem lost = null;
        int n = 4;
        boolean has = false;

        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            p.setNext(head);
            head = p;
        }

        p = head;
        while(p != null && !has) {
            System.out.println(p.getValue());
            if (p.getValue() < 0) {
                System.out.println("YES");
                has = true;
            }
            p = p.getNext();
        }

        if (!has)
            System.out.println("NO");

    }
}
