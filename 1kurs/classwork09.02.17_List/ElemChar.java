/**
 * Created by пользователь on 09.02.2017.
 */
public class ElemChar {
    private char value;
    private ElemChar next;

    public ElemChar(){}

    public ElemChar(char value, ElemChar next) {
        this.value = value;
        this.next = next;
    }

    public char getValue() {
        return value;
    }

    public ElemChar getNext() {
        return next;
    }

    public void setValue(char value) {
        this.value = value;
    }

    public void setNext(ElemChar next) {
        this.next = next;
    }
}
