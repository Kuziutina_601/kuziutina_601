/**
 * Created by пользователь on 09.02.2017.
 */
public class Elem {
    private int value;
    private Elem next;

    public Elem(){}

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public Elem getNext() {
        return next;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNext(Elem next) {
        this.next = next;
    }
}
