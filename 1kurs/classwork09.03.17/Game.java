import myqueue.Queue;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by пользователь on 07.03.2017.
 */
public class Game {
    private static int WIDTH = 9;
    private static int HIGH  = 9;
    private static int COUNT_MINE = 10;

    public static Cell[][] field = new Cell[HIGH][WIDTH];

    public static void main(String[] args) {
        boolean helper;
        int buf1, buf2;
        Random random = new Random();

        createField();
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < HIGH; i++) {
            for (int j = 0; j < WIDTH; j++) {
                //if (field[i][j].getValue() > 10)
                //    System.out.print("* ");
                //else
                    System.out.print(field[i][j].getValue() + " ");
            }
            System.out.println();
        }
        do {
            System.out.println("i start");
            buf1 = sc.nextInt();
            buf2 = sc.nextInt();
            openCell(buf1, buf2);
            System.out.println("i end");
            print();
        }
        while (true);

    }

    public static void createField() {
        int buf1, buf2;
        Random random = new Random();

        for (int i = 0; i < HIGH; i++) {
            for (int j = 0; j < WIDTH; j++) {
                field[i][j] = new Cell(i, j);
            }
        }

        for (int i = 0; i < COUNT_MINE; i++) {
            do {
                buf1 = random.nextInt(HIGH);
                buf2 = random.nextInt(WIDTH);
            }
            while (field[buf1][buf2].getValue() == 19);
            field[buf1][buf2].setValue(19);
            around(buf1, buf2);
        }

    }

    public static void around (int x, int y) {
        if (x > 0 && field[x-1][y].getValue() != 19) field[x-1][y].setValue(field[x-1][y].getValue() + 1);
        if (x > 0 && y > 0 && field[x-1][y-1].getValue() != 19) field[x-1][y-1].setValue(field[x-1][y-1].getValue() + 1);
        if (y > 0 && field[x][y-1].getValue() != 19) field[x][y-1].setValue(field[x][y-1].getValue() + 1);
        if (y > 0 && x < WIDTH - 1 && field[x + 1][y -1].getValue() != 19) field[x+1][y-1].setValue(field[x+1][y-1].getValue() + 1);
        if (x < WIDTH - 1 && field[x + 1][y].getValue() != 19) field[x+1][y].setValue(field[x+1][y].getValue() + 1);
        if (y < HIGH - 1 && x < WIDTH - 1 && field[x + 1][y + 1].getValue() != 19) field[x+1][y+1].setValue(field[x+1][y+1].getValue() + 1);
        if (y < HIGH - 1 && field[x][y + 1].getValue() != 19) field[x][y+1].setValue(field[x][y+1].getValue() + 1);
        if (y < HIGH - 1 && x > 0 && field[x - 1][y + 1].getValue() != 19) field[x-1][y+1].setValue(field[x-1][y+1].getValue() + 1);
    }

    public static void print() {
        for (int i = 0; i < HIGH; i++) {
            for (int j = 0; j < WIDTH; j++) {
                if (field[i][j].getValue() >= 10)
                    System.out.print("* ");
                else
                    System.out.print(field[i][j].getValue() + " ");
            }
            System.out.println();
        }
    }

    public static void openCell(int x, int y) {
        if (field[x][y].getValue() < 10){

        }
        else if (field[x][y].getValue() == 10) {
            System.out.println("it == 10");
            Queue left = new Queue();
            left.push(field[x][y]);
            Cell temp;
            while (!left.isEmpty()) {
                temp = (Cell) left.poll();
                x = temp.getX();
                y = temp.getY();
                System.out.println(x + "  " + y);
                if (temp.getValue() >= 10) {
                    temp.setValue(temp.getValue() - 10);
                    if (temp.getValue() == 0) {
                        System.out.println(" it == 0");
                        if (x > 0) left.push(field[x - 1][y]);
                        if (x > 0 && y > 0) left.push(field[x - 1][y - 1]);
                        if (y > 0) left.push(field[x][y - 1]);
                        if (y > 0 && x < WIDTH - 1) left.push(field[x + 1][y - 1]);
                        if (x < WIDTH - 1) left.push(field[x + 1][y]);
                        if (y < HIGH - 1 && x < WIDTH - 1) left.push(field[x + 1][y + 1]);
                        if (y < HIGH - 1) left.push(field[x][y + 1]);
                        if (y < HIGH - 1 && x > 0) left.push(field[x - 1][y + 1]);
                    }
                }
            }
        }
        else {
            System.out.println("it > 10");
            field[x][y].setValue(field[x][y].getValue() - 10);
        }

    }
}
