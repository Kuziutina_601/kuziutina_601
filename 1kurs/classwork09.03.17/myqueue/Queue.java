package myqueue;

/**
 * Created by пользователь on 06.03.2017.
 */
public class Queue<T> implements IQueue<T>{

    private Elem head;
    private Elem tail;

    public Queue() {
        this.head = null;
        this.tail = null;
    }

    @Override
    public void push(T x) {
        if (head == null){
            head = new Elem(x, head);
            tail = head;
        }
        else {
            Elem p = new Elem(x, null);
            tail.setNext(p);
            tail = p;
        }
    }

    @Override
    public T poll() throws NullPointerException {
        if (head == null) throw new NullPointerException();
        else {
            Elem p;
            p = head;
            head = head.getNext();
            return (T) p.getValue();
        }
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }


}
