package myqueue;

import MyStack.Stack;

/**
 * Created by пользователь on 06.03.2017.
 */
public class QueueStack<T> implements IQueue<T> {

    Stack first = new Stack();
    Stack second = new Stack();
    @Override
    public void push(T x) {
        first.push(x);
    }

    @Override
    public T poll() throws NullPointerException {
        if (second.isEmpty()) {
            if (first.isEmpty()) throw new NullPointerException("Didn't have element");
            while (!first.isEmpty()) {
                second.push(first.pop());
            }
        }

        return (T) second.pop();
    }

    @Override
    public boolean isEmpty() {
        return !(second.isEmpty() && first.isEmpty());
    }
}
