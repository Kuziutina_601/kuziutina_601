package myqueue;

/**
 * Created by пользователь on 06.03.2017.
 */
public interface IQueue <T> {

    public void push(T x);
    public T poll() throws NullPointerException;
    public boolean isEmpty();

}
