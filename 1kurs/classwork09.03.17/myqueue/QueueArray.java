package myqueue;

/**
 * Created by пользователь on 06.03.2017.
 */
public class QueueArray<T> implements IQueue<T>{

    private T[] array = (T[]) new Object[10000];
    private int start = 0;
    private int end = 0;
    @Override
    public void push(T x) {
        if (end + 1 >= array.length) {
            T[] k = (T[]) new Object[array.length * 10];
            for (int i = 0; i < array.length; i++) {
                k[i] = array[i];
            }
            array = k;
        }
        array[end++] = x;
    }

    @Override
    public T poll() throws NullPointerException {
        if (end - start + 1< start) {
            for (int i = 0; i < end - start + 1; i++) {
                array[i] = array[start + i];
            }
            end = end - start;
            start = 0;
        }
        if (isEmpty()) throw new NullPointerException("Haven't element");
        return array[start++];
    }

    @Override
    public boolean isEmpty() {
        return end == start;
    }
}
