/**
 * Created by пользователь on 09.03.2017.
 */
public class BinaryTree {
    private Node<Integer> root;

    private static <T> void makeTreeOnNode (Node<Integer> node, int n)  {
        int nLeft = n/2;
        int nRight = n - 1 - nLeft;
        node.setValue(n);
        if (nLeft > 0) {
            node.setLeft(new Node<>());
            makeTreeOnNode(node.getLeft(), nLeft);
        }
        if (nRight > 0) {
            node.setRight(new Node<>());
            makeTreeOnNode(node.getRight(), nRight);
        }
    }

    public BinaryTree (int n) {
        root = new Node<Integer>();
        makeTreeOnNode(root, n);
    }

    public void printTree() {
        printNode(root, 0);
    }

    public void printNode(Node<Integer> root, int h) {
        if (root != null) {
            printNode(root.getLeft(), h + 1);
            for (int i = 1; i <= h; i++)
                System.out.print("  ");
            System.out.println(root.getValue());
            printNode(root.getRight(), h + 1);
        }
    }

    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree(20);
        bt.printTree();
    }
}
