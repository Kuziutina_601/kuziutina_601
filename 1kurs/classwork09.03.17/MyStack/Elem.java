package MyStack;

/**
 * Created by пользователь on 06.03.2017.
 */
public class Elem<T> {
    private T value;
    private Elem next;

    public Elem(){}

    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public Elem getNext() {
        return next;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

}
