package MyStack;

/**
 * Created by пользователь on 06.03.2017.
 */

public class Stack<T> implements IStack<T> {

    private Elem head;
    private Elem tail;

    public Stack() {
        this.head = null;
        this.tail = null;
    }

    @Override
    public void push(T x) {
        head = new Elem(x, head);
    }

    @Override
    public T pop() {
        T c;
        if (head == null) throw new NullPointerException();
        else {
            c = (T) head.getValue();
            head = head.getNext();
        }
        return c;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}

