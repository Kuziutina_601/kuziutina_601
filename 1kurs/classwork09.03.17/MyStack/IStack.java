package MyStack;

/**
 * Created by пользователь on 16.02.2017.
 */
public interface IStack<T> {

    public void push(T x);
    public T pop() throws NullPointerException;
    public boolean isEmpty();
}
