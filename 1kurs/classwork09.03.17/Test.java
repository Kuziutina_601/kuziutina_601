import myqueue.QueueArray;

/**
 * Created by пользователь on 07.03.2017.
 */
public class Test {
    public static void main(String[] args) {
        QueueArray<Integer> ar = new QueueArray<>();

        ar.push(1);
        ar.push(2);
        ar.push(3);
        ar.push(4);
        System.out.println(ar.poll());
        System.out.println(ar.poll());
        System.out.println(ar.poll());
        System.out.println(ar.poll());
        System.out.println(ar.poll());
    }
}
