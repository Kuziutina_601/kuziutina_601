/**
 * Created by пользователь on 16.02.2017.
 */
public interface IStack {

    public void push(char x);
    public char pop() throws NullPointerException;
    public boolean isEmpty();
}
