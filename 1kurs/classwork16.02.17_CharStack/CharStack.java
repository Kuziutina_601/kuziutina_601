/**
 * Created by пользователь on 16.02.2017.
 */
public class CharStack implements IStack {

    private ElemChar head;
    private ElemChar tail;

    public CharStack() {
        this.head = null;
        this.tail = null;
    }

    @Override
    public void push(char x) {
        head = new ElemChar(x, head);
    }

    @Override
    public char pop() {
        char c;
        if (head == null) throw new NullPointerException();
        else {
            c = head.getValue();
            head = head.getNext();
        }
        return c;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
