package Part08;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by пользователь on 27.05.2017.
 */
public class Part08 {

    public static void main(String[] args) throws FileNotFoundException {

    }

    public void task54() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        String s = sc.nextLine();
        String[] elems = s.split(" ");
        Map<Integer, Integer> result = new HashMap<>();

        for (int i = 0; i < elems.length; i++) {
            int buf = Integer.parseInt(elems[i]);
            if (result.containsKey(buf)) {
                result.put(buf, result.get(buf) + 1);
            }
            else {
                result.put(buf, 1);
            }
        }

        PrintWriter pw = new PrintWriter(new File("out.txt"));
        for (Integer b : result.keySet()) {
            pw.print(b + " ");
            pw.println(result.get(b));
        }
        pw.close();
    }


    public void task55() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        String s = sc.nextLine();
        s.toLowerCase();
        Map<Character, Integer> result = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char buf = s.charAt(i);
            if (buf >= 'a' && buf <= 'z') {
                if (result.containsKey(buf)) {
                    result.put(buf, result.get(buf) + 1);
                } else {
                    result.put(buf, 1);
                }
            }
        }

        PrintWriter pw = new PrintWriter(new File("out.txt"));
        for (char i = 'a'; i <= 'z'; i++) {
            pw.print(i + " ");
            if (result.containsKey(i)) {
                pw.println(result.get(i));
            }
            else pw.println("0");
        }
        pw.close();
    }

    public void Task56() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        String s = sc.nextLine();
        String[] words = s.split(" ");
        Map<String, Integer> result = new HashMap<>();

        for (int i = 0; i < words.length; i++) {
            String buf = words[i].toLowerCase();
            if (result.containsKey(buf)) {
                result.put(buf, result.get(buf) + 1);
            } else {
                result.put(buf, 1);
            }
        }

        for (String word: result.keySet()) {
            System.out.println(word + " " + result.get(word));
        }
    }

    public void Task57() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        Scanner sc_dic = new Scanner(new File("input.txt"));

        Map<String, String> dic = new HashMap<>();
        while (sc_dic.hasNextLine()) {
            String s = sc_dic.nextLine();
            String[] word = s.split(" = ");
            dic.put(word[0], word[1]);
        }

        String text = sc.nextLine();
        String[] words = text.split(" |^[a-zA-Z]"); //////////////////////////////////////  регулярка, не буква
        for (int i = 0; i < words.length; i++) {
            if (dic.containsKey(words[i])) {
                System.out.print(dic.get(words[i]) + " ");
            }
            else System.out.print(words[i] + " ");
        }
    }

    ///////////////////////58 какие вообще оценки могут быть? (а то всплывает точка какая-то..)
}
