package Part02;

import Part01.Elem;

/**
 * Created by пользователь on 15.05.2017.
 */
public class List02 {
    private Elem head;
    private Elem tail;

    public List02() {
        head = null;
        tail = null;
    }

    public void add(int value) {
        if (head == null) {
            head = new Elem();
            head.setValue(value);
            tail = head;
        }
        else {
            Elem p = new Elem();
            p.setValue(value);
            tail.setNext(p);
            tail = p;
        }
    }

    public void addAfter2(int value, int x) { ////Task14
        add(value);
        if (value % 2 == 0)
            add(x);
    }

    public void addAfter2or3(int value, int x) { ////Task15
        add(value);
        if ((value%2 == 0) || (value%3 == 0))
            add(x);
    }

    public void addBefore2(int value, int x) {  /////Task16
        if (value % 2 == 0) {
            add(x);
        }
        add(value);
    }

    public void addAfterAndBeforeSimple(int value) {  ///Task17
        if (simpleNumber(value)) {
            int k = value;
            while (k > 10){
                k = k/10;
            }
            add(k);
            add(value);
            add(value%10);
            return;
        }
        add(value);
    }

    public boolean simpleNumber(int value) {
        for (int i = 2; i < Math.sqrt(value) + 1; i++) {
            if (value % i == 0)
                return false;
        }
        return true;
    }

    public void set(int position, int value) {   /////Task18
        Elem p = head;
        if (size() <= position) {
            //throw new IndexOutOfBoundsException();
            add(value);
            return;
        }
        for (int i = 0; i < position; i++) {
            p = p.getNext();
        }
        Elem buf = new Elem();
        buf.setValue(value);
        buf.setNext(p.getNext());
        p.setNext(buf);
    }

    public int size() {
        Elem p = head;
        int size = 0;
        while (p != null) {
            size++;
            p = p.getNext();
        }
        return size;
    }

    public void delete3Last() {  //Task19
        int size = size();
        if (size == 0)
            return;
        if (size == 1 || size == 2 || size ==3) {
            head = null;
            return;
        }
        Elem p = head;
        while (p.getNext().getNext().getNext().getNext() != null) {    ///hardcode is my love  \o/
            p = p.getNext();
        }
        p.setNext(null);
    }

    public void deleteNot2() {   ///Task20
        Elem p = head;
        while (p != null && (int)p.getValue()%2 == 1) {
            deleteHead();
        }
        while (p != null) {
            if ((int)p.getNext().getValue() % 2 != 0) {
                p.setNext(p.getNext().getNext());
            }
            else {
                p = p.getNext();
            }
        }
    }

    public void deleteHead() {
        head = head.getNext();
    }

    public void deleteByValue(int value) {   ///Task21
        Elem p = head;
        while (p != null && (int)p.getValue() == value) {
            deleteHead();
        }
        while (p != null) {
            if ((int)p.getNext().getValue() == value) {
                p.setNext(p.getNext().getNext());
            }
            else {
                p = p.getNext();
            }
        }
    }

    public List02 merge(List02 l2, List02 l1) {    ////Task22
        List02 result = new List02();
        Elem p1 = l1.head;
        Elem p2 = l2.head;
        while (p1 != null && p2 != null) {
            if ((int) p1.getValue() < (int) p2.getValue()) {
                result.add((int) p1.getValue());
                p1 = p1.getNext();
            }
            else {
                result.add((int) p2.getValue());
                p2 = p2.getNext();
            }
        }
        while (p1 != null) {
            result.add((int) p1.getValue());
            p1 = p1.getNext();
        }
        while (p2 != null) {
            result.add((int) p2.getValue());
            p2 = p2.getNext();
        }
        return result;
    }

    public void deleteByPosition(int position) {   //Task23
        Elem p = head;
        if (size() <= position)
            throw new IndexOutOfBoundsException();
        for (int i = 0; i < position - 1; i++) {
            p = p.getNext();
        }
        p.setNext(p.getNext().getNext());
    }

    public List02 cutOn2List() {
        Elem p = head;
        List02 result = new List02();
        if (p == null) {
            return result;
        }
        while (p != null && (int) p.getValue()%2 != 0) {
            result.add((int)p.getValue());
            deleteHead();
            p = head;
        }

        while (p.getNext() != null) {
            if ((int)p.getNext().getValue()%2 != 0) {
                result.add((int)p.getNext().getValue());
                p.setNext(p.getNext().getNext());
            }
            else {
                p = p.getNext();
            }
        }

        return result;
    }

    /////////!!!!!!!!!!Task25 i don't understand what do/...........................!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public int  twoInTen() {
        int k = 1;
        int result = 0;
        int buf;
        for (int i = size() - 1; i >= 0; i--) {
            buf = get(i);
            result += buf*k;
            k *= 2;
        }
        return result;
    }

    public int get(int position) {
        if (position >= size()) {
            throw new IndexOutOfBoundsException(position + " size:" + size());
        }
        Elem p = head;
        for (int i = 0; i < position; i++) {
            p = p.getNext();
        }

        return (int)p.getValue();
    }


}
