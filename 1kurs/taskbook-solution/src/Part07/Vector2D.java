package Part07;

import java.util.Comparator;

public class Vector2D implements Comparable<Vector2D>, Comparator<Vector2D> {

    private double x, y;

    public Vector2D () {
        this(0,0);
    }
    public Vector2D (double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Vector2D add (Vector2D v3) {
        double x2 = x + v3.getX();
        double y2 = y + v3.getY();

        return new Vector2D(x2, y2);
    }

    public void add2 (Vector2D v3) {
        this.x += v3.getX();
        this.y += v3.getY();
    }

    public Vector2D sub (Vector2D v3) {
        double x2 = x - v3.getX();
        double y2 = y - v3.getY();

        return new Vector2D(x2, y2);
    }

    public void sub2 (Vector2D v3) {
        this.x = this.x - v3.getX();
        this.y = this.y - v3.getY();
    }

    public Vector2D mult (double a) {
        double x2 = x * a;
        double y2 = y * a;

        return new Vector2D(x2, y2);
    }

    public void mult2 (double a) {
        this.x *= a;
        this.y *= a;
    }

    /////////////////////////////////////////////////////
    public String toString() {
        String s ="<" + x + ", " + y + ">";
        return s;
    }

    public double length () {
        return Math.sqrt(x*x + y*y);
    }

    public double scalarProduct (Vector2D v3) {
        double result;
        result = this.x * v3.getX() + this.y * v3.getY();
        return result;
    }

    public double cos (Vector2D v3) {
        double result, down;
        result = this.scalarProduct(v3);
        down = this.length() * v3.length();
        result = result / down;
        return result;
    }

    public boolean equals(Vector2D v3) {
        boolean equals = false;
        if ((x == v3.getX()) && (y == v3.getY()))
            equals = true;
        return equals;
    }

    @Override
    public int compareTo(Vector2D o) {
        if (length() > o.length()) return 1;
        else if (length() < o.length()) return -1;
        return 0;
    }

    @Override
    public int compare(Vector2D o1, Vector2D o2) {
        if (o1.getX() < o2.getX()) return 1;
        else if (o1.getX() > o2.getX()) return -1;
        else {
            if (o1.getY() < o2.getY()) return 1;
            else if (o1.getY() > o2.getY()) return -1;
            else return 0;
        }
    }
}