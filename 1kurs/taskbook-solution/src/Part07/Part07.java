package Part07;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by пользователь on 20.05.2017.
 */
public class Part07 {
    public List inputInteger() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));

        List<Integer> result = new ArrayList<>();
        String[] numbers = sc.nextLine().split(" ");

        for (int i = 0; i < numbers.length; i++) {
            result.add(Integer.parseInt(numbers[i]));
        }
        return result;
    }

    public void outputResult(List<Integer> result) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File("out.txt"));

        for (Integer p:result) {
            pw.print(p + " ");
        }
        pw.close();
    }

    public void Task045() throws FileNotFoundException {
        List<Integer> list = inputInteger();

        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int first = 0;
                int second = 0;
                int buf1 = o1;
                while (buf1 > 0) {
                    buf1 /= 10;
                    first++;
                }
                buf1 = o2;
                while (buf1 > 0) {
                    buf1 /= 10;
                    second++;
                }

                if (first > second) return 1;
                else if (second > first) return  -1;
                return 0;
            }
        });

        outputResult(list);
    }

    public void Task046() {
        List<Vector2D> l1 = new ArrayList<Vector2D>();
        Vector2D v1 = new Vector2D(6, 7);
        l1.add(v1);
        Vector2D v2 = new Vector2D(5, 6);
        l1.add(v2);
        Collections.sort(l1);
    }


    public void Task047() {
        List<Vector2D> l1 = new ArrayList<Vector2D>();
        Vector2D v1 = new Vector2D(6, 7);
        l1.add(v1);
        Vector2D v2 = new Vector2D(5, 6);
        l1.add(v2);
        Collections.sort(l1);
    }

    //////////////////////////////////////////////////////Task48

    public void Task048() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        String[] s = sc.nextLine().split(" ");
        int k = sc.nextInt();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < s.length; i++) {
            if (s[i].length() >= k) {
                list.add(s[i]);
            }
        }

        for (int i = 0; i < k; i++) {
            int finalI = i;
            Collections.sort(list, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    if (o1.charAt(finalI) > o2.charAt(finalI)) return 1;
                    else if (o1.charAt(finalI) < o2.charAt(finalI)) return -1;
                    return 0;
                }
            });
        }

        PrintWriter pw = new PrintWriter(new File("out.txt"));
        pw.println(list);
        pw.close();
    }

    public List Task049(List<Integer> list) {
        MyPriorityQueue<Integer> myq = new MyPriorityQueue<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int first = 0;
                int second = 0;
                int buf1 = o1;
                while (buf1 > 0) {
                    buf1 /= 10;
                    first++;
                }
                buf1 = o2;
                while (buf1 > 0) {
                    buf1 /= 10;
                    second++;
                }

                if (first > second) return 1;
                else if (second > first) return  -1;
                return 0;
            }
        });

        for (Integer elem: list) {
            myq.offer(elem);
        }

        List<Integer> result = new ArrayList<>();
        while (!myq.isEmpty()) {
            result.add(myq.peek());
        }
        return result;
    }

    ///////////////////////////////Task50

    public List<SiteInfo> readSiteInfo() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        List<SiteInfo> list = new ArrayList<>();
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            String[] buf = s.split(" ");
            SiteInfo si = new SiteInfo(buf[0]);

            for (int i = 1; i < buf.length; i++) {
                si.add(Integer.parseInt(buf[i]));
            }
            list.add(si);
        }
        return list;
    }

    public void printSiteInfo(List<SiteInfo> list, String filename) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File(filename));

        for (SiteInfo si: list) {
            pw.print(si.getName() + " ");
            pw.println(si.getValue());
        }
        pw.close();
    }

    public void Task51() throws FileNotFoundException {
        List<SiteInfo> list = readSiteInfo();
        printSiteInfo(list, "out.txt");
    }

    public void Task52() throws FileNotFoundException {
        List<SiteInfo> list = readSiteInfo();
        Collections.sort(list);
        printSiteInfo(list, "out2.txt");
    }

    ///////////////////////53 i don't know what do with 0
}
