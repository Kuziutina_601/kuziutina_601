package Part07;

import Part03.MyQueue;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by пользователь on 27.05.2017.
 */
public class MyPriorityQueue<T> implements MyQueue<T> {
    private Comparator<T> comparator;
    private List<T> elems;

    public MyPriorityQueue(Comparator<T> comparator) {
        this.comparator = comparator;
        elems = new LinkedList<T>();
    }

    @Override
    public void offer(T x) {
        elems.add(x);
        Collections.sort(elems, comparator);
    }

    @Override
    public T poll() throws NullPointerException {
        return elems.get(0);
    }

    @Override
    public T peek() throws NullPointerException {
        return elems.remove(0);
    }

    @Override
    public boolean isEmpty() {
        return elems.size() == 0;
    }
}
