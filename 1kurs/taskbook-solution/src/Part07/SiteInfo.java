package Part07;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by пользователь on 27.05.2017.
 */
public class SiteInfo implements Comparable<SiteInfo>{
    private String name;
    private List<Integer> count;

    public SiteInfo(String name) {
        this.name = name;
        count = new ArrayList<>();
    }

    public void addInfoCount(String s) {
        String[] buf = s.split(" ");
        for (int i = 0; i < buf.length; i++) {
            count.add(Integer.parseInt(buf[i]));
        }
    }

    public void add(int count) {
        this.count.add(count);
    }

    public double getValue() {
        double result = 0;
        for (Integer b: count) {
            result += b;
        }

        return result/7;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getCount() {
        return count;
    }

    public void setCount(List count) {
        this.count = count;
    }

    @Override
    public int compareTo(SiteInfo o) {
        return name.compareTo(o.getName());
    }
}
