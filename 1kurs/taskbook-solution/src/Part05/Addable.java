package Part05;

/**
 * Created by пользователь on 20.05.2017.
 */
public interface Addable<T> {
    T add(T x);
}
