package Part05;

/**
 * Created by пользователь on 20.05.2017.
 */
public class ComplexNumber implements Addable<ComplexNumber> {
    int a;
    int b;

    public ComplexNumber(int a, int b) {
        this.a = a;
        this.b = b;
    }


    @Override
    public ComplexNumber add(ComplexNumber x) {
        return new ComplexNumber(a + x.getA(), b + x.getB());
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
