package Part05;



/**
 * Created by пользователь on 20.05.2017.
 */
public class Vector2D<T extends Addable> {
    private T x;
    private T y;

    public Vector2D(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D v2) {
        return new Vector2D((T)x.add(v2.getX()), (T)y.add(v2.getY()));
    }

    public T getX() {
        return x;
    }

    public void setX(T x) {
        this.x = x;
    }

    public T getY() {
        return y;
    }

    public void setY(T y) {
        this.y = y;
    }
}
