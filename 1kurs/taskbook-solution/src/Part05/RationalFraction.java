package Part05;

/**
 * Created by пользователь on 20.05.2017.
 */
public class RationalFraction implements Addable<RationalFraction> {
    int k;

    public RationalFraction(int k) {
        this.k = k;
    }
    @Override
    public RationalFraction add(RationalFraction x) {
        return new RationalFraction(k + x.getK());
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }
}
