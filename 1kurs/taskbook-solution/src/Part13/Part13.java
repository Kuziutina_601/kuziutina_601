package Part13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by пользователь on 29.05.2017.
 */
public class Part13 {
    private final String sog = "qwrtpsdfghjklzxcvbnm";
    private final String gl  = "eyuioa";
    private List<String> solution;
    public void Task79() {
        solution = new ArrayList<>();
        back79(new StringBuilder(), 5, 0);
    }

    public void back79(StringBuilder stringBuilder, int n, int k) {
        if (stringBuilder.length() == n) {
            solution.add(String.valueOf(stringBuilder));
            return;
        }
        if (k < 1) {
            for (int i = 0; i < gl.length(); i++) {
                stringBuilder.append(gl.charAt(i));
                back79(stringBuilder, n, k+1);
                stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
            }
        }
        for (int i = 0; i < sog.length(); i++) {
            stringBuilder.append(sog.charAt(i));
            back79(stringBuilder, n, k);
            stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
        }

    }


    public void Task80() throws FileNotFoundException {
        solution = new ArrayList<>();
        Scanner sc = new Scanner(new File("in.txt"));
        int n = sc.nextInt();
        back79(new StringBuilder(), n, 0);
    }

    public void back80(StringBuilder stringBuilder, int n, int k) {
        if (stringBuilder.length() == n) {
            solution.add(String.valueOf(stringBuilder));
            return;
        }
        if (k < 3) {
            for (int i = 0; i < gl.length(); i++) {
                stringBuilder.append(gl.charAt(i));
                back79(stringBuilder, n, k+1);
                stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
            }
        }
        for (int i = 0; i < sog.length(); i++) {
            stringBuilder.append(sog.charAt(i));
            back79(stringBuilder, n, k);
            stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
        }

    }

    public void Task81() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        int m = sc.nextInt();
        int k = sc.nextInt();
    }

    private void back81(StringBuilder stringBuilder, int n, int k, int summ) {
        if (summ < n && stringBuilder.length() == k) {
            solution.add(String.valueOf(stringBuilder));
            return;
        }
        if (summ >= n || stringBuilder.length() >= k)
            return;
        for (int i = 0; i < 10; i++) {
            stringBuilder.append(Integer.toString(i));
            back81(stringBuilder, n, k, summ + i);
            stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
        }
    }

    public void Task83() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        int n = sc.nextInt();
        solution = new ArrayList<>();
        back83(n-1, 1, 1);
        System.out.println(solution);
    }

    public void back83(int k, int a, int b) {
        if (k == 1) {
            solution.add(Integer.toString(a));
            return;
        }
        int h = a;
        a = a + b;
        back83(k-1, a, h);
    }
}
