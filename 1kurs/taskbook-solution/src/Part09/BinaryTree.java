package Part09;

/**
 * Created by пользователь on 09.03.2017.
 */
public class BinaryTree<T extends Comparable<T>> {
    private Node<T> root;
    private int size = 0;

    public boolean add(T value) {
        size++;
        if (root == null) setRoot(new Node<T>(value));
        else {
            Node<T> p = root;
            boolean go = true;
            boolean left = true;
            while (go) {
                if (value.compareTo(p.getValue()) < 0) {
                    left = true;
                    if (p.getLeft() != null) p = p.getLeft();
                    else go = false;
                }
                else {
                    left = false;
                    if (p.getRight() != null) p = p.getRight();
                    else go = false;
                }
            }
            if (left) p.setLeft(new Node<T>(value));
            else p.setRight(new Node<T>(value));

        }
        return true;
    }


    public BinaryTree() {
        root = null;
    }

    public void printTree() {
        printNode(root, 0);
    }

    private void printNode(Node<T> root, int h) {
        if (root != null) {
            printNode(root.getLeft(), h + 1);
            for (int i = 1; i <= h; i++)
                System.out.print("  ");
            System.out.println(root.getValue());
            printNode(root.getRight(), h + 1);
        }
    }

    public Node<T> getRoot() {
        return root;
    }

    public void setRoot(Node<T> root) {
        this.root = root;
    }
}
