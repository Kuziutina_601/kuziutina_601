package Part09;

import java.util.*;

/**
 * Created by пользователь on 27.05.2017.
 */
public class Part09 {
    public static void main(String[] args) {

    }

    public void Task60() {
        BinaryTree<Integer> bt = new BinaryTree<Integer>();
        for (int i = -5; i < 5; i++) {
            bt.add(i);
        }
        int result = 0;

        Queue<Node<Integer>> queue = new LinkedList<Node<Integer>>();
        queue.add(bt.getRoot());
        while (!queue.isEmpty()) {
            Node<Integer> buf = queue.peek();
            result += buf.getValue();
            if (buf.getLeft() != null) queue.add(buf.getLeft());
            if (buf.getRight() != null) queue.add(buf.getRight());
        }

        System.out.println(result);
    }

    public void Task61() {
        BinaryTree<Integer> bt = new BinaryTree<Integer>();
        for (int i = -5; i < 5; i++) {
            bt.add(i);
        }
        System.out.println(multi(1, bt.getRoot()));
    }

    private int multi(int result, Node<Integer> node) {
        if (node == null) return result;
        if (node.getLeft() != null) result *= multi(result, node.getLeft());
        if (node.getRight() != null) result *= multi(result, node.getRight());
        return result *= node.getValue();
    }


    public void Task62() {
        BinaryTree<Integer> bt = new BinaryTree<Integer>();
        for (int i = -5; i < 5; i++) {
            bt.add(i);
        }

        Stack<Node> stack = new Stack<Node>();
        stack.add(bt.getRoot());
        int max = bt.getRoot().getValue();
        Node<Integer> buf;

        while (!stack.isEmpty()) {
            buf = stack.pop();
            if (buf.getValue() > max) {
                max = buf.getValue();
            }
            if (buf.getRight() != null) stack.add(buf.getRight());
            if (buf.getLeft() != null) stack.add(buf.getLeft());
        }
        System.out.println(max);
    }


    public void Task64() {
        List<Node> has = new LinkedList<Node>();

        BinaryTree<Integer> bt = new BinaryTree<Integer>();
        for (int i = -5; i < 5; i++) {
            bt.add(i);
        }

        Stack<Node> stack = new Stack<Node>();
        stack.add(bt.getRoot());
        Node<Integer> buf;
       // has.add(bt.getRoot());

        while (!stack.isEmpty()) {
            buf = stack.pop();
            if (buf.getRight() != null && !has.contains(buf.getRight())) {
                stack.add(buf.getRight());
            }
            stack.add(buf);
            if (buf.getLeft() != null && !has.contains(buf.getLeft())) {
                stack.add(buf.getLeft());
            }
            else {
                stack.pop();
                has.add(buf);
            }
        }
    }

    public void Task65() {
        List<Node> has = new LinkedList<Node>();

        BinaryTree<Integer> bt = new BinaryTree<Integer>();
        for (int i = -5; i < 5; i++) {
            bt.add(i);
        }

        Stack<Node> stack = new Stack<Node>();
        stack.add(bt.getRoot());
        Node<Integer> buf;
        // has.add(bt.getRoot());

        while (!stack.isEmpty()) {
            buf = stack.pop();
            stack.add(buf);
            if (buf.getRight() != null && !has.contains(buf.getRight())) {
                stack.add(buf.getRight());
            }
            if (buf.getLeft() != null && !has.contains(buf.getLeft())) {
                stack.add(buf.getLeft());
            }

            if ((buf.getLeft() == null || has.contains(buf.getLeft()))
                    && (buf.getRight() == null || has.contains(buf.getRight()))) {
                stack.pop();
                has.add(buf);
            }


        }
    }

    public void Task66() {
        List<Node> has = new LinkedList<Node>();

        BinaryTree<Integer> bt = new BinaryTree<Integer>();
        for (int i = -5; i < 5; i++) {
            bt.add(i);
        }

        Stack<Node> stack = new Stack<Node>();
        stack.add(bt.getRoot());
        Node<Integer> buf;
        // has.add(bt.getRoot());

        while (!stack.isEmpty()) {
            buf = stack.pop();
            if (buf.getRight() != null && !has.contains(buf.getRight())) {
                stack.add(buf.getRight());
            }

            if (buf.getLeft() != null && !has.contains(buf.getLeft())) {
                stack.add(buf.getLeft());
            }

            has.add(buf);

        }
    }

}
