package Part09;

/**
 * Created by пользователь on 09.03.2017.
 */
public class Node<T> {
    private T value;
    private Node<T> left;
    private Node<T> right;
    private boolean mark;


    public Node(T value) {
        this.value = value;
        mark = true;
    }

    public Node() {
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getLeft() {
        return left;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
    }

    public Node<T> getRight() {
        return right;
    }

    public void setRight(Node<T> right) {
        this.right = right;
    }
}
