package Part01;

import java.util.Scanner;

/**
 * Created by пользователь on 29.04.2017.
 */
public class List01 {
    private Elem head;
    private Elem tail;

    public List01() {
        head = null;
        tail = null;
    }

    public void add(int value) { /////////////Task001
        if (head == null) {
            head = new Elem();
            head.setValue(value);
            tail = head;
        }
        else {
            Elem p = new Elem();
            p.setValue(value);
            tail.setNext(p);
            tail = p;
        }
    }

    public int getMax() {    //////////Task002
        Elem p = head;
        int max;

        if (head == null) {
            throw new NullPointerException();
        }
        else {
            max = (int) head.getValue();
            while (p != null) {
                if (max < (int)p.getValue()) {
                    max = (int) p.getValue();
                }
                p = p.getNext();
            }
            return max;
        }
    }

    public int summAllElem() {   //////////////Task003
        Elem p = head;
        int summ = 0;

        if (head == null) {
            throw new NullPointerException();
        }
        else {
            while (p != null) {
                summ += (int)p.getValue();
                p = p.getNext();
            }
            return summ;
        }
    }

    public int multiAllElem() {
        Elem p = head;
        int mult = 0;

        if (head == null) {
            throw new NullPointerException();
        }
        else {
            while (p != null) {
                mult *= (int)p.getValue();
                p = p.getNext();
            }
            return mult;
        }
    }

    public int summAll2() {   //////////////Task004
        Elem p = head;
        int summ = 0;

        if (head == null) {
            throw new NullPointerException();
        }
        else {
            while (p != null) {
                if ((int) p.getValue() % 2 == 0) {
                    summ += (int) p.getValue();
                }
                p = p.getNext();
            }
            return summ;
        }
    }

    public boolean hasZeroElem() {   //////Task004a
        Elem p = head;

        if (head == null) {
            throw new NullPointerException();
        }
        else  {
            while (p != null) {
                if ((int)p.getValue() == 0) {
                    return true;
                }
                p = p.getNext();
            }
            return false;
        }
    }

    public int multi1Elem() {     ///////Task004b
        Elem p = head;
        int mult = 1;
        int k = 1;

        if (head == null) {
            throw new NullPointerException();
        }
        else {
            while (p != null) {
                if (k % 2 != 0) {
                    mult *= (int) p.getValue();
                }
                k++;
                p = p.getNext();
            }
            return mult;
        }
    }

    //////Task005     i don't understand what do

    public void addElems(List01 list) {
        Elem p = head;
        if (p == null) throw new NullPointerException();
        while (p.getNext() != null) {
            p = p.getNext();
        }
        p.setNext(list.head);
    }

    public void changeFirstAndLast() {     ///////Task006
        Elem p = head;
        int first;

        if (head == null) {
            throw new NullPointerException();
        }
        else {
            first = (int) p.getValue();
            while (p.getNext() != null) {
                p = p.getNext();
            }
            head.setValue(p.getValue());
            p.setValue(first);
        }
    }

    public int multiNo2() { //////////Task07
        Elem p = head;
        int multi = 1;
        if (head == null) {
            return 0;
        }
        do {
            multi = multi * (int) p.getValue();
            if (multi == 0) {
                return 0;
            }
            p = p.getNext();
            if (p != null) {
                p = p.getNext();
            }
        }
        while (p !=  null);

        return multi;
    }

    public int localMaxAndMin() {  ///Task08
        int k = 0;
        Elem p = head;
        int pr1, pr2;
        boolean pretend = false;
        if (head == null) {
            throw new NullPointerException();
        }

        pr1 = (int) p.getValue();
        p = p.getNext();

        if (p == null) {
            return 0;
        }
        pr2 = (int) p.getValue();
        p = p.getNext();

        while (p != null) {
            if ((pr1 - pr2) * (pr2 - (int)p.getValue()) < 0) {
                k++;
            }
            pr1 = pr2;
            pr2 = (int) p.getValue();
            p = p.getNext();
        }

        return k;
    }

    public void input(int n) {
        List01 list = new List01();
        Scanner sc = new Scanner(System.in);
        int buf;
        for (int i = 0; i < n; i++) {
            buf = sc.nextInt();
            list.add(buf);
        }
    }

    public void output() {
        Elem p = head;
        System.out.print("list [");
        while (p != null) {
            System.out.print(" " + p.getValue());
            p = p.getNext();
        }
        System.out.print("]");
    }

    public void replace() {          ////Task09
        for (int i = size()-1; i > 0; i--) {
            insert(i, (Integer) head.getValue());
            head = head.getNext();
        }

    }

    public int size() {
        Elem p = head;
        int size = 0;
        while (p != null) {
            size++;
            p = p.getNext();
        }
        return size;
    }

    public void insert(int position, int value) {
        Elem p = head;
        if (size() < position)
            throw new NullPointerException();
        for (int i = 0; i < position; i++) {
            p = p.getNext();
        }
        Elem ne = new Elem();
        ne.setValue(value);
        ne.setNext(p.getNext());
        p.setNext(ne);
    }

    public Elem get(int position) {
        Elem p = head;
        if (size() < position)
            throw new NullPointerException();
        for (int i = 0; i < position; i++) {
            p = p.getNext();
        }
        return p;
    }

    public void bubbleSort() {       //////////Task10
        Elem p1, p2;
        for(int i = 0; i < size(); i++) {
            p1 = get(i);
            for (int j = size() - 1; j > i; j--) {
                p2 = get(j);
                if ((int)p1.getValue() > (int) p2.getValue()) {
                    int buf = (int) p2.getValue();
                    p2.setValue(p1.getValue());
                    p1.setValue(buf);
                }
            }
        }
    }

    public void insertSort() {  ////////Task11
        int k;
        Elem p;
        for (int i = 0; i < size(); i++) {
            p = getMin(i, size());
            int buf = (int) get(i).getValue();
            get(i).setValue(p.getValue());
            p.setValue(buf);
        }
    }

    public Elem getMin(int l, int r) {
        Elem p = get(l);
        for (int j = l + 1; j < r; j++) {
            Elem p1 = get(j);
            if ((int)p1.getValue() < (int)p.getValue()) {
                p = p1;
            }
        }
        return p;
    }
    public void shiftElem() {    ////////Task12
        Elem p = head;
        if (size() == 0 || size() == 1)
            return;;
        if (size() == 2) {
            p = head.getNext();
            p.setNext(head);
            head.setNext(null);
            head = p;
            return;
        }
        while (p != null && p.getNext() != null && p.getNext().getNext()!= null) {
            p = p.getNext();
        }
        p.getNext().setNext(head);
        head = p.getNext();
        p.setNext(null);
        return;
    }

    public void shiftElem(int n) {
        for (int i = 0; i< n; i++) {
            shiftElem();
        }
    }

    public void shiftElemToStart() {//////////Task13
        Elem p = head;
        while (p.getNext() != null) {
            p = p.getNext();
        }

        p.setNext(head);
        Elem buf = head.getNext();
        head.setNext(null);
        head = buf;
    }

    public void shiftElemToStart(int n) {
        for (int i = 0; i < n; i++) {
            shiftElemToStart();
        }
    }
}
