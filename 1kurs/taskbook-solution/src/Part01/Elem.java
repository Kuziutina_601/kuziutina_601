package Part01;

/**
 * Created by пользователь on 29.04.2017.
 */
public class Elem<T> {
    T value;
    Elem<T> next;

    public Elem() {}

    public Elem(Elem next, T value) {
        this.next = next;
        this.value = value;
    }
    public Elem(T value) {
        this(null, value);
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem<T> next) {
        this.next = next;
    }
}
