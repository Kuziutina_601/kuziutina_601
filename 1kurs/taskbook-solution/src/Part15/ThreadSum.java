package Part15;

/**
 * Created by пользователь on 29.05.2017.
 */
public class ThreadSum extends Thread {
    private int[] array;
    private int l;
    private int r;
    private int sum;

    public ThreadSum(int[] array, int l, int r) {
        this.array = array;
        this.l = l;
        this.r = r;
    }

    public int getSum() {
        return sum;
    }

    public void run() {
        sum = 0;
        for (int i = l; i < r; i++) {
            sum += array[i];
        }
    }
}

