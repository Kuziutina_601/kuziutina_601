package Part15;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by пользователь on 29.05.2017.
 */
public class Part15 {
    public void Task87() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        ThreadSum sum = new ThreadSum(array, 0, array.length/2);
        ThreadSum sum2 = new ThreadSum(array, array.length/2, array.length);
        sum.start();
        sum2.start();
        try {
            sum.join();
            sum2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println((sum.getSum() + sum2.getSum()));
    }

    public void Task88() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        int n = sc.nextInt();
        int k = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }

        ThreadSum[] threads = new ThreadSum[k];
        for (int i = 0; i < k; i++) {
            threads[i] = new ThreadSum(array, i * (n/k), (i+1)*(n/k) );
            threads[i].start();
        }
        for (int i = 0; i < k; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int summ = 0;

        for (int i = 0; i < k; i++) {
            summ += threads[i].getSum();
        }

        System.out.println(summ);

    }
}
