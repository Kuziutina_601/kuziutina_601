package Part06;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by пользователь on 29.05.2017.
 */
public class ArrayCollection<T> implements Collection<T> {

    private Object[] array;
    private int first, last, size;

    public ArrayCollection() {
        array = new Object[100];
        first = 0;
        last = 0;
        size = 100;
    }

    @Override
    public int size() {
        return last - first;
    }

    @Override
    public boolean isEmpty() {
        return last - first == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = first; i < last; i++) {
            if (o.equals(array[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (size == last) {
            capacity();
        }
        array[last] = t;
        last++;
        return true;
    }

    public void capacity() {
        int newSize = size + size*2/3;
        Object[] buf = new Object[newSize];
        for (int i = 0; i < size; i++) {
            buf[i] = array[i];
        }
        size = newSize;
        array = buf;
    }

    @Override
    public boolean remove(Object o) {
        if (!contains(o)) return false;
        int i = first;
        while (i < last) {
            if (o.equals(array[i])){
                break;
            }
            i++;
        }
        last--;
        while (i < last) {
            array[i] = array[i+1];
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean has = true;
        for (Object hh: c) {
            if (!contains(hh))
                has = false;
        }

        return has;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T hh: c) {
            add(hh);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object hh: c) {
            remove(hh);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        int i = first;
        while (i < last) {
            if (!c.contains(array[i])) {
                remove(array[i]);
            }
            else {
                i++;
            }
        }
        return true;
    }

    @Override
    public void clear() {
        last = first;
    }
}
