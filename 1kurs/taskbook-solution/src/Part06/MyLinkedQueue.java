package Part06;

import Part03.MyLinkedStack;
import Part03.MyStack;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

/**
 * Created by пользователь on 29.05.2017.
 */
public class MyLinkedQueue<T> implements Queue<T> {

    private MyStack<T> firstStack;
    private MyStack<T> secondStack;

    public MyLinkedQueue() {
        firstStack = new MyLinkedStack<T>();
        secondStack = new MyLinkedStack<T>();
    }
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return firstStack.push(t);
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        return firstStack.push(t);
    }

    @Override
    public T remove() {
        if (firstStack.isEmpty() && secondStack.isEmpty()) throw new NullPointerException();
        return getNext();
    }
    private T getNext() {
        if (secondStack.isEmpty()) {
            while (!firstStack.isEmpty()) {
                secondStack.push(firstStack.pop());
            }
        }
        return secondStack.pop();
    }

    @Override
    public T poll() {
        if (firstStack.isEmpty() && secondStack.isEmpty()) return null;
        return getNext();
    }

    @Override
    public T element() {
        if (firstStack.isEmpty() && secondStack.isEmpty()) throw new NullPointerException();
        T buf = getNext();
        secondStack.push(buf);
        return buf;
    }

    @Override
    public T peek() {
        if (firstStack.isEmpty() && secondStack.isEmpty()) return null;
        T buf = getNext();
        secondStack.push(buf);
        return buf;
    }
}
