package Part03;

/**
 * Created by пользователь on 20.05.2017.
 */
public interface MyStack<T> {
    public T pop() throws NullPointerException;
    public boolean push(T value);
    public boolean isEmpty();
}
