package Part03;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by пользователь on 29.05.2017.
 */
public class MyLinkedDeque<T> {
    private List<T> list;


    public MyLinkedDeque() {
        list = new LinkedList<T>();
    }

    public void addLast(T x) {
        if (!list.add(x) ) throw new NullPointerException();
    }

    public boolean offerLast(T x) {
        return list.add(x);
    }

    public T removeFirst() {
        if (list.isEmpty()) throw new NullPointerException();
        return list.remove(0);
    }

    public T pollFirst() {
        if (list.isEmpty()) return null;
        return list.remove(0);
    }

    public T getFirst() {
        if (list.isEmpty()) throw new NullPointerException();
        return list.get(0);
    }

    public T peekFirst() {
        if (list.isEmpty()) return null;
        return list.get(0);
    }

    public void addFirst(T x) {
        list.add(0, x);
    }

    public void offerFirst(T x) {
        list.add(0, x);
    }

    public T removeLast() {
        if (list.isEmpty()) throw new NullPointerException();
        return list.remove(list.size() - 1);
    }

    public T pollLast() {
        if (list.isEmpty()) return null;
        return list.remove(list.size() - 1);
    }

    public T getLast() {
        if (list.isEmpty()) throw new NullPointerException();
        return list.get(list.size() - 1);
    }

    public T peekLast() {
        if (list.isEmpty()) return null;
        return list.get(list.size() - 1);
    }
}
