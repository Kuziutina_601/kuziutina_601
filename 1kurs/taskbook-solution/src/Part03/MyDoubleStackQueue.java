package Part03;

/**
 * Created by пользователь on 20.05.2017.
 */
public class MyDoubleStackQueue<T> implements MyQueue<T> {
    private MyStack<T> firstStack;
    private MyStack<T> secondStack;

    public MyDoubleStackQueue() {
        firstStack = new MyLinkedStack<T>();
        secondStack = new MyLinkedStack<T>();
    }

    @Override
    public void offer(T x) {
        firstStack.push(x);
    }

    @Override
    public T peek() throws NullPointerException {
        if (isEmpty()) throw new NullPointerException();
        T buf = getNext();
        secondStack.push(buf);
        return buf;
    }

    @Override
    public T poll() throws NullPointerException {
        if (isEmpty()) throw new NullPointerException();
        return getNext();
    }

    @Override
    public boolean isEmpty() {
        return secondStack.isEmpty() && firstStack.isEmpty();
    }

    private T getNext() {
        if (secondStack.isEmpty()) {
            while (!firstStack.isEmpty()) {
                secondStack.push(firstStack.pop());
            }
        }
        return secondStack.pop();
    }
}
