package Part03;

import com.sun.javafx.scene.control.skin.VirtualFlow;

/**
 * Created by пользователь on 20.05.2017.
 */
public class MyArrayStack<T> implements MyStack<T> {
    private int size = 1000;
    private int last = 0;
    public Object[] elems;

    public MyArrayStack() {
        elems = new Object[size];
    }

    @Override
    public T pop() throws NullPointerException {
        last--;
        return (T)elems[last+1];
    }

    @Override
    public boolean push(T value) {
        if (size == last) {
            capacity();
        }
        elems[last] = value;
        last++;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return last == 0;
    }

    public void capacity() {
        int newSize = size + size*2/3;
        Object[] buf = new Object[newSize];
        for (int i = 0; i < size; i++) {
            buf[i] = elems[i];
        }
        size = newSize;
        elems = buf;
    }
}
