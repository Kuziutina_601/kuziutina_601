package Part03;

import Part01.Elem;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by пользователь on 20.05.2017.
 */
public class MyLinkedStack<T> implements MyStack<T> {

    private List<Elem<T>> data = new LinkedList<Elem<T>>();
    @Override
    public T pop() throws NullPointerException {
        if (data.size() == 0) throw new NullPointerException();
        T result = data.get(data.size()-1).getValue();
        data.remove(data.size()-1);
        return result;
    }

    @Override
    public boolean push(T value) {
        return data.add(new Elem<T>(value));
    }

    @Override
    public boolean isEmpty() {
        return data.size() == 0;
    }
}
