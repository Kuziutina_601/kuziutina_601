package Part03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by пользователь on 20.05.2017.
 */
public class Labirinth {
    private static int[][] field;
    private static Cell start, finish;
    private static int high, width;
    private static boolean foundFinish;
    private static MyStack<Cell> result;
    //0 - можно ходить, 1 - нельзя, 2 - старт, 3 - финиш

    public static void main(String[] args) throws FileNotFoundException {
        initField();
        foundFinish = false;

        result = new MyLinkedStack<Cell>();
        checkThis(start);
        //result.push(start);
        if (!foundFinish){
            System.out.println("it is impossible");
        }
        else {
            MyStack<Cell> print = new MyLinkedStack<Cell>();
            while (!result.isEmpty()) {
                print.push(result.pop());
            }

            while (!print.isEmpty()) {
                Cell buf = print.pop();
                System.out.println(buf.getX() + "   " + buf.getY());
            }
        }

    }

    public static void checkThis(Cell temp) {
//        System.out.println(foundFinish + "    cell: " + temp.getX() + ",  " + temp.getY());
        if (foundFinish || (temp.getX() == finish.getX() && temp.getY() == finish.getY())) {
            result.push(finish);
            foundFinish = true;
            return;
        }
        if (field[temp.getY()][temp.getX()] == 7 || field[temp.getY()][temp.getX()] == 5)
            return;
        if (field[temp.getY()][temp.getX()] == 0){
            field[temp.getY()][temp.getX()] = 5;
        }
        if (field[temp.getY()][temp.getX()] == 2){
            field[temp.getY()][temp.getX()] = 7;
        }

        result.push(temp);
        int x = temp.getX();
        int y = temp.getY();
        if (!foundFinish & x > 0 && (field[y][x-1] == 0 || field[y][x-1] == 3)) checkThis(new Cell(x - 1, y));
//        System.out.println(foundFinish + " " + x + "  " + y);
        if (!foundFinish & y > 0&& (field[y-1][x] == 0 || field[y-1][x] == 3)) checkThis(new Cell(x, y - 1));
//        System.out.println(foundFinish + " " + x + "  " + y);
        if (!foundFinish & x < width - 1&& (field[y][x+1] == 0 || field[y][x+1] == 3)) checkThis(new Cell(x + 1, y));
//        System.out.println(foundFinish + " " + x + "  " + y);
        if (!foundFinish & y < high - 1&& (field[y+1][x] == 0 || field[y+1][x] == 3)) checkThis(new Cell(x, y + 1));
        //System.out.println(foundFinish + " " + x + "  " + y);

        if (field[temp.getY()][temp.getX()] == 5)
            field[temp.getY()][temp.getX()] = 0;
        //System.out.println(foundFinish + "    cell: " + temp.getX() + ",  " + temp.getY() + "  " + foundFinish + field[1][0]);
        if (!foundFinish)
            result.pop();

    }

    public static void initField() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int y, x;
        String s = sc.nextLine();
        System.out.println(s);
        x = s.length();
        y = 1;
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            System.out.println(s);
            y++;
        }
        high = y;
        width = x;

        field = new int[y][x];
        //System.out.println(x + "   " +y);
        sc = new Scanner(new File("input.txt"));
        for (int i = 0; i < y; i++) {
            s = sc.nextLine();
            for (int j = 0; j < x; j++) {
                field[i][j] = s.charAt(j) - '0';
                if (field[i][j] == 2)
                    start = new Cell(j, i);
                if (field[i][j] == 3)
                    finish = new Cell(j, i);
            }
        }
    }
}
