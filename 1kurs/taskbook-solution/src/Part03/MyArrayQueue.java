package Part03;

/**
 * Created by пользователь on 06.03.2017.
 */
public class MyArrayQueue<T> implements MyQueue<T> {
    private Object[] array;
    private int size = 1000;
    private int first = 0;
    private int last = 0;

    public MyArrayQueue() {
        array = new Object[size];
    }
    @Override
    public void offer(T x) {
        if (last == size) capacity();
        array[last] = x;

    }

    @Override
    public T peek() throws NullPointerException {
        if (isEmpty()) throw new NullPointerException();
        return (T) array[first];
    }

    @Override
    public T poll() throws NullPointerException {
        if (isEmpty()) throw new NullPointerException();
        first++;
        return (T) array[first-1];
    }

    @Override
    public boolean isEmpty() {
        return first == last;
    }

    public void capacity() {
        if (last - first < first - 3) {
            for (int i = 0; i < last - first; i++) {
                array[0] = array[i+first];
            }
            first = 0;
            last = last - first;
            return;
        }
        int newSize = size + size*2/3;
        Object[] buf = new Object[newSize];
        for (int i = 0; i < size; i++) {
            buf[i] = array[i];
        }
        size = newSize;
        array = buf;
    }
}
