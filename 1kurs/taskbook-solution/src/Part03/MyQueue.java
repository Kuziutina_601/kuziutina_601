package Part03;

/**
 * Created by пользователь on 06.03.2017.
 */
public interface MyQueue<T> {

    public void offer(T x);
    public T poll() throws NullPointerException;
    public T peek() throws NullPointerException;
    public boolean isEmpty();

}
