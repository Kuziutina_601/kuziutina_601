package Part03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by пользователь on 20.05.2017.
 */
public class Part03 {

    public void revert(int[] array) {   ///////Task028
        MyLinkedStack<Integer> stack = new MyLinkedStack<Integer>();
        for (int i = 0; i < array.length; i++) {
            stack.push(array[i]);
        }

        for (int i = 0; i < array.length; i++) {
            array[i] = stack.pop();
        }
    }

    public void Task29() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("in.txt"));
        MyStack<Character> stack = new MyLinkedStack<Character>();
        String s = sc.nextLine();

        int i = 0;
        boolean has = true;
        while (i < s.length() && has) {
            if (s.charAt(i) == '(' || s.charAt(i) == '{') {
                stack.push(s.charAt(i));
            }
            else if (s.charAt(i) == ')') {
                if (stack.isEmpty()){
                    has = false;
                    System.out.println("Ошибка: лишняя закрывающая " + i);
                }
                else if (stack.pop() != '(') {
                    has = false;
                    System.out.println("Ошибка: не соответствующая скобка" + i);
                }
            }
            else if (s.charAt(i) == '}') {
                if (stack.isEmpty()) {
                    has = false;
                    System.out.println("Ошибка: лишняя закрывающая" + i);
                }
                else if (stack.pop() != '{') {
                    has = false;
                    System.out.println("Ошибка: не соответствующая скобка" + i);
                }
            }
            i++;
        }
        if (!stack.isEmpty() && has) {
            System.out.println("Ошибка: лишняя открывающая "+i);
        }
        else if (has) System.out.println("good");

    }

    /////////////////////////////////////////////////////////////////Task29     ....................

    public void skob(String s) { //Task29a
        int open = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                open++;
            }
            else if (s.charAt(i) == ')') {
                open--;
            }

            if (open < 0) throw new NullPointerException("it is wrong");
        }
    }

    public void Task30() {

        Scanner sc = new Scanner(System.in);

        String s = sc.next();
        String[] part = s.split(",");
        int buf, a, b;;

        //buf = Integer.parseInt(s);

        MyStack<Integer> st = new MyLinkedStack<Integer>();
        for (int i = 0; i < part.length; i++) {
            try {
                buf = Integer.parseInt(part[i]);
                st.push(buf);
            }
            catch (NumberFormatException e) {
                a = st.pop();
                b = st.pop();
                if (part[i].equals("+")){
                    st.push(a + b);
                }
                else if (part[i].equals("-")) {
                    st.push(a - b);
                }
                else if (part[i].equals("*")) {
                    st.push(a * b);
                }
                else if (part[i].equals("/")) {
                    st.push(a/b);
                }
            }
        }
        System.out.println(st.pop());
    }


    public int postfixExpression(String s) { ////////////////////////////////inet help me with it
        MyLinkedStack<Character> stack = new MyLinkedStack<Character>();
        for (int i = 0; i < s.length(); i++) {
            stack.push(s.charAt(i));
        }

        return 0;
    }







}
