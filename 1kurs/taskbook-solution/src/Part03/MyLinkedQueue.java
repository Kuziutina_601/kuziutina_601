package Part03;

import Part01.Elem;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by пользователь on 20.05.2017.
 */
public class MyLinkedQueue<T> implements MyQueue<T> {
    private List<Elem<T>> elems = new LinkedList<>();

    @Override
    public void offer(T x) {
        elems.add(new Elem<T>(x));
    }

    @Override
    public T peek() throws NullPointerException {
        if (elems.size() == 0) throw new NullPointerException();
        return elems.get(0).getValue();
    }

    @Override
    public T poll() throws NullPointerException {
        if (elems.size() == 0) throw new NullPointerException();
        T result = elems.get(0).getValue();
        elems.remove(0);
        return result;
    }

    @Override
    public boolean isEmpty() {
        return elems.size() == 0;
    }
}
