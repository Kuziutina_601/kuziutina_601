/**
 * Created by пользователь on 20.03.2017.
 */
public class Elem {
    private double[] value;
    private Elem next;

    public Elem(double[] value) {
        this.value = value;
    }

    public double[] getValue() {
        return value;
    }

    public void setValue(double[] value) {
        this.value = value;
    }

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }
}
