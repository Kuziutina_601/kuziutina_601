import java.util.Arrays;
import java.util.Random;

/**
 * Created by пользователь on 20.03.2017.
 */
public class List {

    public static Elem root;
    public static Elem tail;

    public static void add(double [] ar) {
        if (root == null) {
            root = new Elem(ar);
            tail = root;
        }
        else {
            Elem p = new Elem(ar);
            tail.setNext(p);
            tail = p;
        }
    }

    public static void getMin() {
        Elem p = root;
        Elem minElem = p;
        int min = 0;
        int count = 0;
        double minValue = Double.MAX_VALUE;
        double before = 0;
        double next;
        if (p == root) {
            before = p.getValue().length + summ(p);
        }
        while(p.getNext() != null) {
            count++;
            next = p.getNext().getValue().length + summ(p.getNext());
            if (before + next < minValue){
                minValue = before + next;
                min = count;
                minElem = p;
            }
            before = next;
            p = p.getNext();
        }

        System.out.println(min + "  " + Arrays.toString(minElem.getValue()));
        System.out.println(min + 1 + "  " + Arrays.toString(minElem.getNext().getValue()));
    }

    public static double summ(Elem ar) {
        double summ = 0;
        for (int i = 0; i < ar.getValue().length; i++) {
            summ += ar.getValue()[i];
        }
        return summ;
    }

    public static void check (double k) {
        Elem p = root;
        while (p != null) {
            if (summ(p) < k) {
                double[] array = new double[2 * p.getValue().length];
                for (int i = 0; i < p.getValue().length; i++) {
                    array[i] = p.getValue()[i];
                }
                int length = p.getValue().length;
                for (int i = 0; i < length; i++) {
                    array[length + i] = p.getValue()[i];
                }
                p.setValue(array);
            }
            p = p.getNext();
        }
    }

    public static void print() {
        Elem p = root;
        while (p!=null) {
            for (int i = 0; i < p.getValue().length; i++) {
                System.out.print(p.getValue()[i] + " ");
            }
            System.out.println();
            p = p.getNext();
        }
    }

    public static void main(String[] args) {
        Random random = new Random();
        List l = new List();
        for (int i = 0; i < 5; i++) {
            double[] ar = new double[random.nextInt(10) + 2];
            for (int j = 0; j < ar.length; j++) {
                ar[j] = random.nextDouble() * 10;
            }
            l.add(ar);
        }
        print();

        getMin();

        check(40);
        print();


    }
}
