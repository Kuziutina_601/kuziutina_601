public class TaskE {

	public static void main(String[] args) {

		final double EPS = 1e-9;

		double x = Double.parseDouble(args[0]);

		double summ = 1, y = 1, n = 1;

		do {
			y = y * x * (1.0 / n);
			n++;
			summ += y;
			System.out.println(summ);
		}
		while (Math.abs(y) > EPS);

		System.out.println(summ);
		System.out.println(Math.exp(x));
		
	}

}