public class TaskCos {

	public static void main(String[] args) {

		final double EPS = 1e-9;
		
		double x = Double.parseDouble(args[0]);
		int n = 1;
		double summ = 1, y = 1;

		do {
			y = y * x * x * (-1) * (1.0 / (n * (n+1)));
			summ += y;
			n = n + 2;
		}
		while(Math.abs(y) > EPS);

		System.out.println(summ);
		System.out.println(Math.cos(x));
	}

}