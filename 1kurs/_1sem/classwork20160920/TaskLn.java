public class TaskLn {

	public static void main(String[] args) {
		
		final double EPS = 1e-9;
		double x = Double.parseDouble(args[0]);
		double summ = 0, y = -1;
		int n = 1;

		x = x - 1;

		do {
			y = y * x * (-1.0) * (1.0 / n) ;
			summ += y;
			n++;
		}
		while (Math.abs(y) > EPS);

		System.out.println(summ);
		System.out.println(Math.log(x+1));

	}

}