public class TaskSin {

	public static void main(String[] args) {
		
		final double EPS = 1e-9;

		double x = Double.parseDouble(args[0]);
		int down = 1, n = 1;
		double sin, summ;

		sin = x;
		summ = x;

		do{
			down = down * (n + 1) * (n + 2);
			sin = sin * x * x * (-1.0) / down;
			summ += sin;
			n = n + 2;
		}
		while(Math.abs(sin) > EPS);

		System.out.println(summ);

	}

}