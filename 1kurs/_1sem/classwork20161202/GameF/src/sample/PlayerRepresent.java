package sample;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;

/**
 * Created by пользователь on 02.12.2016.
 */
public class PlayerRepresent {
    private Circle circle;
    private Player player;
    public void updateX(int x) {
        circle.setCenterX(x);
    }

    public PlayerRepresent(Group root, Player p) {
        this.player = p;
        circle = new Circle(p.getX(), p.getY(), 10);
        root.getChildren().add(circle);
    }

    public int getRadius() {
        return (int) circle.getRadius();
    }
}
