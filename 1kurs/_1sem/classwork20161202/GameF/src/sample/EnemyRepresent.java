package sample;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

/**
 * Created by пользователь on 04.12.2016.
 */
public class EnemyRepresent {
    private Circle circle;
    private Enemy enemy;
    public void updateY(int y) {
        circle.setCenterY(y);
    }

    public EnemyRepresent(Group root, Enemy e) {
        this.enemy = e;
        circle = new Circle(enemy.getX(), enemy.getY(), 5);
        circle.setStyle("-fx-fill: red");
        root.getChildren().add(circle);
    }

    public int getRadius() {
        return (int) circle.getRadius();
    }

}
