package sample;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;

/**
 * Created by пользователь on 02.12.2016.
 */
public class Player {

    private int x;
    private int y;
    private int hp;
    private int v;
    private int score;
    private PlayerRepresent pr;

    public Player(int x, int y, Group group) {
        this.x = x;
        this.y = y;
        this.hp = 100;
        this.v = 10;
        this.score = 0;
        pr = new PlayerRepresent(group, this);
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getRadius() {
        return pr.getRadius();
    }

    public int getScore() {
        return score;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHp() {
        return hp;
    }

    public void updateHp(int det) {this.hp = this.hp - det;}

    public void updateX(int dir) {
        this.x = x + v * dir;
        pr.updateX(this.x);
    }
}
