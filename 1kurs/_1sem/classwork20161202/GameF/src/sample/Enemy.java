package sample;

import javafx.scene.Group;

/**
 * Created by пользователь on 04.12.2016.
 */
public class Enemy {
    private int x;
    private int y;
    private int v;
    private EnemyRepresent er;

    public Enemy(int x, int y, Group root) {
        this.x = x;
        this.y = y;
        er = new EnemyRepresent(root, this);
    }

    public void updateY(int y) {
        this.y = this.y + y;
        er.updateY(this.y);
    }

    public int getRadius() {
        return er.getRadius();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
