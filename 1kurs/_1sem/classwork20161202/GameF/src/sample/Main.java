package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    private final static int WIDTH = 400;
    private final static int HEIGHT = 400;
    private static int kill = -1;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Player player = new Player(WIDTH / 2, HEIGHT - 30, root);
        Enemy[] enemies = new Enemy[50];
        final int[] enemies_number = {0};
        final double[] m1 = new double[2];

        for (int i = 0; i < 50; i++){
            enemies[i] = new Enemy(-10, HEIGHT, root);
        }

        new AnimationTimer() {
            long was = System.currentTimeMillis();
            @Override
            public void handle(long now) {
                if (player.getHp() <= 0)
                    this.stop();
                for (int i = 0; i < 50; i++) {
                    if (enemies[i].getY() <= HEIGHT + enemies[0].getRadius()) {
                        enemies[i].updateY(1);
                        m1[0] = (enemies[i].getX() - player.getX()) * (enemies[i].getX() - player.getX());
                        m1[1] = (enemies[i].getY() - player.getY()) * (enemies[i].getY() - player.getY());
                        if (enemies[i].getRadius() + player.getRadius() > (int)(Math.sqrt(m1[0] + m1[1]))){
                            enemies[i].updateY(HEIGHT);
                            player.updateHp(1);
                        }
                        else {
                            if (enemies[i].getY() > HEIGHT + enemies[i].getRadius())
                                player.updateHp(1);
                        }
                    }
                }
                Text score = new Text(WIDTH/4, 10, "Score: " + player.getScore());
                if (System.currentTimeMillis() - was > 1000) {
                    enemies[enemies_number[0]] = new Enemy((int) (Math.random()*WIDTH), 0, root);
                    was = System.currentTimeMillis();
                    enemies_number[0] = (enemies_number[0] + 1) % 50;
                }
            }
        }.start();

        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case RIGHT:
                    player.updateX(1);
                    System.out.println(player.getHp());
                    break;
                case LEFT:
                    player.updateX(-1);
                    break;
                case SPACE:
                    Circle bullet = new Circle(player.getX(), player.getY(), 3);
                    root.getChildren().add(bullet);
                    new AnimationTimer() {
                        @Override
                        public void handle(long now) {
                            if (player.getHp() <= 0)
                                this.stop();
                            bullet.setCenterY(bullet.getCenterY() - 5);
                            if (bullet.getCenterY() <= 0)
                                this.stop();
                            for (int i = 0; i < 50; i++){
                                if (enemies[i].getY() <= HEIGHT) {
                                    double del = (bullet.getCenterX() - enemies[i].getX()) * (bullet.getCenterX() - enemies[i].getX());
                                    double del2 = (bullet.getCenterY() - enemies[i].getY()) * (bullet.getCenterY() - enemies[i].getY());
                                    if (bullet.getRadius() + enemies[i].getRadius() > (int) (Math.sqrt(del + del2))) {
                                        enemies[i].updateY(HEIGHT);
                                        bullet.setCenterY(-1);
                                        player.setScore(1);
                                    }
                                }
                            }
                        }
                    }.start();
            }
        });


        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
        root.requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
