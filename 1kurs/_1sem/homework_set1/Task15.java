import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 115
*/

public class Task15 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int gip  = sc.nextInt();
		int kat1 = sc.nextInt();
		double kat2 = Math.sqrt(gip * gip - kat1*kat1);
		double r = (kat1 + kat2 - gip) / 2;

		System.out.print("Kat2 = " + kat2 + " radius = " + r);
	}

}