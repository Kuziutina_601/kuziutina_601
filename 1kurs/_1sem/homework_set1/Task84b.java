 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 184b
*/

public class Task84b {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double x = sc.nextDouble();
		double result = 0, mn = 1;
		int  l = 0;

		do{
			l++;
			mn *= x;
			result = result + Math.sin(mn);
		}
		while (l < n);

		System.out.println(result);

	}

}