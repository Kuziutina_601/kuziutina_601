import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 117
*/

public class Task17 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double s1 = 3.14 * 20 * 20;
		double s2 = sc.nextDouble();

		s2 = 3.14 * s2 * s2;

		System.out.print(s2 - s1);
	}

}