 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 135b
*/

public class Task35b {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double z = sc.nextDouble();
		double m, k;

		m = x + y + (z / 2);
		k = x * y * z;

		if (m < k)
			System.out.println(m * m + 1);
		else
			System.out.println(k * k + 1);
	}

}