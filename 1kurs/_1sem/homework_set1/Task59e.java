 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 159и
*/

public class Task59e {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();
		boolean k = false;
		double l = 1.0/3;

		if (x < 0) {
			if ((y < -x) && (y < -2*x -1) && (y > l*x - l)){
				System.out.println("YES");
				k = true;
			}
		}
		else{
			if ((y > l*x - l) && (y < 0)){
				System.out.println("YES");
				k = true;
			}
		}
		if (!k)
			System.out.println("NO");
	}

}