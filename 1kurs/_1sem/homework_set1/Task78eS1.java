 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 178d
*/

public class Task78dS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();
		int n = sc.nextInt();
		double p = 1;

		for (int i = 0; i < n+1; i++){
			p*= (a + n*i);
		}

		System.out.print(p);
	}
}