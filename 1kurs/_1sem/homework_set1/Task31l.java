 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 131l
*/

public class Task31l {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();

		a = a * a; //2
		a = a * a; //4
		a = a * a; //8
		a = a * a; //16
		a = a * a; //32
		a = a * a; //64

		System.out.println(a);
	}

}