import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 167d
*/

public class Task67d {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		n = n%100;
		n = n/10;
		System.out.print(n); 
	}

}