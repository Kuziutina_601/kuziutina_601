 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 111e
*/

public class Task11e {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double z = sc.nextDouble();
		double a, b, up, down;

		up = Math.sin(x + y);
		up = up * up + 1;
		down = 1 + x* x * y * y;
		down = (2 * x) / down;
		down = Math.abs(x - down) + 2;
		a = up / down + x;

		up = Math.atan(1.0 / z);
		up = Math.cos(up);
		b = up * up;

		System.out.println(a + "  " + b);

	}

}