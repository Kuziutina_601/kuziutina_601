import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 118
*/

public class Task18 {
	public static double side (double a, int r){
		return(r * Math.sin(a * 3.14 / 180));
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int r = sc.nextInt();
		double a = sc.nextInt();
		double b = sc.nextInt();
		double c = sc.nextInt();

		r = r * 2;

		a = side(a, r);
		b = side(b, r);
		c = side(c, r);

		System.out.print("a = " + a + ", b = " + b + ", c = " + c);
	}

}