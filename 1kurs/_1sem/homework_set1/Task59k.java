 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 159k
*/

public class Task59k {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();

		if ((y > 1) || ((y > -x) && (y > x)))
			System.out.println("YES");
		else
			System.out.println("NO");
	}
}