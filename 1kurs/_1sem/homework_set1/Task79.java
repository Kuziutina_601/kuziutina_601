 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 179
*/

public class Task79 {

	public static void main(String[] args) {
		double result = 1, arg = 0;

		do {
			arg += 0.1;
			result = result * (1 + Math.sin(arg));
		}
		while (arg < 10);

		System.out.println(result);
	}

}