 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 159з
*/

public class Task59z {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();

		if ((y < -x) && (y < x) && (x < 1) && (x > -1) && (y > -2)){
			System.out.println("YES");
		}
		else 
			System.out.println("NO");
	}

}