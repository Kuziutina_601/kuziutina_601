 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 1114e
*/

public class Task114e {

	public static void main(String[] args) {
		
		int down = 1;
		double result = 1;

		for (int i = 1; i < 11; i++){
			down = down * i;
			result = result * (2 + 1.0/down);
		}

		System.out.println(result);
	
	}
}