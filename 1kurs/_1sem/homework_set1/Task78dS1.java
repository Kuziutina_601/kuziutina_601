import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 178d
*/

public class Task78dS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();
		int n = sc.nextInt();
		double summ = 1.0/a, zn = a;
		int k = 1, p = 1; 

		for (int i = 1; i < n+1; i++){
			p *= 2;
			for (int j = k; j < p; j++){
				zn *= a;
			}
			summ += 1.0 / zn;
			k = p;
		}

		System.out.print(summ);
	}

}