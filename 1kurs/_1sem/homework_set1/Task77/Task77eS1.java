import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177e
*/

public class Task77eS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double p = 0;

		for (int i = 0; i < n; i++){
			p = Math.sqrt(2 + p);
		}

		System.out.print(p);
	}

}