import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177c
*/

public class Task77cS1 {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);

		double p = 1;
		int n = sc.nextInt();

		for (int i = 1; i < n+1; i++){
			p *= (1 + 1.0/(i*i));
		}

		System.out.print(p);
	}

}