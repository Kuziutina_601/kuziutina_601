import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177f
*/

public class Task77fS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double sum_sin = 0, sum_cos = 0, p = 1;

		for (int i = 1; i < n+1; i++){
			sum_sin += Math.sin(3.14*i / 180);
			sum_cos += Math.cos(3.14*i / 180);
			p = p * sum_cos/sum_sin;
		} 

		System.out.print(p);
		
	}

}