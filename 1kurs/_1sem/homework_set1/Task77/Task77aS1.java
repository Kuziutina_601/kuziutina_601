import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177a
*/

public class Task77aS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int p = 1;
		for (int i = 0; i < n; i++){
			p *= 2;
		}
		System.out.println("2^" + n + "=" + p);
	}

}