import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177b
*/

public class Task77bS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int f = 1;

		for (int i = 2; i <= n; i++){
			f *= i;
		}

		System.out.print(n + "! = " + f);
	}

}