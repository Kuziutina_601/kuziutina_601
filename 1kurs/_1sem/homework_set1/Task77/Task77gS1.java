import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177g
*/

public class Task77gS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double answer = 0;

		for (int i = n; i > 0; i--) {
			answer = Math.sqrt(3*i + answer);
		}

		System.out.print(answer);
		
	}

}