import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 177d
*/

public class Task77dS1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double answer = 0, sum = 0;

		for (int i = 1; i < n+1; i++){
			sum += Math.sin(3.14*i / 180);
			answer += 1.0/sum;
		}

		System.out.print(answer);
		
	}

}