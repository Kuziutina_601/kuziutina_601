 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 159ж
*/

public class Task59g {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();

		if((y < 2*x+2) && (y < -2*x + 2) && (y > -1)){
			System.out.println("YES");
		}
		else
			System.out.println("NO");


	}

}