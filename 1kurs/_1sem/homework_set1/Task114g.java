 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 1114ж
*/

public class Task114g {

	public static void main(String[] args) {
		double result = 1;

		for (int i =2; i < 101; i++){
			result = result * (((i + 1)*1.0) / (i + 2));
		}

		System.out.println(result);
	}
}