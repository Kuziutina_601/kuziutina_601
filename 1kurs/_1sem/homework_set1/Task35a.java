 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 135a
*/

public class Task35a {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double z = sc.nextDouble();
		double m, k;

		m = x + y + z;
		k = x * y * z;

		if (m < k)
			System.out.println(k);
		else
			System.out.println(m);
	}

}