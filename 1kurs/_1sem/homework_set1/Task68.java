import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 168
*/

public class Task68 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int [] a = new int[10];
		int n = sc.nextInt();
		int x = n, sch = 0, k1 = 1;
		boolean status = true, state = true;

		for (int i = 0; i < 10; i++){
			a[i] = 0;
		}

		while (x > 0){
			a[x%10]++;
			sch++;
			x /= 10;
		}

		for (int i = 1; i < sch; i++){
			k1 *=10;
		}
		
		for (int i = 0; i < sch/2 && status; i++){
			if (n%10 != n/k1){
				status = false;
				System.out.println("NO");
			}
			n  %= k1;
			k1 /= 100;
			n  /= 10;
		}

		if (status)
			System.out.println("YES");

		status = false;
		for (int i = 0; i < 10 && !status; i++){
			if(a[i] == 3){
				status = true;
				System.out.print(" 3 digit and not differend");
			}
			if (a[i] > 1)
				state = false;
		}

		if(!status)
			System.out.print(" not 3");
		if (state)
			System.out.print(" differend");
		else
			System.out.print(" not differend");

		
	}

}