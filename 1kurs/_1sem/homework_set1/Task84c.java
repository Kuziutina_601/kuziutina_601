 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 184c
*/

public class Task84c {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.out);

		int n = sc.nextInt();
		double x = sc.nextDouble();
		double result = 0, mn = x;
		int l = 1;

		do {
			l++;
			mn = Math.sin(mn);
			result += mn;
		}
		while (l < n);

		System.out.println(result);

	}

}