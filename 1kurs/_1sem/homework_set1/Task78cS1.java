import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 178c
*/

public class Task78cS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();
		int n = sc.nextInt();
		double p = 1, summ = 0;

		for (int i = 0; i < n+1; i++) {
			p *= (a+i);
			summ += 1.0/p;
		}

		System.out.print(summ);
	}

}