 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 184a
*/

public class Task84a {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double x = sc.nextDouble();
		double result = 0, ch = 1;
		int l = 0;

		x = Math.sin(x);

		do{
			l++;
			ch = ch * x;
			result += ch;
		}
		while (l < n);

		System.out.println(result);
		
	}

}