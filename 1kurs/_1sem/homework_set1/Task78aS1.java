import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 178a
*/

public class Task78aS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();
		int n = sc.nextInt();
		double p = 1;

		for (int i = 0; i < n; i++){
			p *= a;
		}

		System.out.print(p);
	}

}