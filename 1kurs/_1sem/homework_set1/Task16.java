import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 116
*/

public class Task16 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double length = sc.nextDouble();

		double r = length / (6.28);
		r = 3.14 * r * r;
		System.out.print("S = " + r);
	}

}