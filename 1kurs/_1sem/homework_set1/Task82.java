 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 182
*/

public class Task82 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		int l = 0;
		double result = 1;

		do{
			result = result * ((x - l - 2.0)/(x - l - 1));
			l = l + 2;
		}
		while (l < 64);

		System.out.print(result);
	}
}