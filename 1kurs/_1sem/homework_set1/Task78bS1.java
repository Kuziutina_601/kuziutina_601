import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 178b
*/

public class Task78bS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();
		int n = sc.nextInt();
		double p = 1;

		for (int i = 0; i < n; i++) {
			p *= (a-i);
		}

		System.out.print(p);
	}

}