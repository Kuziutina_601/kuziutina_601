 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 183a
*/

public class Task83a {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double a = sc.nextDouble();
		int down = 0;
		double result = 0;

		do{
			down++;
			result = result + (1.0/down);
		}
		while (result < a);

		System.out.println(result);

	}

}