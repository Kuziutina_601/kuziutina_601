 import java.util.Scanner;

/**
* @author Kuziutina Sofia
* 11-601
* 181
*/

public class Task81 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);

		double x = sc.nextDouble();
		double a = sc.nextDouble();
		int n = sc.nextInt();

		int m = 0;
		double result = x;

		do{
			m++;
			result += a;
			result = result * result;			
		}
		while (m < n);

		result += a;

		System.out.print(result);
	}

}