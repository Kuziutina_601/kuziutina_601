package Fighting;

public class Gamer{

	private int hp;
	private String name;

	public Gamer(String name) {
		this.hp = 100;
		this.name = name;
	}

	public void hit(int force) {
		hp = hp - force;
	}

	public int getHp() {
		return hp;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return name + "  " + hp + " hp";
	}

}