package Fighting;

import java.util.Random;
import java.util.Scanner;

public class Game{

	private static Gamer p1, p2;

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		int force = 9, random = 9;

		System.out.println("Enter name first player");
		p1 = new Gamer(sc.nextLine());
	
		System.out.println("Enter name second player");
		p2 = new Gamer(sc.nextLine());


		while(true) {
			
			do {
				System.out.println(p1.getName() + ", force hit 1 - 9");
				force = sc.nextInt();
			}
			while((force > 9) || (force < 1));
			random = r.nextInt(10) + 1;
			if (random >= force){
				p2.hit(force);
				System.out.println("you hitted");
			}
			else
				System.out.println("you missed");

			if (p2.getHp() <= 0) {
				System.out.println(p1.getName() + " win");
				break;
			}

			showValue();

			
			do {
				System.out.println(p2.getName() + ", force hit 1 - 9");
				force = sc.nextInt();
			}
			while((force > 9) || (force < 1));
			random = r.nextInt(10) + 1;
			if (random >= force){
				p1.hit(force);
				System.out.println("you hitted");
			}
			else
				System.out.println("you missed");
			if (p1.getHp() <= 0) {
				System.out.println(p1.getName() + " win");
				break;
			}

			showValue();
		}
	}

	private static void showValue() {
			System.out.println();
			System.out.println(p1);
			System.out.println(p2);
			System.out.println();
	}

}