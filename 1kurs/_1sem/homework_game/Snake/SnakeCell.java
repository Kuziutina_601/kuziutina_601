package Snake;

public class SnakeCell {

    private int x;
    private int y;

    public SnakeCell() {
        x = -1;
        y = -1;
    }

    public SnakeCell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() { return x; }

    public int getY() { return y; }

    public void setX(int x) { this.x = x; }

    public void setY(int y) { this.y = y; }
}
