package Snake;

public class Field {

    public char[][] field = new char[Game.HIGHT_FIELD][Game.LENGTH_FIELD];
    private SnakeCell  endSnake;
    private Food lastfood;

    public Field() {
        for (int i = 0; i < Game.HIGHT_FIELD; i++) {
            for (int j = 0; j < Game.LENGTH_FIELD; j++) {
                field[i][j] = '\'';
            }
        }
    }

    public void render(Snake snake) {
        for (int i = 0; i < snake.getLength(); i++) {
            field[snake.snakeCells[i].getY()][snake.snakeCells[i].getX()] = '+';
        }
        endSnake = snake.snakeCells[snake.getLength() - 1];
    }

    public void render(Food food) {
        field[food.getY()][food.getX()] = '*';
        lastfood = food;
    }

    public void show() {
        for (int i = 0; i < Game.HIGHT_FIELD; i++) {
            for (int j = 0; j < Game.LENGTH_FIELD; j++){
                System.out.print(field[i][j]);
            }
            System.out.println();
        }
        clean();
    }

    public void clean(){
        field[lastfood.getY()][lastfood.getX()] = '\'';
        field[endSnake.getY()][endSnake.getX()] = '\'';
    }



}
