package Snake;

public class Snake {

    public SnakeCell[] snakeCells;
    private int length = 0;

    public int getLength() { return length; }

    public void setLength(int length) { this.length = length; }

    public Snake() {
        snakeCells = new SnakeCell[Game.HIGHT_FIELD * Game.LENGTH_FIELD];
        //System.out.print(Game.LENGTH_FIELD * Game.HIGHT_FIELD);
        for (int i = 0; i < Game.LENGTH_FIELD * Game.HIGHT_FIELD; i++) {
            snakeCells[i] = new SnakeCell();
        }
        snakeCells[0].setX(5);
        snakeCells[0].setY(5);
        snakeCells[1].setX(6);
        snakeCells[1].setY(5);
        setLength(2);
    }

    public void reloadSnake(int x, int y){
        setLength(getLength()+1);
        for (int i = getLength()-2; i >= 0; i--) {
            snakeCells[i+1].setX(snakeCells[i].getX());
            snakeCells[i+1].setY(snakeCells[i].getY());
        }
        snakeCells[0].setX(x);
        snakeCells[0].setY(y);
    }

    public void reloadSnake(char c) {
        boolean need = false;
        switch (c) {
            case 'w' : {
                if (snakeCells[0].getY()-1 != snakeCells[1].getY())
                    need = true;
                break;
            }
            case 'a' : {
                if (snakeCells[0].getX()-1 != snakeCells[1].getX())
                    need = true;
                break;
            }
            case 's' : {
                if (snakeCells[0].getY() + 1 != snakeCells[1].getY())
                    need = true;
                break;
            }
            case 'd' : {
                if (snakeCells[0].getX() + 1 != snakeCells[1].getX())
                    need = true;
                break;
            }
        }
        if (need) {
            for (int i = getLength() - 1; i > 0; i--) {
                snakeCells[i].setX(snakeCells[i - 1].getX());
                snakeCells[i].setY(snakeCells[i - 1].getY());
            }
            switch (c) {
                case 'w':
                    snakeCells[0].setY(snakeCells[0].getY() - 1);
                    break;
                case 'a':
                    snakeCells[0].setX(snakeCells[0].getX() - 1);
                    break;
                case 's':
                    snakeCells[0].setY(snakeCells[0].getY() + 1);
                    break;
                case 'd':
                    snakeCells[0].setX(snakeCells[0].getX() + 1);
                    break;
            }
        }
    }

}
