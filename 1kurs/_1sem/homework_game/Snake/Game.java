package Snake;

import java.util.Random;
import java.util.Scanner;

public class Game {

    public static final int HIGHT_FIELD = 10;
    public static final int LENGTH_FIELD = 10;
    private static boolean game = true;
    static Random r = new Random();
    private static Scanner sc;

    public static void main(String[] args) {
        start();
    }

    public static void start() {
        sc = new Scanner(System.in);
        char next;
        Field field = new Field();
        Snake snake = new Snake();
        Food food = new Food(r.nextInt(LENGTH_FIELD), r.nextInt(HIGHT_FIELD));
        while (game) {
            field.render(food);
            field.render(snake);
            field.show();
            next = sc.next().charAt(0);
            if (checkFood(next, food, snake)){
                System.out.println("next = " + next);
                snake.reloadSnake(food.getX(), food.getY());
                do {
                    food.setXY(r.nextInt(Game.LENGTH_FIELD), r.nextInt(Game.HIGHT_FIELD));
                }while(!checkCreateFood(food, snake));

            }
            else {
                snake.reloadSnake(next);
                checkWall(snake);
                checkSelf(snake);
            }
            if (!game || snake.getLength() == Game.LENGTH_FIELD*Game.HIGHT_FIELD)
                break;

        }

        if(game)
            System.out.print("YOU WIN!!");
        else
            System.out.print("GAME OVER!");
    }

    public static boolean checkCreateFood(Food f, Snake s) {
        for (int i = 0; i < s.getLength(); i++) {
            if ((s.snakeCells[i].getX() == f.getX()) && (s.snakeCells[i].getY() == f.getY()))
                return false;
        }
        return true;
    }

    public static void checkWall(Snake s) {
        if ((s.snakeCells[0].getX() < 0) || (s.snakeCells[0].getY() < 0))
            game = false;
        if ((s.snakeCells[0].getX() >= LENGTH_FIELD) || (s.snakeCells[0].getY() >= HIGHT_FIELD))
            game = false;
    }

    public static void checkSelf(Snake s) {
        for (int i = 1; i < s.getLength() && game; i++) {
            if ((s.snakeCells[0].getX() == s.snakeCells[i].getX()) && (s.snakeCells[0].getY() == s.snakeCells[i].getY()))
                game = false;
        }
    }

    public static boolean checkFood(char c, Food f, Snake s) {
        switch (c) {
            case 'w':{
                if ((f.getX() == s.snakeCells[0].getX()) && (f.getY() == s.snakeCells[0].getY()-1))
                    return true;
                break;
            }
            case 'a':{
                if ((f.getX() == s.snakeCells[0].getX() - 1) && (f.getY() == s.snakeCells[0].getY())) 
                    return true;
                break;
            }
            case 's':{
                if ((f.getX() == s.snakeCells[0].getX()) && (f.getY() == s.snakeCells[0].getY()+1))
                    return true;
                break;
            }
            case 'd':{
                if ((f.getX() == s.snakeCells[0].getX() + 1) && (f.getY() == s.snakeCells[0].getY()))
                    return true;
                break;
            }
        }
        return false;
    }

}
