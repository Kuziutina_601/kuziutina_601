import java.util.Scanner;

public class SearchEasy {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		String s1 = sc.nextLine();
		String s2 = sc.nextLine();

		boolean res = false;

		int k = 0;
		int i = 0;

		while (k < s1.length()){
			if (s1.charAt(k) == s2.charAt(i)) {
				i++;
			}
			else
				i = 0;

			if (i == s2.length()){
				res = true;
				break;
			}
			k++;
		}

		if (res)
			System.out.print("  " + (k - i + 2));
		else
			System.out.print("NO");
	}
}