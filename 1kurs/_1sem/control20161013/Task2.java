import java.util.Scanner;

// number 2 b

/**
* @author Kuziutina Sofia
* 11-601
* 2b
*/

public class Task2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		int m, k = 0, d = 0;

		//int[] array = new int[n];

		for (int i = 0; i < n; i++) {
			m = sc.nextInt();

			while ((m > 0) && (d < 3)){
				if ((m%10) % 2 == 1)
					d++;
				m /= 10;
			}
			if (d == 2)
				k++;
			d = 0;
		}

		if (k < 4)
			System.out.print("Yes");
		else
			System.out.print("No");
		
		
	}

}