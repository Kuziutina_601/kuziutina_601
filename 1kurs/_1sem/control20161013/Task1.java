import java.util.Scanner;

// number 1 e

/**
* @author Kuziutina Sofia
* 11-601
* 1e
*/

public class Task1 {

	public static double ln (double x){
		final double EPS = 1e-9;
		int k = -1, i = -1;
		double summ = 0, up = 1, summ1 = 0;

		do{
			i++;
			k = -k;
			if (k > 0)
				summ1 = 0;
			up *= x;
			summ1 += k * up/(i+1);
			summ += summ1;
			
		}
		while(EPS < Math.abs(summ1));

		return(summ);

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x = sc.nextDouble();
		double y;

		y = 5 * x;
		x = 0.8 * x * x;
		x = ln(x);
		y += x;

		System.out.println("Answer " + y);

		
	}

}