import java.util.Scanner;

//numer 3 b

/**
* @author Kuziutina Sofia
* 11-601
* 3b
*/

public class Task3 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int [][] array = new int[n][n];

		int zn = 0, i = 0, j = 0, sum = 0;
		array[n-1][n-1] = -1;

		while (zn < n * n) {
			for (int k = i; j <= i ; k--){
				array[k][j] = zn;
				zn++;
				j++;
			}

			if (i < n-1){
				i++;
			}
			sum++;
			j = sum - i;
		}

		for (int m = 0; m < n; m++){
			for (int l = 0; l < n; l++){
				System.out.print(array[m][l] + "  ");
			}
			System.out.println();
		}
	}
}