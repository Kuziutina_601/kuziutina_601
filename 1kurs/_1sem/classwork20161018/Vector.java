import java.util.Scanner;

public class Vector {

	private double x,y;

	Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	void print(){
		System.out.println (x + "  " + y);
	}

	public double getX (){
		return x;
	}

	public double getY (){
		return y;
	}

	public void setX (double x){
		this.x = x;
	}

	public void setY (double y){
		this.y = y;
	}

	public Vector summ (Vector v3) {
		v3.setX(this.x + v3.getX()); //// we can this.x + v3.x 
		v3.setY(this.y + v3.getY());
		return(v3); 
	}

	public double scalar_product (Vector v3) {
		double result;
		result = this.x * v3.getX() + this.y * v3.getY();
		return result;
	}

	public double cosin (Vector v3) {
		double result, down;
		result = this.scalar_product(v3);
		down = this.lenght() * v3.lenght();
		result = result / down;
		return result;
	}

	public double lenght () {
		double lenght = 0;
		lenght = Math.sqrt(this.x * this.x + this.y * this.y);
		return lenght;
	}


}