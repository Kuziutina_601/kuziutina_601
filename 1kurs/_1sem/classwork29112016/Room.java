

public class Room {

	private String roomId;
	private String roomName;
	private int beds;
	private String bedType;
	private int maxOccupancy;
	private int basePrice;
	private String decor;

	public String getRoomId() {
		return roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public int getBeds() {
		return beds;
	}

	public String getBedType() {
		return bedType;
	}

	public int getMaxOccupancy() {
		return maxOccupancy;
	}

	public int getBasePrice() {
		return basePrice;
	}

	public String getDecor() {
		return decor;
	}

	public Room(String roomId, String roomName, String beds, String bedType, String maxOccupancy, String basePrice, String decor) {
		this.roomId = roomId.substring(1, roomId.length() - 2);
		this.roomName = roomName.substring(1, roomName.length() - 2);
		this.beds = Integer.parseInt(beds);
		this.bedType = bedType.substring(1, bedType.length() - 2);
		this.maxOccupancy = Integer.parseInt(maxOccupancy);
		this.basePrice = Integer.parseInt(basePrice);
		this.decor = decor.substring(1, decor.length() - 2);
	}


}