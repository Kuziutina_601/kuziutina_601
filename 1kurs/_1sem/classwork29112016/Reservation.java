

public class Reservation{

	private int code;
	private String roomName;
	private String checkIn;
	private String checkOut;
	private double rate;
	private String lastName;
	private String firstName;
	private int adults;
	private int kids;
	private Room room;

	public Reservation(String code, String roomName, String checkIn, String checkOut,
						String rate, String lastName, String firstName, String adults, String kids) {
		this.code = Integer.parseInt(code);
		this.roomName = roomName.substring(1, roomName.length() - 2);
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.rate = Double.parseDouble(rate);
		this.lastName = lastName.substring(1, lastName.length() - 2);
		this.firstName = firstName.substring(1, firstName.length() - 2);
		this.adults = Integer.parseInt(adults);
		this.kids = Integer.parseInt(kids);
		for (int i = 0; i < Gener.room.length; i++) {
			if (Gener.room[i].getRoomId().equals(roomName)) {
				room = Gener.room[i];				
				break;
			}			
		}
	}

	public String getLastName() {
		return lastName;
	}

	public Room getRoom() {
		return room;
	}
}