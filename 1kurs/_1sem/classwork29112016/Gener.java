import java.util.Scanner;
import java.io.*;
import java.util.regex.*;

public class Gener {

	public static Room[] room;

	public static void main(String[] args) throws IOException{
		
		Scanner r = new Scanner(new File("Rooms.csv"));
		Scanner reserv = new Scanner(new File("Reservations.csv"));
		final int ROOM_SIZE = 10;
		int n = 0;

		room = new Room[10];
		Reservation[] reservation;

		String buf = "";
		String[] column;

		buf = r.nextLine();
		for (int i = 0; i < ROOM_SIZE; i++) {
			buf = r.nextLine();
			column = buf.split(",");

			room[i] = new Room(column[0], column[1], column[2], column[3], column[4], column[5], column[6]);

		}

		buf = reserv.nextLine();
		do {
			buf = reserv.nextLine();
			n++;
		}
		while(reserv.hasNextLine());
		reserv.close();

		Scanner sc = new Scanner(new File("Reservations.csv"));
		reservation = new Reservation[n];

		buf = sc.nextLine();

		for (int i = 0; i < n; i++) {
			buf = sc.nextLine();
			column = buf.split(",");

			checkDate(column[2]);
			checkDate(column[3]);

			reservation[i] = new Reservation(column[0], column[1], column[2], column[3], 
												column[4], column[5], column[6], column[7], column[8]);
		}

		sortPrice();

		for (int i = 0; i < room.length; i++) {
			System.out.println(room[i].getBasePrice());
		}
	}


	public static void checkDate(String s) throws NumberFormatException {
		String pattern = "\'[0-9]{2}-[A-Z]{3}-[0-9]{2}\'";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(s);
		
		if (m.matches()) {

		}
		else{
			throw new NumberFormatException();
		}
		
	}

	public static void sortPrice() {
		Room buf;
		for (int i = 0; i < room.length; i++) {
			for (int j = i + 1; j < room.length; j++) {
				if (room[i].getBasePrice() > room[j].getBasePrice()){
					buf = room[i];
					room[i] = room[j];
					room[j] = buf;
				}
			}
		}
	}
}