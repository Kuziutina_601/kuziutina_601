public class Task42a {

	public static void main(String[] args) {

		final double EPS = 1e-9;
		double x, x1, z;
		int n = 2;

		x1 = 1;
		x  = -1/2;
		z  = 1;


		while (Math.abs(x1 - x) > EPS){

			n++;
			x1 = x;
			x  = z *(1.0) / n;
			z  = -z;
		}

		System.out.print(x);
		
	}

}