public class Task42c {

	public static void main(String[] args) {

		final double EPS = 1e-9;
		double x1, x;
		int n = 2;

		x = -0.999;

		do {
			x1 = x;
			x  = -x * 0.999;
			n++;  
		}
		while (Math.abs(x1-x) > EPS);

		System.out.print(x);
	}

}