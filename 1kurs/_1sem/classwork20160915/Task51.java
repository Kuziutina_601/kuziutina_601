public class Task51 {

	public static void main(String[] args) {

		final double EPS = 1e-9;
		double y, summ, summ2;
		int n = 2;
		summ = 0.25;
		y = 1;

		do{
			n++;
			summ2 = summ;
			y = y + n - 1;
			summ = y / (n*n);
				
		}
		while(Math.abs(summ2 - summ) > EPS);

		System.out.print(summ);

	}

}