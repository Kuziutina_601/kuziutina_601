public class Task55 {

	public static void main(String[] args) {

		final double EPS = 1e-9;
		double summ, x, ch = 2;
		int n = 2;

		summ = 0.5;

		do{
			ch *= 2;
			x = (2*n - 1.0)/ch;
			summ += x;
			n++;

		}
		while (Math.abs(x) > EPS);

		System.out.print(summ);

	}

}