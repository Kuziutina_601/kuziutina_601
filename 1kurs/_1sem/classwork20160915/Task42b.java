public class Task42b {

	public static void main(String[] args) {

		final double EPS = 1e-9;
		double x1, x;
		int n = 1;

		x = 1;
		
		do {
			n++;
			x1 = x;
			x  = x * (1.0 / n);
		}
		while (Math.abs(x1 - x) > EPS);

		System.out.print(x);

	}

}