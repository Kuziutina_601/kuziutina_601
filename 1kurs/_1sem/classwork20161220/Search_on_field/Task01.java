import java.util.Scanner;
import java.io.*;

public class Task01 {

	final static int SIZE_X = 5;
	final static int SIZE_Y = 5;
	static char[][] character;

	public static void main(String[] args) throws IOException{
		Scanner sc = new Scanner(new File("1.txt"));
		Scanner word = new Scanner(new File("2.txt"));

		String buf;
		character = new char[SIZE_Y][SIZE_X];
		for (int i = 0; i < SIZE_Y; i++) {
			buf = sc.nextLine();
			for (int j = 0; j < SIZE_X; j++) {
				character[i][j] = buf.charAt(j);
				System.out.print(character[i][j] + " ");
			}
			System.out.println();
		}

		do {
			buf = word.nextLine();
			for (int i = 0; i < SIZE_Y; i++) {
				for (int j = 0; j < SIZE_X; j++) {
					if (character[i][j] == buf.charAt(0)){
						//System.out.println(i + "  " + j);
						if (serch(buf, i, j))
							System.out.println(buf);
					}
				}
			}
		}
		while(word.hasNext());
	}

	public static boolean serch(String buf, int i, int j) {
		boolean has = false, all = true;
		if ((j + buf.length() - 1 < SIZE_X) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i][j + k])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}

		if ((i + buf.length() - 1 < SIZE_Y) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i + k][j])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}

		//System.out.println("kljhd" + j + " - " + buf.length() + " +1 = " +(j - buf.length() + 1) + has);
		if ((j - buf.length() + 1 >= 0) && (!has)){
			//System.out.println("zashel");
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i][j - k])
					all = false;
				//System.out.println(k + "  " + all);
			}
			if (all)
				has = true;
			all = true;
		}

		if ((i - buf.length() + 1 >= 0) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i - k][j])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}


		if ((j + buf.length() - 1 < SIZE_X) && (i + buf.length() - 1 < SIZE_Y) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i + k][j + k])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}

		if ((j - buf.length() + 1 >= 0) && (i + buf.length() - 1 < SIZE_Y) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i + k][j - k])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}

		if ((j - buf.length() + 1 >= 0) && (i - buf.length() + 1 >= 0) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i - k][j - k])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}

		if ((j + buf.length() - 1 < SIZE_X) && (i - buf.length() + 1 >= 0) && (!has)){
			for (int k = 1; k < buf.length() && all; k++) {
				if (buf.charAt(k) != character[i - k][j + k])
					all = false;
			}
			if (all)
				has = true;
			all = true;
		}


		return has;
	} 

}