package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();

        TextField text = new TextField();
        text.setEditable(false);
        root.getChildren().add(text);
        Button[] btn = new Button[9];
        btn[0] = new Button("abc");
        btn[1] = new Button("def");
        btn[2] = new Button("ghi");
        btn[3] = new Button("jkl");
        btn[4] = new Button("mno");
        btn[5] = new Button("pqrs");
        btn[6] = new Button("tuv");
        btn[7] = new Button("wxyz");
        btn[8] = new Button(" ");

        Button backspace = new Button("<-");
        backspace.setLayoutX(150);
        backspace.setOnAction(event -> {
            if (text.getText().length() > 0)
                text.setText(text.getText(0, text.getText().length()-1));
        });
        root.getChildren().add(backspace);
        int m = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                btn[m].setPrefSize(50, 30);
                btn[m].setLayoutX(30 + 50*i);
                btn[m].setLayoutY(70 + 30*j);
                final long[] lastTime = {System.currentTimeMillis()};
                int finalM = m;
                final int[] cur = {0};
                btn[m].setOnAction(event -> {
                    int k = finalM;

                    if (System.currentTimeMillis() - lastTime[0] > 550) {
                        cur[0] = 0;
                        text.setText(text.getText() + btn[k].getText().charAt(0));
                    }
                    else {
                        cur[0] = cur[0] + 1;
                        text.setText(text.getText(0, text.getText().length()-1) + btn[k].getText().charAt(cur[0]%(btn[k].getText().length())));
                    }
                    lastTime[0] = System.currentTimeMillis();
                });
                root.getChildren().add(btn[m]);
                m++;
            }
        }

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();


    }

    public static void main(String[] args) {
        launch(args);
    }
}
