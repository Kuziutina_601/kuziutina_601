public class Task05 {

	public static void main(String[] args) {

		int h = Integer.parseInt(args[0]);
		int y;

		for (int i = 0; i < 2*h; i++){

			for (int j = 0; j < h*2-i; j++){
				System.out.print(" ");
			}

			for (int j = 0; j < (i%h)*2+1; j++){
				System.out.print("*");
			}

			if (i >= h){
				y = 4*h - 2*i - 1;

				for (int j = 0; j < y; j++){
					System.out.print(" ");
				}
				y = (i%h) * 2 +1;

				for (int j = 0; j < y; j++){
					System.out.print("*");
				}

			}

			System.out.println();
		}

	}

}