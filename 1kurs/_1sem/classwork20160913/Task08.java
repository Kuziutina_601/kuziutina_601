public class Task08 {

	public static void main (String[] args) {

		int r = Integer.parseInt(args[0]);
		int summ, summ_left, summ_right, r1 = r/4;

		for (int i = -r; i <= r; i++){

			for (int j = -r; j <= r; j++) {
				summ = i*i + j*j;
				summ_left = (j + r/2)*(j + r/2) + (i + r/2)*(i + r/2);
				summ_right= (j - r/2)*(j - r/2) + (i + r/2)*(i + r/2);

				if ((summ < r*r) & (i < 0) & (summ_right>r1*r1) & (summ_left>r1*r1)){
					System.out.print("*");
				}

				else if ((summ < r*r) & (i>=0) & ((j < -r/2+i) | (j > r/2-i))){
					System.out.print("*");
				}

				else if ((i >= 0) & (j > -r/2+i) & (j < r/2-i)){
					System.out.print("g");
				}

				else{
					System.out.print(".");
				}
			}
			System.out.println();
		}

	}

}