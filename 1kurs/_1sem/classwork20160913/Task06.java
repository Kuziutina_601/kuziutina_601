public class Task06 {

	public static void main(String[] args) {

		int r = Integer.parseInt(args[0]);
		int summ;

		for (int i = -r; i <= r; i++) {

			for (int j = -r; j <= r; j++){

				summ = i*i + j*j;

				if((summ < r*r) & (summ > (r-1)*(r-1))) {
					System.out.print("*");
				}

				else {
					System.out.print(".");
				}
			}
			System.out.println();
		} 

	}

}