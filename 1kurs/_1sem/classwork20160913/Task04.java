public class Task04{

	public static void main(String[] args){

		int x = Integer.parseInt(args[0]);
		
		for (int i = x; i > 0; i--){

			for (int j = 0; j < i - 1; j++){
				System.out.print(" ");
			}

			for (int j = 0; j < x; j++){
				System.out.print("*");
			}

			System.out.println();
		}

	}

}