public class Task02{

	public static void main(String[] args){

		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);

		for (int i = 0; i < y; i++){

			for (int j = 0; j < x; j++){

				System.out.print("*");
			}

			System.out.println();
		}

	}

}