import java.util.Scanner;
public class Task01 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);	

		int n = sc.nextInt();
		char [] a = new char[n];
		boolean game = true;
		int p1 = 0, p2 = 1, x;
		for(int i = 0; i < n; i++){
			a[i] = '|';
		}

		while(game){
			for (int i = 0; i < n; i++){
				System.out.print(a[i]);
			}
			System.out.println();
			
			x = sc.nextInt();
			
			if (x > (n - p2 - p1)){
				System.out.print("P1 GAME OVER");
				break;
			}
			for (int i = p1; i < p1+x; i++){
				a[i] = '.';
			}
			p1 += x;

			for (int i = 0; i < n; i++){
				System.out.print(a[i]);
			}

			System.out.println();

			x = sc.nextInt();
			
			if (x > (n - p2 - p1)){
				System.out.print("P2 GAME OVER");
				break;
			}
			for (int i = n - p2; i > n - p2 - x ; i--){
				a[i] = '.';
			}
			p2 += x;

		}
	}
}