import java.util.Random;
import java.util.Scanner;

public class Snake {

	public static void main(String[] args) {

		Random r = new Random();
		Scanner sc = new Scanner(System.in);
		
		int hight  = sc.nextInt();
		int weight = sc.nextInt();
		int [][] a = new int[hight][weight];
		boolean game = true, status = true;
		int run = 3, lastl = 0, lasts = 2, pointl = 0, points = 0;
		char nap;


		for (int i = 0; i < hight; i++){
			for (int j = 0; j < weight; j++){
				a[i][j] = 0;
			}
		}
		a[0][0] = 3;
		a[0][1] = 2;
		a[0][2] = 1;
		for(int i = 0; i < hight; i++){
			for (int j = 0; j < weight; j++){
				if(a[i][j] == 0){
					System.out.print("'");
				}
				else{
					System.out.print("*");
				}
			}
			System.out.println();
		}

		while(game){
			if(status){
				do{
					pointl = r.nextInt(hight);
					points = r.nextInt(weight);
				}while(a[pointl][points] != 0);
				a[pointl][points] = hight*weight+1;
				status = false;
			}
			nap = sc.next().charAt(0);
			if (nap == 'w')
				lastl--;
			if (nap == 'a')
				lasts--;
			if (nap == 's')
				lastl++;
			if (nap == 'd')
				lasts++;
			if ((lastl < 0) || (lastl >= hight) || (lasts < 0) || (lasts >= weight) || ((a[lastl][lasts] != 0) & (a[lastl][lasts] != hight*weight+1))){
				System.out.println("GAME OVER");
				break;
			}
			if((lastl == pointl) & (lasts == points)){
				run++;
				status = true;
			}
			for(int i = 0; i < hight; i++){
				for (int j = 0; j < weight; j++){
					if ((a[i][j] != 0) & (a[i][j] != hight*weight+1) & (a[i][j] + 1 <= run)){
						a[i][j]++;
					}
					else if (a[i][j] == run)
						a[i][j] = 0;
				}
			}
			a[lastl][lasts] = 1;
			for(int i = 0; i < hight; i++){
				for (int j = 0; j < weight; j++) {
					if (a[i][j] == 0)
						System.out.print("'");
					else
						System.out.print("*");
				}
				System.out.println();
			}
			if(run == hight*weight){
				game = false;
				System.out.println("*****IT'S IMPOSSIBLE!! YOU WIN!!*****");
			}
		}
	}
}