import java.util.Scanner;
import java.util.Random;

public class SaperBeautiful {
	static int [][] a;
	static int weight, hight, sch;
	public static void open(int i, int j){
		if ((a[i][j] > 10) && (a[i][j] < 19))
			a[i][j] = a[i][j] - 10;
		
		if (a[i][j] == 10){
			a[i][j] = a[i][j] - 10;
			if (j - 1 >= 0){
				open(i, --j);
				j++;
			}
					
			if ((j - 1 >= 0) & (i - 1 >= 0)){
				open(--i, --j);
				i++;
				j++;
			}
			
			if (i - 1 >= 0){
				open(--i, j);
				i++;
			}

			if ((j + 1 < weight) & (i - 1 >= 0)){
				open(--i, ++j);
				i++;
				j--;
			}

			if (j + 1 < weight){
				open(i, ++j);
				j--;
			}

			if ((j + 1 < weight) & (i + 1 < hight)){
				open(++i, ++j);
				i--;
				j--;
			}

			if (i + 1 < hight){
				open(++i, j);
				i--;
			}

			if ((j - 1 >= 0) & (i + 1 < hight)){
				open(++i, --j);
				j++;
				i--;
			}

		}
	}                                        //open cell counts code,recursion if 0

	public static void level(int level){
		Random  r  = new Random();
		int i , j, kolvo = 0;

		do{
			i = r.nextInt(hight);
			j = r.nextInt(weight);
			if (a[i][j] != 19){
				kolvo++;
				a[i][j] = 19;
			}
		}while(kolvo < level);
	}                                        //defines the coordinates of bombs

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int kolvo = 0, k, line, stol, around = 0, sch = 0;
		int game = 0;

		System.out.println("9 - this is a bomb, If asked to enter the coordinates again, \nthen the previous incorrect");

		System.out.println("Enter the field size: hight and weight");
		hight  = sc.nextInt();
		weight = sc.nextInt();

		a = new int[hight][weight];

		for (int i = 0; i < hight; i++){
			for (int j = 0; j < weight; j++){
				System.out.print("*");
				a[i][j] = 10;
			}
			System.out.println();
		}									//fills the array and displays the playing field
		
		System.out.println("Enter kolovo bomb 1 - " + weight*hight);
		int level = sc.nextInt();
		level(level);
		kolvo = level;

		for (int i = 0; i < hight; i++){
			for (int j = 0; j < weight; j++){
				if (a[i][j]!=19){
					if (j - 1 >= 0){
						if (a[i][j-1] == 19)
							around++;
					}
					if ((j - 1 >= 0) & (i - 1 >= 0)){
						if (a[i-1][j-1] == 19)
							around++;
					}
					if (i - 1 >= 0){
						if (a[i-1][j] == 19)
							around++;
					}
					if ((j + 1 < weight) & (i - 1 >= 0)){
						if (a[i-1][j+1] == 19)
							around++;
					}
					if (j + 1 < weight){
						if (a[i][j+1] == 19)
							around++;
					}
					if ((j + 1 < weight) & (i + 1 < hight)){
						if (a[i+1][j+1] == 19)
							around++;
					}
					if (i + 1 < hight){
						if (a[i+1][j] == 19)
							around++;
					}
					if ((j - 1 >= 0) & (i + 1 < hight)){
						if (a[i+1][j-1] == 19)
							around++;
					}

					a[i][j] += around;
				}
				around = 0; 
			}
		}									//It counts the number of bombs around

		while(game == 0){                   //GAME
			sch = 0;
			do{
				System.out.print("Enter coord : line and stol " + sch);
				line = sc.nextInt();
				line--;
				stol = sc.nextInt();
				stol--;
			}while(a[line][stol] < 9);
			System.out.println("bomb left --- " + kolvo);

			open(line, stol);

			if (a[line][stol] == 19){
				a[line][stol] = 9;
				game = 1;
			}

			for (int i = 0; i < hight; i++){
				for (int j = 0; j < weight; j++) {
					if(a[i][j] > 9){
						System.out.print("*");					
					}
					else{
						System.out.print(a[i][j]);
						sch++;
					}
				}
				System.out.println();
			}

			if (sch >= (hight*weight - kolvo))
				game = 2;
		}

		if(game == 2){
			System.out.print("YOU WIN");
		}
		else{
			System.out.print("BOOOOOM!!!!  GAME OVER");
		}
	}
}