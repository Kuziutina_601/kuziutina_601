public class Task03 {

	public static void main(String[] args) {
		
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int [] a = new int[n];
		int b;

		for (int i = 0; i < n; i++){
			int m = Integer.parseInt(args[i+2]);
			a[i] = m;
		}

		for (int i = 0; i < k / 2; i++) {
			b = a[i];
			a[i] = a[k - i - 1];
			a[k - i - 1] = b;
		}

		for (int i = 0; i < (n - k) /2; i++){
			b = a[k + i];
			a[k + i] = a[n - i - 1];
			a[n - i - 1] = b;
		}
		
		for (int i = 0; i < n / 2; i++){
			b = a[i];
			a[i] = a[n - i - 1];
			a[n - i -1] = b;
		}

		for(int i = 0; i< n; i++){
			System.out.print(a[i] + "  ");
		}
	}

}