

public class Task01 {

	public static void main(String[] args) {
		
		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int Max;

		for (int i = 0; i < n; i++) {
			int m = Integer.parseInt(args[i+1]);
			a[i] = m;
			System.out.println(a[i]);
		}

		Max = a[0];

		for (int i = 1; i < n; i++) {
			if (a[i] > Max){
				Max = a[i];
			}
		}

		System.out.print(Max);


	}

}