public class Task02 {

	public static void main(String[] args) {

		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int Max;

		for (int i = 0; i < n; i++){
			int m = Integer.parseInt(args[i+1]);
			a[i] = m;
		}

		Max = a[0];
		for (int x : a) {
			if (x > Max){
				Max = x;
			}
		}

		System.out.print(Max);
		
	}

}