public class Task02_5 {

	public static void main(String[] args) {
		
		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int h;

		for (int i = 0; i < n; i++){
			int k = Integer.parseInt(args[1+i]);
			a[i] = k;
		}

		for (int i = 0; i < n/2; i++){
			h = a[i];
			a[i] = a[n - i - 1];
			a[n - i - 1] = h;
		}

		for (int i = 0; i < n; i++){
			System.out.print(a[i] + "  ");
		}
	}

}