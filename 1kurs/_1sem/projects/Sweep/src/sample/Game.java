package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileNotFoundException;

/**
 * Created by пользователь on 12.12.2016.
 */
public class Game extends Application {
    public static Scene scene;
    public static Group root;
    public static FieldView fv;
    public static VBox front;
    public static boolean game;
    public MenuBar menuBar;
    public StackPane information;
    public static Button next;
    private AnimationTimer at;
    public static long t;

    @Override
    public void start(Stage primaryStage) throws Exception{
        game = true;
        root = new Group();
        scene = new Scene(root, 450, 505);

        next = new Button();
        next.setAlignment(Pos.BASELINE_CENTER);
        next.setOnAction(event -> {
            this.game = false;
            at.stop();
            newGame();
        });
        front = new VBox();
        menuBar = new MenuBar();
        Menu gameMenu = new Menu("Game");
        MenuItem newGame = new MenuItem("new game");
        newGame.setOnAction(event -> {
            game = false;
            at.stop();
            newGame();
        });
        MenuItem people= new MenuItem("Рекорды");
        people.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("TheBest");
            stage.initOwner(primaryStage);
            try {
                new TheBest(stage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        gameMenu.getItems().add(newGame);
        gameMenu.getItems().add(people);
        menuBar.getMenus().add(gameMenu);
        newGame();
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void newGame() {
        t = 0;
        System.out.println("NNEEEWWWWW");
        final CreateGame[] cg = new CreateGame[1];
        game = true;
        root.getChildren().remove(front);
        System.out.println("refr");
        front = new VBox();
        Image im = new Image("wait2.png");
        ImageView result = new ImageView(im);
        result.setFitHeight(25);
        result.setFitWidth(25);
        next.setAlignment(Pos.BASELINE_CENTER);
        next.setGraphic(result);
        information = new StackPane();


        Label time = new Label();
        at = new AnimationTimer() {
            long lasttime = System.currentTimeMillis();
            @Override
            public void handle(long now) {
                //System.out.println(game);
                if (System.currentTimeMillis() - lasttime > 1000) {
                    information.getChildren().remove(time);
                    System.out.println(t);
                    t++;
                    time.setText(t + "");
                    time.setStyle("-fx-font-size: 20px");
                    information.getChildren().add(time);
                    information.setAlignment(time, Pos.BOTTOM_LEFT);
                    lasttime = System.currentTimeMillis();
                }
                if (!game){
                    this.stop();
                }
            }
        };
        at.start();

        information.getChildren().add(next);
        front.getChildren().add(menuBar);
        front.getChildren().add(information);
        cg[0] = new CreateGame(9, 9, 9);
        fv = new FieldView(cg[0].getField());
        front.getChildren().add(fv.getPane());
        root.getChildren().add(front);
        System.out.println("false");

    }

    public static void update(int h, int w) {
        //root.getChildren().remove(front);
        front.getChildren().remove(fv.getPane());
        FieldView.updateFieldView(h, w);
        front.getChildren().add(fv.getPane());
    }

    public static void main(String[] args) {
        launch(args);
    }
}


