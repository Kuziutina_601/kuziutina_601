package sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by пользователь on 12.12.2016.
 */
public class Field {
    private static Cell[][] cells;
    private static int high;
    private static int width;
    private static int countMine;
    private static int closes;
    private static ArrayList<Cell> mines;

    public Field(int high, int width) {
        this.high = high;
        this.width = width;
        closes = high*width;

        cells = new Cell[high][width];
    }

    public void createField(int countMine){
        this.countMine = countMine;
        Random r = new Random();
        int k,m;
        mines = new ArrayList<>();

        for (int i = 0; i < high; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j] = new Cell(10, i, j);
            }
        }

        for (int i = 0; i < countMine; i++){
            do{
                k = r.nextInt(width);
                m = r.nextInt(high);
            }while(cells[m][k].getValue() != 10);

            mines.add(cells[m][k]);
            cells[m][k].setValue(19);

            if ((k - 1 >= 0) && (m - 1 >= 0))
                cells[m-1][k-1].setValue(cells[m-1][k-1].getValue() + 1);
            if (m - 1 >= 0)
                cells[m-1][k].setValue(cells[m-1][k].getValue() + 1);
            if ((m - 1 >= 0) && (k + 1 < width))
                cells[m-1][k+1].setValue(cells[m-1][k+1].getValue() + 1);
            if (k + 1 < width)
                cells[m][k+1].setValue(cells[m][k+1].getValue() + 1);
            if ((k + 1 < width) && (m + 1 < high))
                cells[m+1][k+1].setValue(cells[m+1][k+1].getValue() + 1);
            if (m + 1 < high)
                cells[m+1][k].setValue(cells[m+1][k].getValue() + 1);
            if ((m + 1 < high) && (k - 1 >= 0))
                cells[m+1][k-1].setValue(cells[m+1][k-1].getValue() + 1);
            if (k - 1 >= 0)
                cells[m][k-1].setValue(cells[m][k-1].getValue() + 1);
        }

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                System.out.print("  " + cells[i][j].getValue());
            }
            System.out.println();
        }
    }

    public static void openTwo(int i, int j) throws IOException {
        if (cells[i][j].getValue() == 19 || cells[i][j].getValue() == 39){
            Game.game = false;
            openMine();
            Image im = new Image("fall.png");
            ImageView iv = new ImageView(im);
            iv.setFitHeight(25);
            iv.setFitWidth(25);
            Game.next.setGraphic(iv);
            System.out.println(i + "  "+ j + " GAME OVER");
            return;
        }

        ArrayList<Cell> list = new ArrayList<>();
        list.add(cells[i][j]);
        while (list.size() != 0) {
            System.out.println(list.size());
            if (list.get(0).getValue() < 10 || (list.get(0).getValue() >= 20 && list.get(0).getValue() < 30)){
                list.remove(0);
                System.out.println("no");
                continue;
            };

            if (list.get(0).getValue() == 10 || list.get(0).getValue() == 30) {
                int m = list.get(0).getX(), n = list.get(0).getY();
                int i1 = list.get(0).getY(), j1 = list.get(0).getX();
                if (j1 - 1 >= 0) {
                    m = j1 - 1;
                    System.out.println("i = " + n + "  j " + m);
                    list.add(cells[i1][m]);
                }

                if ((j1 - 1 >= 0) && (i1 - 1 >= 0)) {
                    m = j1 - 1;
                    n = i1 - 1;
                    list.add(cells[n][m]);
                }

                if (i1 - 1 >= 0){
                    n = i1 - 1;
                    list.add(cells[n][j1]);
                }

                if ((j1 + 1 < width) && (i1 - 1 >= 0)) {
                    m = j1 + 1;
                    n = i1 - 1;
                    list.add(cells[n][m]);
                }

                if (j1 + 1 < width) {
                    m = j1 + 1;
                    list.add(cells[i1][m]);
                    //System.out.println("rigth added i = " + n + " j = " + m);
                }

                if ((j1 + 1 < width) && (i1 + 1 < high)) {
                    n = i1 + 1;
                    m = j1 + 1;
                    list.add(cells[n][m]);
                }

                if (i1 + 1 < high) {
                    n = i1 + 1;
                    list.add(cells[n][j1]);
                }

                if ((j1 - 1 >= 0) && (i1 + 1 < high)) {
                    n = i1 + 1;
                    m = j1 - 1;
                    list.add(cells[n][m]);
                }
            }
            while (list.get(0).getValue() >= 10) {
                list.get(0).setValue(list.get(0).getValue() - 10);
            }
            closes--;
            list.get(0).getRec().setFill(Color.LIGHTGREY);
            list.get(0).getChildren().remove(list.get(0).getIv());
            Text text = new Text();
            if (list.get(0).getValue() == 0)
                text.setText("");
            else {
                text.setText(list.get(0).getValue() + "");
                text.setStyle("-fx-font-size: 30px");
            }
            list.get(0).getChildren().add(text);
            Game.update(list.get(0).getY(), list.get(0).getX());
            list.remove(0);
            if (closes == countMine) {
                Game.game = false;
                Image im = new Image("win2.png");
                ImageView iv = new ImageView(im);
                iv.setFitHeight(25);
                iv.setFitWidth(25);
                Game.next.setGraphic(iv);
                Stage searchW = new Stage();
                searchW.setTitle("SEARCH");
                //searchW.initOwner(Game.);
                new Result(searchW, Game.t);
            }
        }
    }

    public static void openMine() {

        for (int i = 0; i < mines.size(); i++) {
            Image image = new Image("Minesweepers.png");
            ImageView iv = new ImageView(image);
            iv.setFitHeight(cells[0][0].getHeight());
            iv.setFitWidth(cells[0][0].getWidth());
            mines.get(i).getChildren().remove(mines.get(i).getIv());
            mines.get(i).getRec().setFill(Color.LIGHTGREY);
            mines.get(i).getChildren().add(iv);
            Game.update(mines.get(i).getY(), mines.get(i).getX());
        }
    }

    public static void mark(int i, int j) {


        Image flag = new Image("flag.png");
        //ImageView ivflag = new ImageView(flag);
        //ivflag.setFitHeight(cells[i][j].getRec().getHeight());
        //ivflag.setFitWidth(cells[i][j].getRec().getWidth());

        Image quest = new Image("ques.png");
        //ImageView ivquest = new ImageView(quest);
        //ivquest.setFitHeight(cells[i][j].getRec().getHeight());
        //ivquest.setFitWidth(cells[i][j].getRec().getWidth());*/

        System.out.println("FFFFFFFFFFFFFFFFFFFFFFFF  " + cells[i][j].getValue());

        if (cells[i][j].getValue() >= 20){
            if (cells[i][j].getValue() >= 30) {
                cells[i][j].getChildren().remove(cells[i][j].getIv());
                cells[i][j].setValue(cells[i][j].getValue() - 20);
            }
            else{
                cells[i][j].getChildren().remove(cells[i][j].getIv());
                System.out.println("REMOVE");
                cells[i][j].setIv(new ImageView(quest));
                cells[i][j].getIv().setFitHeight(cells[i][j].getRec().getHeight());
                cells[i][j].getIv().setFitWidth(cells[i][j].getRec().getWidth());
                cells[i][j].getChildren().add(cells[i][j].getIv());
                System.out.println("ADD");
                cells[i][j].setValue(cells[i][j].getValue() + 10);
            }
        }
        else if (cells[i][j].getValue() >= 10) {
            cells[i][j].setIv(new ImageView(flag));
            cells[i][j].getChildren().add(cells[i][j].getIv());
            cells[i][j].getIv().setFitHeight(cells[i][j].getRec().getHeight());
            cells[i][j].getIv().setFitWidth(cells[i][j].getRec().getWidth());
            cells[i][j].setValue(cells[i][j].getValue() + 10);
        }
    }

    public Cell[][] getCells() {
        return cells;
    }

    public int getHigh() {
        return high;
    }

    public int getWidth() {
        return width;
    }
}
