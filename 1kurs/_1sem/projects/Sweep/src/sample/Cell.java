package sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.IOException;

/**
 * Created by пользователь on 12.12.2016.
 */
public class Cell extends StackPane {
    private int value;
    private int x;
    private int y;
    private Rectangle rec;
    private ImageView iv;

    public Cell(int value, int h, int w) {
        this.value = value;
        rec = new Rectangle(Game.scene.getWidth()/9 - 2, (Game.scene.getHeight()-50)/9 - 2);
        rec.setFill(Color.GREEN);
        this.x = w;
        this.y = h;
        getChildren().add(rec);
        //setTranslateX(Game.scene.getWidth()/9 * w);
        //setTranslateY(Game.scene.getHeight()/9 * h);

        System.out.println(getTranslateX() + "      " + getTranslateY() + " " + w + " " + h + " " + Game.scene.getHeight());
        setOnMousePressed(event -> {
            System.out.println("in clicked");
            if (event.getButton() == MouseButton.PRIMARY) {
                if (Game.game && ((value >= 10 && value < 20) || (value >= 30))) {
                    try {
                        Field.openTwo(y, x);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (event.getButton() == MouseButton.SECONDARY) {
                Field.mark(y, x);
            }
        });


    }

    public Rectangle getRec() {
        return rec;
    }

    public int getValue() {
        return value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ImageView getIv() {
        return iv;
    }

    public void setIv(ImageView iv) {
        this.iv = iv;
    }
}
