package sample;

/**
 * Created by пользователь on 12.12.2016.
 */
public class CreateGame {
    private int high;
    private int width;
    private int countMine;
    private Field field;

    public CreateGame(int high, int width, int countMine) {
        this.high = high;
        this.width = width;
        this.countMine = countMine;
        System.out.println("CREATE GAME");

        field = new Field(high, width);
        field.createField(countMine);
    }

    public Field getField() {
        return field;
    }
}
