package sample;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparing;

/**
 * Created by пользователь on 25.12.2016.
 */
public class Result {
    private static long hightResult = Long.MAX_VALUE;
    private static ArrayList<Pair<String, Long>> people;
    static {
        try {
            Scanner sc = new Scanner(new File("records.txt"));
            people = new ArrayList<>();
            while (sc.hasNextLine()) {
                String[] bufLine = sc.nextLine().split(",#,");
                people.add(new Pair<String, Long>(bufLine[0], Long.parseLong(bufLine[1])));
                long buf = Long.parseLong(bufLine[1]);
                if (buf < hightResult)
                    hightResult = buf;

            }
            final Comparator<Pair<String, Long>> c = comparing(Pair::getValue);
            Collections.sort(people, c);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Result(Stage stage, long result) throws IOException {
        VBox root = new VBox();
        Label resText = new Label();
        resText.setPadding(new Insets(0,30,0,0));

        if (result < hightResult) {
            resText.setText("Вы установили новый рекорд: " + result);
        }
        else {
            resText.setText("Ваш результат: " + result);
        }
        Label sol = new Label("Введите ваше имя для сохранения результата");
        TextField name = new TextField();
        Button ok = new Button("Ok");
        ok.setOnAction(event -> {
            if (!name.getText().isEmpty()) {
                int k = 0;
                boolean take = false;

                try {
                    PrintWriter pw = new PrintWriter(new File("records.txt"));
                    while (k < 5 && k < people.size()) {
                        if (result < people.get(k).getValue() && !take) {
                            people.add(k, new Pair<String, Long>(name.getText(), result));
                            pw.println(name.getText() + ",#," + result);
                            take = true;
                        } else {
                            pw.println(people.get(k).getKey() + ",#," + people.get(k).getValue());
                        }
                        k++;
                    }

                    pw.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            stage.close();
        });
        StackPane okPane = new StackPane();
        okPane.getChildren().add(ok);
        okPane.setAlignment(ok, Pos.BOTTOM_RIGHT);

        root.getChildren().add(resText);
        root.getChildren().add(sol);
        root.getChildren().add(name);
        root.getChildren().add(okPane);
        Scene scene = new Scene(root, 300, 300);
        stage.setScene(scene);
        stage.show();
    }
}
