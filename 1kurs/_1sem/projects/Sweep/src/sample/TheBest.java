package sample;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by пользователь on 25.12.2016.
 */
public class TheBest {
    public TheBest(Stage stage) throws FileNotFoundException {
        Label theBest = new Label("Лучшие:");
        theBest.setStyle("-fx-font-size: 30px");
        VBox vBox = new VBox();
        StackPane people;
        Label l1, l2;
        Scanner sc = new Scanner(new File("records.txt"));
        while (sc.hasNextLine()) {
            people = new StackPane();
            String[] buf = sc.nextLine().split(",#,");
            l1 = new Label(buf[0] + " :");
            people.getChildren().add(l1);
            people.setAlignment(l1, Pos.BASELINE_LEFT);
            l2 = new Label(buf[1]);
            people.getChildren().add(l2);
            l2.setPadding(new Insets(0, 50, 0, 0));
            people.setAlignment(l2, Pos.BASELINE_RIGHT);
            vBox.getChildren().add(people);
            System.out.println(buf[0]);

        }

        vBox.setPadding(new Insets(10, 0, 70, 5));
        VBox root = new VBox();
        root.getChildren().add(theBest);
        root.getChildren().add(vBox);

        Scene scene = new Scene(root, 300, 150);
        stage.setScene(scene);
        stage.show();

    }
}
