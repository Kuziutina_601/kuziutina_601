package sample;

import javafx.scene.layout.Pane;

/**
 * Created by пользователь on 12.12.2016.
 */
public class FieldView {
    private static Field field;
    private static Pane pane;
    static {
        pane = new Pane();
    }

    public FieldView(Field field) {
        this.field = field;
        System.out.println("CR FV");
        System.out.println(pane);

        for (int i = 0; i < field.getCells().length; i++) {
            for (int j = 0; j <  field.getCells()[i].length; j++) {
                field.getCells()[i][j].setLayoutX(Game.scene.getWidth()/9 * j);
                field.getCells()[i][j].setLayoutY((Game.scene.getHeight()-50)/9 * i);
                pane.getChildren().add(field.getCells()[i][j]);
            }
        }
    }

    public static void updateFieldView(int h, int w) {
        pane.getChildren().remove(field.getCells()[h][w]);
        pane.getChildren().add(field.getCells()[h][w]);
    }

    public Pane getPane() { return pane;}
}
