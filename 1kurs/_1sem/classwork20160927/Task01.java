public class Task01 {

	public static void main(String[] args) {
		
		boolean k = false;
	
		for (int i = 0; i < args.length && !k; i++){
			int m = Integer.parseInt(args[i]);
			if (m > 0){
				i = args.length;
				k = true;
			}
		}

		if (k){
			System.out.print("NO");
		}
		else {
			System.out.print("YES");
		}
	}

}