public class Task01A {

	public static void main(String[] args) {

		int s = 0;

		for (int i = 0; i < args.length && s < 3; i++){
			int m = Integer.parseInt(args[i]);
			if (m > 0){
				s++;
			}
		}
		if (s == 2){
			System.out.print("YES");
		}
		else{
			System.out.print("NO");
		}


	}

}