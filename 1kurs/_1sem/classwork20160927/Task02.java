public class Task02 {

	public static void main(String[] args) {
		
		boolean k = false;

		for (int i = 0; i < args.length && !k; i++){
			int m = Integer.parseInt(args[i]);
			if ((m % 5 == 0) || (m % 3 == 0)){
				k = true;
			}
		}

		if(k){
			System.out.print("YES");
		}
		else{
			System.out.print("NO");
		}
	}

}