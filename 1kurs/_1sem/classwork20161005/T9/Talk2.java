import java.util.*;
import java.io.*;

public class Talk2 {
	static String k;

	public static boolean serch(String s) throws FileNotFoundException{
		Scanner ser = new Scanner(new File("word.txt"));
		String part = ser.nextLine();

		for (int i = 0; i < s.length(); i++){
		
			while(((s.length() > part.length()) || (s.charAt(i) > part.charAt(i))) && (ser.hasNextLine())) {
				part = ser.nextLine();
			}	
		}

		if(s.equals(part))
			return(true);
		else
			return(false);
	}

	public static boolean shift(int number, String s) throws FileNotFoundException{
		Scanner comb = new Scanner(new File("comb.txt"));
		String neiber = comb.nextLine();

		while(neiber.charAt(0) != s.charAt(number)){
			neiber = comb.nextLine();
		}

		for (int i = 1; i < neiber.length(); i++){
			char[] chars = s.toCharArray();
			chars[number] = neiber.charAt(i);
			String jack = new String(chars);
			k = jack;
			if (serch(k))
				return(true);
		}
		return(false);
	}

	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(new File("input.txt"));
		PrintWriter pw = new PrintWriter(new File("Result.txt"));
	
		String sentence = sc.nextLine();
		sentence = sentence.toLowerCase();

		String [] word = sentence.split(" ");
		for (int i = 0; i < word.length; i++){
			
			if(serch(word[i]))
				pw.print(word[i] + " ");
			
			else{
				for (int j =0; j < word[i].length(); j++){
					
					if(shift(j, word[i])){
						pw.print(k + " ");
						break;
					}
				}
			}
		}
		pw.close();
	}
}