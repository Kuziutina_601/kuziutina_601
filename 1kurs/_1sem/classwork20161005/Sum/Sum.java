import java.io.*;
import java.util.*;


public class Sum {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("input.txt"));
		PrintWriter pw = new PrintWriter(new File("output.txt"));
		int s = 0;

		do{
			int x = sc.nextInt();
			s += x;
		}
		while(sc.hasNextInt());
		pw.print(s);
		pw.close();

	}

}