import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;

public class Task04{

	public static void main(String[] args) {
		Random r = new Random();

		int count = 0, summ = 0, buf;

		Pattern p = Pattern.compile("[0-9]*(0|2|4|6|8)");
        Matcher m;
    	while (count < 10) {
    		buf = r.nextInt();
			m = p.matcher(buf + "");
			if (m.matches()){
				count++;
				System.out.println(buf);
			}
			summ++;
		}
		System.out.println(summ);

	}
}