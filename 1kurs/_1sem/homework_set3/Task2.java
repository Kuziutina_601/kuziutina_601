import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2{

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		Pattern p = Pattern.compile("((0|(\\+|-)?[1-9][0-9]*)((,|\\.)[0-9]*([1-9]|\\([0-9]*[1-9]\\))([0-9]*[1-9])?)?)|(\\+|-)?0(,|\\.)[0-9]*([1-9]|\\([0-9]*[1-9]\\))([0-9]*[1-9])?");
        Matcher m = p.matcher(sc.next());
        System.out.println(m.matches());
    
	}
}