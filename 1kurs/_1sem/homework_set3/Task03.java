import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task03{

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		String[] s = new String[n];

		for (int i = 0; i < n; i++) {
			s[i] = sc.next();
		}

		Pattern p = Pattern.compile("(1+)|(0+)|((01)+0?)|((10)+1?)");
		Matcher m;
		for (int i = 0; i < n; i++) {
			m = p.matcher(s[i]);
			if (m.matches())
				System.out.println(i + "  ");
		}    
	}
}