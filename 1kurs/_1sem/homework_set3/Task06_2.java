package sample;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by пользователь on 21.12.2016.
 */
public class Task06_2 {

    public static void main(String[] args) {
        Random r = new Random();

        Pattern pattern = Pattern.compile("(1|3|5|7|9)");
        Pattern pattern1 = Pattern.compile("(2|4|6|8|0){4}");
        Pattern pattern2 = Pattern.compile("(2|4|6|8|0){6}");
        Matcher matcher, matcher1, matcher2;
        int count = 0, m = 0, buf;
        do {
            count++;
            buf = r.nextInt(Integer.MAX_VALUE-1) + 1;
            matcher = pattern.matcher(buf + "");
            matcher1 = pattern1.matcher(buf + "");
            matcher2 = pattern2.matcher(buf + "");
            if (!matcher.find() && matcher1.find() && !matcher2.find()){
                m++;
                System.out.println(buf);
            }
        }
        while (m < 10);
        System.out.println(count);
    }
}
