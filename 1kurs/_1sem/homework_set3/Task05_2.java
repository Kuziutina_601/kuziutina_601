package sample;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by пользователь on 21.12.2016.
 */
public class Task05_2 {
    public static void main(String[] args) {
        Random r = new Random();

        Pattern pattern = Pattern.compile("(0|2|4|6|8)(0|2|4|6|8)(0|2|4|6|8)");
        Matcher matcher;
        int count = 0, m = 0, buf;
        do {
            count++;
            buf = r.nextInt(Integer.MAX_VALUE-1) + 1;
            matcher = pattern.matcher(buf + "");
            if (!matcher.find()){
                m++;
                System.out.println(buf);
            }
        }
        while (m < 10);
        System.out.println(count);
    }

}
