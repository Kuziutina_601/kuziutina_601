public class Task15{

	public static void main(String[] args){

		int x = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int m = 1, l = 0;

		while (x > 0){
			if(x % 10 != k){
				l = l + (x%10) * m;
				m *= 10;
			}
			x = x / 10;
		}
		System.out.println(l);

	}

}