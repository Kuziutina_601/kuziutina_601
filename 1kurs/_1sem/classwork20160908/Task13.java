public class Task13{

	public static void main(String[] args){

		int x = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int m = 0;

		while (x > 0){
			if (x%10 == k){
				m++;
			}
			x = x / 10;
		}
		System.out.println(m);

	}

}