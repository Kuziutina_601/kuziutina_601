public class Task08{

	public static void main(String[] args){

		int N = Integer.parseInt(args[0]);
		
		double result = 0.0;

		for(int i = 2; i <= N; i = i+2){
			result += ((i-1.0)*(i-1))/(i*i);
		}
		System.out.println(result);

	}

}