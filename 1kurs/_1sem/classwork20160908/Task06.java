public class Task06{

	public static void main(String[] args){

		double a = Double.parseDouble(args[0]);
		double b = Double.parseDouble(args[1]);
		double c = Double.parseDouble(args[2]);
		String s = "no solution";
		double d = 0;

		double x1, x2;
		if (a == 0){
			if (b == 0){
				if (c == 0)
					System.out.println("infinite number of solutions");
				else
					System.out.println(s);
			}
			else{
				x1 = -c / b;
				System.out.println(x1);
			}
		}
		else {
			d = b * b - 4 * a * c;
			if (d > 0) {
				x1 = ((-1) * b + Math.sqrt(d)) / (2 * a);
				x2 = ((-1) * b - Math.sqrt(d)) / (2 * a);
				System.out.println(x1 + "   " + x2);
			}
			else if (d == 0){
				x1 = ((-1) * b + Math.sqrt(d)) / (2 * a);
				System.out.println(x1);
			}
			else
				System.out.println(s);
		}

	}

}