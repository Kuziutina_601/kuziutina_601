

public class Money {


	long rub;
	byte kop;

	public Money(long rub, byte kop) {
		this.rub = rub;
		this.kop = kop;
	}

	public long getRub() { return rub; }

	public byte getKop() { return kop; } 

	public String toString() {
		String result = "";
		if (kop > 10)
			result = rub + "," + kop;
		else
			result = rub + ",0" + kop;

		return result;
	}

	public Money add(Money m3) {
		byte kopres = (byte) (kop + m3.getKop());
		long rubres = rub + m3.getRub() + (kopres/100);
		kopres %= 100;
		return new Money(rubres, kopres);
	}

	public Money sub(Money m3) {
		byte kopres = (byte) (kop - m3.getKop());
		long rubres = rub - m3.getRub();
		if (kopres < 0) {
			rubres--;
			kopres += 100;
		}

		return new Money(rubres, kopres);
	}

	public Money div(int d) {
		double result = rub*100 + kop;
		result /= d;
		long rubres =(long) result/100;
		byte kopres = (byte) ((result - rubres*100));

		return new Money(rubres, kopres);
	}

	public double conv() {
		double result = rub * 100 + kop;
		return result;
	}

	public Money div(Money m3) {
		double c1 = this.conv();
		double c2 = m3.conv();
		c1 /= c2;
		long k = (long) c1 / 100;
		byte m = (byte)(c1 - k * 100);

		return new Money(k, m);
	}

	public Money multi (int d) {
		int kopres = (int) (kop * d);
		long rubres = rub * d + (kopres/100);
		kopres %= 100;
		byte res = (byte)kopres;
		return new Money(rubres, res);
	
	}

	public boolean equals(Money m3) {
		boolean res = false;
		if((rub == m3.getRub()) && (kop == m3.getKop()))
			res = true;
		return res;
	}



	public static void main(String[] args) {
		long r1 = 13, r2 = 0;
		byte k1 = 0, k2 = 56;
		Money f1 = new Money(r1, k1);
		Money f2 = new Money(r2, k2);
		Money f3;

		f3 = f2.add(f1);
		System.out.println(f3);
		f3 = f2.sub(f1);
		System.out.println(f3);
		f3 = f1.sub(f2);
		System.out.println(f3);	
		f3 = f3.add(f2);
		System.out.println(f3);
		f3 = f1.div(2);
		System.out.println(f3);
	}
}