public class Helper3 {
    public static void main(String[] args) {
        int a = 100;
        int b = 200;
        BinaryFunction oper = (int x, int y) -> {return x + y;};

        System.out.println(oper.f(a, b));
    }
}
