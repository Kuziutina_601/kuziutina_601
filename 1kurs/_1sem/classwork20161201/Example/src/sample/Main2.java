package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main2 extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        final int LENGTH = 300;
        final int HIGHT = 300;
        Group root = new Group();

        Circle circle = new Circle(30, 30, 10);
        circle.setStyle("-fx-fill: red");
        root.getChildren().add(circle);
        root.setOnKeyPressed(event -> {
            switch (event.getCode()){
                case LEFT: {
                    if((circle.getCenterX() - circle.getRadius() - 10) >= 0)
                        circle.setCenterX(circle.getCenterX() - 10);
                    break;
                }
                case RIGHT: {
                    if((circle.getCenterX() + circle.getRadius() + 10) <= LENGTH)
                        circle.setCenterX(circle.getCenterX() + 10);
                    break;
                }
                case DOWN: {
                    if((circle.getCenterY() + circle.getRadius() + 10) <= HIGHT)
                        circle.setCenterY(circle.getCenterY() + 10);
                    break;
                }
                case UP: {
                    if((circle.getCenterY() - circle.getRadius() - 10) >= 0)
                        circle.setCenterY(circle.getCenterY() - 10);
                    break;
                }
            }

        });

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 300));
        primaryStage.show();
        primaryStage.getScene().getRoot().requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
