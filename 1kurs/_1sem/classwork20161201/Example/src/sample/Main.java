package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {

    public static double a, b;
    public static int doing;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        TextField tf = new TextField();
        TextField tf2 = new TextField();
        Button btn = new Button("OK");
        Label l1 = new Label("Answer");
        ChoiceBox ch = new ChoiceBox();
        ch.setItems(FXCollections.observableArrayList("+", "*", "-", "/"));

        l1.setLayoutX(170);
        tf2.setLayoutY(30);
        btn.setLayoutY(100);
        ch.setLayoutY(60);


        ch.setOnAction(event -> {
            doing = ch.getSelectionModel().getSelectedIndex();
        });

        btn.setOnAction(event -> {
            a = Double.parseDouble(tf.getText());
            b = Double.parseDouble(tf2.getText());
            l1.setText("");
            switch (doing){
                case 0: l1.setText(a + b + "");break;
                case 1: l1.setText(a*b + "");break;
                case 2: l1.setText((a - b) + "");break;
                case 3: l1.setText((a/b) + "");break;
            }
        });
        root.getChildren().add(tf);
        root.getChildren().add(tf2);
        root.getChildren().add(ch);
        root.getChildren().add(l1);
        root.getChildren().add(btn);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 300));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
