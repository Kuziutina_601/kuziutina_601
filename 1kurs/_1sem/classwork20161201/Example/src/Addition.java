/**
 * Created by пользователь on 01.12.2016.
 */
class Addition implements BinaryFunction {

    @Override
    public int f(int x, int y) {
        return x + y;
    }
}
