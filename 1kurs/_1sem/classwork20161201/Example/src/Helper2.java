public class Helper2 {
    public static void main(String[] args) {
        int a = 100;
        int b = 200;

        BinaryFunction oper1 = new BinaryFunction() {
            @Override
            public int f(int x, int y) {
                return x + y;
            }
        };

        BinaryFunction oper2 = (x, y) -> {return x + y;};

        System.out.println(oper1.f(a, b));
    }
}
