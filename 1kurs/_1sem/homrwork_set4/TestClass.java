import java.util.Scanner;

public class TestClass {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		String name = sc.nextLine();
		String subject = sc.nextLine();

		Teacher teacher = new Teacher(name, subject);
		teacher.evaluate_student();
	}

}