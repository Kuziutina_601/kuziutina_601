public class RationalVector2D{

	RationalFraction x;
	RationalFraction y;

	RationalVector2D() {
		x = new RationalFraction(0, 0);
		y = new RationalFraction(0, 0);
	}

	RationalVector2D(RationalFraction x, RationalFraction y) {
		this.x = x;
		this.y = y;
	}

	public RationalFraction getX () {
		return x;
	}

	public RationalFraction getY() {
		return y;
	}

	public RationalVector2D add (RationalVector2D rv3) {
		RationalFraction x2 = new RationalFraction ();
		RationalFraction y2 = new RationalFraction ();
		x2 = x.add(rv3.getX());
		y2 = y.add(rv3.getY());

		return new RationalVector2D(x2, y2);
	}

	public double length () {
		double x = this.x.value();
		double y = this.y.value();

		return Math.sqrt(x*x + y*y);
	}

	public String toString () {
		return "< " + x + ", " + y + " >";
	}

	public RationalFraction scalarProduct(RationalVector2D rv3){
		RationalFraction result = new RationalFraction();
		RationalFraction helper = new RationalFraction();
		result = this.x.mult(rv3.getX());
		helper = this.y.mult(rv3.getY());
		result.add2(helper);
		return result;
	}

	public boolean equals (RationalVector2D rv3) {
		boolean result = false;
		if ((x.equals(rv3.getX())) && (y.equals(rv3.getY())))
			result = true;
		return result;
	}

}