public class Matrix2x2 {

	double[][] matrix = new double[2][2];

	public Matrix2x2() {
		this(0);
	}

	public Matrix2x2(double a) {
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++) {
				matrix[i][j] = a;
			}
		}
	}

	public Matrix2x2(double[][] mat) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++){
				matrix[i][j] = mat[i][j];
			}
		}
	}

	public Matrix2x2(double x, double y, double z, double m) {
		matrix[0][0] = x;
		matrix[0][1] = y;
		matrix[1][0] = z;
		matrix[1][1] = m;
	}	

	public Matrix2x2 add (Matrix2x2 mat) {
		Matrix2x2 mat2 = new Matrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				mat2.matrix[i][j] = matrix[i][j] + mat.matrix[i][j];
			}
		}

		return mat2;
	}

	public void add2 (Matrix2x2 mat) {
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				matrix[i][j] += mat.matrix[i][j];
			}
		}
	}

	public Matrix2x2 sub (Matrix2x2 mat) {
		Matrix2x2 mat2 = new Matrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				mat2.matrix[i][j] = matrix[i][j] - mat.matrix[i][j];
			}
		}

		return mat2;
	}

	public void sub2 (Matrix2x2 mat) {
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				matrix[i][j] -= mat.matrix[i][j];
			}
		}
	}

	public Matrix2x2 multNumber (double a) {
		Matrix2x2 mat2 = new Matrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				mat2.matrix[i][j] = matrix[i][j] * a;
			}
		}

		return mat2;
	}

	public void multNumber2 (double a) {
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				matrix[i][j] *= a;
			}
		}
	}

	public Matrix2x2 mult(Matrix2x2 mat2){
		double c;
		Matrix2x2 mat = new Matrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				c = 0;
				for (int k = 0; k < 2; k++){
					c += matrix[i][k]*mat2.matrix[k][j];
					
				}
				mat.matrix[i][j] = c;
			}
		}
		return mat;
	}

	public void mult2(Matrix2x2 mat2){
		double c;
		Matrix2x2 mat = new Matrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				c = 0;
				for (int k = 0; k < 2; k++){
					c += matrix[i][k]*mat2.matrix[k][j];
					
				}
				mat.matrix[i][j] = c;
			}
		}
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matrix[i][j] = mat.matrix[i][j];
			}
			
		}
	}

	public double det() {
		return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
	}

	public void transpon() {
		double k;
		k = matrix[0][1];
		matrix[0][1] = matrix[1][0];
		matrix[1][0] = k;
	}

	public Matrix2x2 inverseMatrix() {
		Matrix2x2 mat = new Matrix2x2();
		double det = det();
		if (det == 0){
			mat.matrix[0][0] = matrix[1][1] / det;
			mat.matrix[0][1] = -1 * matrix[0][1] / det;
			mat.matrix[1][0] = -1 * matrix[1][0] / det;
			mat.matrix[1][1] = matrix[0][0] / det;
		}
		else
			System.out.println("no");
		return mat;
	}

	public Matrix2x2 equivalentDiagonal() {
		Matrix2x2 mat = new Matrix2x2(0);
		int k = 1;
		if (matrix[0][0] * matrix[1][0] > 0)
			k = -k;
		mat.matrix[1][1] = matrix[0][0] * k * matrix[1][1] + matrix[1][0]*matrix[0][1];
		k = 1;
		if (matrix[0][1] * mat.matrix[1][1] > 0)
			k = -k;
		mat.matrix[0][0] = mat.matrix[1][1] * k * matrix[0][0];

		return mat;
	}

	public Vector2D multVector (Vector2D v3) {
		double x = v3.getX() * matrix[0][0] + v3.getY() * matrix[0][1];
		double y = v3.getX() * matrix[1][0] + v3.getY() * matrix[1][1];

		return new Vector2D(x, y);
	}

	public String toString() {
		String s = "";
		s = matrix[0][0] + "  " + matrix[0][1] + "\n";
		s += matrix[1][0] + "  " + matrix[1][1];

		return s;
	}

}