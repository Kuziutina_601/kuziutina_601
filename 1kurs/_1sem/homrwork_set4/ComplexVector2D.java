public class ComplexVector2D {

	ComplexNumber x;
	ComplexNumber y;

	public ComplexVector2D() {
		x = new ComplexNumber();
		y = new ComplexNumber();
		this (x, y);
	}

	public ComplexVector2D(ComplexNumber x, ComplexNumber y) {
		this.x = x;
		this.y = y;
	}

	public ComplexNumber getX() {
		return x;
	}

	public ComplexNumber getY() {
		return y;
	}

	public ComplexVector2D add (ComplexVector2D cv3) {
		ComplexNumber x2, y2;
		x2 = x.add(cv3.getX());
		y2 = y.add(cv3.getY());

		return new ComplexVector2D(x2, y2);
	}

	public String toString() {
		return "< " + x + ", " + y + " >";
	}

	public ComplexNumber scalarProduct (ComplexVector2D cv3) {
		ComplexNumber v1, v2;
		v1 = x.mult(cv3.getX());
		v2 = x.mult(cv3.getY());
		v1.add2(v2);
		return v1;
	}

	public boolean equals (ComplexVector2D cv3) {
		boolean result = false;
		if(x.equals(cv3.getX()) && (y.equals(cv3.getY())))
			result = true;
		return result;
	}


}