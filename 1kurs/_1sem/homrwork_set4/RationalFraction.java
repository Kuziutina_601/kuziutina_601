public class RationalFraction {

	private int numerator, denominator;

	public RationalFraction() {
		this (0 ,0);
	}

	public RationalFraction(int numerator, int denominator){
		this.numerator = numerator;
		this.denominator = denominator;
	}

	private int nod() {
		int a = numerator;
		int b = denominator;
		int c;
		while (b != 0) {
			a %= b;
			c = a;
			a = b;
			b = c;
		}

		return a;
	}

	public int getNumerator() {
		return numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void reduce() {
		int nod = nod();
		numerator = numerator / nod;
		denominator = denominator / nod;
	}

	public RationalFraction add (RationalFraction r3) {
		int up = numerator * r3.getDenominator() + r3.getNumerator() * denominator;
		int down = denominator * r3.getDenominator();
		RationalFraction r2 = new RationalFraction(up, down);
		r2.reduce();

		return r2; 
	}

	public void add2 (RationalFraction r3) {
		numerator = numerator * r3.getDenominator() + r3.getNumerator() * denominator;
		denominator *= getDenominator();
		this.reduce();
	}

	public RationalFraction sub (RationalFraction r3) {
		int up = numerator * r3.getDenominator() - r3.getNumerator() * denominator;
		int down = denominator * r3.getDenominator();
		RationalFraction r2 = new RationalFraction(up, down);
		r2.reduce();

		return r2; 
	}

	public void sub2 (RationalFraction r3) {
		numerator = numerator * r3.getDenominator() - r3.getNumerator() * denominator;
		denominator *= getDenominator();
		this.reduce();
	}

	public RationalFraction mult (RationalFraction r3) {
		int up = numerator * r3.getNumerator();
		int down = denominator * r3.getDenominator();
		RationalFraction r2 = new RationalFraction(up, down);
		r2.reduce();

		return r2;
	}

	public void setNumerator (int numerator) {
		this.numerator = numerator;
	}

	public void setDenominator (int denominator) {
		this.denominator = denominator;
	}

	public void mult2 (RationalFraction r3) {
		numerator *= r3.getNumerator();
		denominator *= r3.getDenominator();
		this.reduce();
	}

	public RationalFraction div (RationalFraction r3) {
		RationalFraction r2 = new RationalFraction(r3.getDenominator(), r3.getNumerator());

		return this.mult(r2);
	}

	public void div2 (RationalFraction r3) {
		RationalFraction r2 = new RationalFraction(r3.getDenominator(), r3.getNumerator());

		this.mult2(r2);
	}

	public String toString () {
		String s = "";
		s = Math.abs(numerator) + "/" + Math.abs(denominator);
		if (numerator * denominator < 0)
			s = "-" + s; 
		return s;
	}

	public double value() {
		return numerator * 1.0 / denominator;
	}

	public boolean equals (RationalFraction r3) {
		this.reduce();
		r3.reduce();
		boolean equals = false;
		if ((numerator == r3.getNumerator()) && (denominator == r3.getDenominator()))
			equals = true;
		return equals;
	}

	public int numberPart() {
		return numerator/ denominator;
	}

}