public class RationalMatrix2x2 {

	RationalFraction [][] matrix = new RationalFraction[2][2];

	public RationalMatrix2x2() {
		
		this(new RationalFraction(0, 0));
	}

	public RationalMatrix2x2(RationalFraction k) {
		this(k, k, k, k);
	}

	public RationalMatrix2x2(RationalFraction a, RationalFraction b, RationalFraction c, RationalFraction d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public RationalMatrix2x2 add (RationalMatrix2x2 rm3) {
		RationalMatrix2x2 rm2 = new RationalMatrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++){
				rm2.matrix[i][j] = matrix[i][j].add(rm3.matrix[i][j]); 
			}
		}

		return rm2;
	}

	public RationalMatrix2x2 mult(RationalMatrix2x2 rm3){
		RationalFraction c = new RationalFraction();
		RationalFraction m = new RationalFraction(0, 0);
		RationalMatrix2x2 helper = new RationalMatrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				c = m;
				for (int k = 0; k < 2; k++){
					c = c.add(matrix[i][k].mult(rm3.matrix[k][j]));
					
				}
				helper.matrix[i][j] = c;
			}
		}
		return helper;
	}

	public RationalFraction det() {
		RationalFraction v1, v2;
		v1 = matrix[0][0].mult(matrix[1][1]);
		v2 = matrix[0][1].mult(matrix[1][0]);
		return v1.sub(v2);
	}

	public RationalVector2D multVector(RationalVector2D rv3) {
		RationalFraction x = rv3.getX().mult(matrix[0][0]);
		x = x.add(rv3.getY().mult(matrix[0][1]));
		RationalFraction y = rv3.getX().mult(matrix[1][0]);
		y = y.add(rv3.getY().mult(matrix[1][1]));

		return new RationalVector2D(x, y);
	}
}