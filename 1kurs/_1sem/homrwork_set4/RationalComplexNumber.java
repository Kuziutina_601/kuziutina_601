public class RationalComplexNumber {

	RationalFraction x, y;

	public RationalComplexNumber(){
		this(new RationalFraction(0,0), new RationalFraction(0,0));
	}

	public RationalComplexNumber(RationalFraction x, RationalFraction y){
		this.x = x;
		this.y = y;
	}

	public RationalFraction getX(){
		return x;
	}

	public RationalFraction getY(){
		return y;
	}

	public RationalComplexNumber add (RationalComplexNumber rcn3) {
		RationalFraction x2, y2;
		x2 = x.add(rcn3.getX());
		y2 = y.add(rcn3.getY());
		return new RationalComplexNumber(x2, y2);
	}

	public RationalComplexNumber sub (RationalComplexNumber rcn3) {
		RationalFraction x2, y2;
		x2 = x.sub(rcn3.getX());
		y2 = y.sub(rcn3.getY());
		return new RationalComplexNumber(x2, y2);
	}

	public RationalComplexNumber mult (RationalComplexNumber rcn3) {
		RationalFraction x2 = x.mult(rcn3.getX());
		RationalFraction helper = y.mult(rcn3.getY());
		x2 = x2.sub(helper);
		RationalFraction y2 = x.mult(rcn3.getY());
		helper =  y.mult(rcn3.getX());
		y2 = y2.add(helper);

		return new RationalComplexNumber(x2, y2);
	}

	public String toString() {
		String s = y + "*i ";
		if (x.getNumerator() * x.getDenominator() > 0)
			s += "+";
		s += x;
		return s;
	}

}