import java.util.Random;

public class Teacher {

	private String name = "";
	private String subject = "";

	public Teacher (String name, String subject) {
		this.name = name;
		this.subject = subject;
	}

	public String getName () {
		return name;
	}

	public String getSubject () {
		return subject;
	}

	public void setName (String name) {
		this.name = name;
	}

	public void setSubject (String subject) {
		this.subject = subject;
	}

	public void evaluate_student () {
		Random r = new Random();
		Student student = new Student();

		int evaluat = r.nextInt(4) + 2;
		String ev = "";
		switch (evaluat) {
			case 2: ev = "неудовлетворительно";break;
			case 3: ev = "удовлетворительно"; break;
			case 4: ev = "хорошо"; break;
			case 5: ev = "отлично"; break;
			default: break;
		}

		System.out.println("Преподователь " + name + " оценил студента с именем " + student.getName() + " по предмету " + subject + " на оценку " + ev);
	}

}