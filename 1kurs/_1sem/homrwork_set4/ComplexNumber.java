public class ComplexNumber {

	double x, y;

	public ComplexNumber(){
		this (0 , 0);
	}

	public ComplexNumber(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public ComplexNumber add (ComplexNumber c3) {
		double x2 = x + c3.getX();
		double y2 = y + c3.getY();
		return new ComplexNumber(x2, y2);
	}

	public void add2 (ComplexNumber c3) {
		x += c3.getX();
		y += c3.getY();
	}

	public ComplexNumber sub (ComplexNumber c3) {
		double x2 = x - c3.getX();
		double y2 = y - c3.getY();
		return new ComplexNumber(x2, y2);
	}

	public void sub2 (ComplexNumber c3) {
		x -= c3.getX();
		y -= c3.getY();
	}

	public ComplexNumber multNumber (double a) {
		double x2 = x * a;
		double y2 = y * a;

		return new ComplexNumber(x2, y2);
	}

	public void multNumber2 (double a) {
		x *= a;
		y *= a;
	}

	public ComplexNumber mult (ComplexNumber c3) {
		double x2 = x * c3.getX() - y * c3.getY();
		double y2 = x * c3.getY() + y * c3.getX();

		return new ComplexNumber(x2, y2);
	}

	public void mult2 (ComplexNumber c3) {
		x = x * c3.getX() - y * c3.getY();
		y = x * c3.getY() + y * c3.getX();
	}

	public double length() {
		return Math.sqrt(x*x + y*y);
	}

	public ComplexNumber div (ComplexNumber c3) {
		ComplexNumber c2 = new ComplexNumber(c3.getX(), -c3.getY());
		c2 = this.mult(c2);
		double l = c2.length();
		return new ComplexNumber(c2.getX() / l , c2.getY() / l);
	}

	public void div2 (ComplexNumber c3) {
		ComplexNumber c2 = new ComplexNumber(c3.getX(), -c3.getY());
		c2 = this.mult(c2);
		double l = c2.length();
		x = c2.getX() / l;
		y = c2.getY() / l;
	}

	public String toString() {
		String s = "";
		s = y + "*i ";
		if (x < 0)
			s += x;
		else
			s += "+" + x;
		return s;
	}

	public double arg() {
		return Math.atan(y*1.0 / x);
	}

	public ComplexNumber pow(double n) {
		double r = this.length();
		r = Math.pow(r, n);
		double x2 = r * Math.cos(n * this.arg());
		double y2 = r * Math.sin(n * this.arg());

		return new ComplexNumber(x2, y2);
	}

	public boolean equals (ComplexNumber c3) {
		boolean equal = false;
		if ((x == c3.getX()) && (y == c3.getY()))
			equal = true;

		return equal;
	}



}