public class RationalComplexVector2D {

	RationalComplexNumber x, y;

	public RationalComplexVector2D() {
		this (new RationalComplexNumber(), new RationalComplexNumber());
	}

	public RationalComplexVector2D (RationalComplexNumber x, RationalComplexNumber y) {
		this.x = x;
		this.y = y;
	}

	public RationalComplexNumber getX() {
		return x;
	}

	public RationalComplexNumber getY() {
		return y;
	}

	public RationalComplexVector2D add (RationalComplexVector2D rcv3) {
		RationalComplexNumber x2, y2;
		x2 = x.add(rcv3.getX());
		y2 = y.add(rcv3.getY());

		return new RationalComplexVector2D(x2, y2);
	}

	public String toString() {
		return "< " + x +", " + y + " >";
	}

	public RationalComplexNumber scalarProduct (RationalComplexVector2D rcv3) {
		RationalComplexNumber v1, v2;
		v1 = x.mult(rcv3.getX());
		v2 = y.mult(rcv3.getY());

		return v1.add(v2);
	}

}