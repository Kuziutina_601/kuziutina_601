public class ComplexMatrix2x2 {

	ComplexNumber[][] matrix = new ComplexNumber[2][2];

	public ComplexMatrix2x2 () {
		this (new ComplexMatrix2x2(0, 0));
	}

	public ComplexMatrix2x2(ComplexNumber k) {
		this(k, k, k, k);
	}

	public ComplexMatrix2x2(ComplexNumber a, ComplexNumber b, ComplexNumber c, ComplexNumber d){
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public ComplexMatrix2x2 add (ComplexMatrix2x2 cm3) {
		ComplexMatrix2x2 cm2 = new ComplexMatrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++){
				cm2.matrix[i][j] = matrix[i][j].add(cm3.matrix[i][j]); 
			}
		}

		return cm2;
	}

	public ComplexMatrix2x2 mult(ComplexMatrix2x2 cm3){
		ComplexNumber c = new ComplexNumber();
		ComplexNumber nul = new ComplexNumber(0, 0);
		ComplexMatrix2x2 helper = new ComplexMatrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				c = nul;
				for (int k = 0; k < 2; k++){
					c = c.add(matrix[i][k].mult(cm3.matrix[k][j]));
					
				}
				helper.matrix[i][j] = c;
			}
		}
		return helper;
	}

	public ComplexNumber det() {
		ComplexNumber v1, v2;
		v1 = matrix[0][0].mult(matrix[1][1]);
		v2 = matrix[0][1].mult(matrix[1][0]);
		return v1.sub(v2);
	}

	public ComplexVector2D multVector(ComplexVector2D cv3) {
		ComplexNumber x = cv3.getX().mult(matrix[0][0]);
		x = x.add(cv3.getY().mult(matrix[0][1]));
		ComplexNumber y = cv3.getX().mult(matrix[1][0]);
		y = y.add(cv3.getY().mult(matrix[1][1]));

		return new ComplexVector2D(x, y);
	}


}