public class RationalComplexMatrix2x2 {

	RationalComplexNumber[][] matrix = new RationalComplexNumber[2][2];

	public RationalComplexMatrix2x2() {
		this(new RationalComplexNumber());
	}

	public RationalComplexMatrix2x2(RationalComplexNumber k) {
		this(k, k, k, k);
	}

	public RationalComplexMatrix2x2(RationalComplexNumber a, RationalComplexNumber b,
		RationalComplexNumber c, RationalComplexNumber d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public RationalComplexMatrix2x2 add (RationalComplexMatrix2x2 rcm3) {
		RationalComplexMatrix2x2 rcm2 = new RationalComplexMatrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++){
				rcm2.matrix[i][j] = matrix[i][j].add(rcm3.matrix[i][j]); 
			}
		}

		return rcm2;
	}

	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 rcm3){
		RationalComplexNumber c = new RationalComplexNumber();
		RationalComplexNumber nul = new RationalComplexNumber();
		RationalComplexMatrix2x2 helper = new RationalComplexMatrix2x2();
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++){
				c = nul;
				for (int k = 0; k < 2; k++){
					c = c.add(matrix[i][k].mult(rcm3.matrix[k][j]));
					
				}
				helper.matrix[i][j] = c;
			}
		}
		return helper;
	}

	public RationalComplexNumber det() {
		RationalComplexNumber v1, v2;
		v1 = matrix[0][0].mult(matrix[1][1]);
		v2 = matrix[0][1].mult(matrix[1][0]);
		return v1.sub(v2);
	}

	public RationalComplexVector2D multVector(RationalComplexVector2D rcv3) {
		RationalComplexNumber x = rcv3.getX().mult(matrix[0][0]);
		x = x.add(rcv3.getY().mult(matrix[0][1]));
		RationalComplexNumber y = rcv3.getX().mult(matrix[1][0]);
		y = y.add(rcv3.getY().mult(matrix[1][1]));

		return new RationalComplexVector2D(x, y);
	}


}