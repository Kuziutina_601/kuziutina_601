import java.util.List;

public class Node {
    private int value;
    private List<Node> sons;
    private boolean visited;

    public Node() {
        this(0, null);
    }

    public Node(int value, List<Node> sons) {
        this.value = value;
        this.sons = sons;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public List<Node> getSons() {
        return sons;
    }

    public void setSons(List<Node> son) {
        this.sons = son;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
