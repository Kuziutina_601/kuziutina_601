import java.util.*;

public class DFS {

    public static String result;

    public static void main(String[] args) {
        result = "";
    }

    public static void dfs(Node node) {
        Stack<Node> buf = new Stack<>();
        Node current = node;
        node.setVisited(true);
        buf.add(current);
        while (!buf.isEmpty()) {
            current = buf.pop();
            if (current.getSons() != null) {
                for (int i = 0; i < current.getSons().size(); i++) {
                    if (!current.getSons().get(i).isVisited()) {
                        buf.add(current.getSons().get(i));
                        current.getSons().get(i).setVisited(true);
                    }
                }
            }
            result += current.getValue();
        }
    }

    public static void bfs(Node node) {
        Queue<Node> buf = new LinkedList<>();
        Node current = node;
        buf.add(node);
        while (!buf.isEmpty()) {
            current = buf.remove();
            if (current.getSons() != null) {
                for (int i = 0; i < current.getSons().size(); i++) {
                    if (!current.getSons().get(i).isVisited()) {
                        buf.add(current.getSons().get(i));
                        current.getSons().get(i).setVisited(true);
                    }
                }
            }
            result += current.getValue();
        }
    }
}
